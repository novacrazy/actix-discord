//! Rich-text formatting tools
//!
//!

use std::borrow::Cow;
use std::cell::RefCell;
use std::fmt::{self, Write};
use std::iter;
use std::string::ToString;

use lazy_static::lazy_static;

use regex::Regex;

use crate::internal::Sealed;

bitflags! {
    /// Rich-text styles
    pub struct Style: u32 {
        /// <u>Underline style</u>
        const UNDERLINE     = 0b0001;

        /// <s>Strikthrough style</s>
        const STRIKETHROUGH = 0b0010;

        /// <i>Italic style</i>
        const ITALIC        = 0b0100;

        /// <b>Bold style</b>
        const BOLD          = 0b1000;
    }
}

impl Style {
    fn delimiter(self) -> (String, String) {
        let forward = match self {
            Style::UNDERLINE => "__".into(),
            Style::STRIKETHROUGH => "~~".into(),
            Style::ITALIC => "_".into(),
            Style::BOLD => "**".into(),
            _ => {
                let mut delimiter = String::with_capacity((self.bits().count_ones() * 2) as usize);

                if self.intersects(Style::UNDERLINE) {
                    delimiter.push_str("__");
                }

                if self.intersects(Style::STRIKETHROUGH) {
                    delimiter.push_str("~~");
                }

                if self.intersects(Style::ITALIC) {
                    delimiter.push('_');
                }

                if self.intersects(Style::BOLD) {
                    delimiter.push_str("**");
                }

                delimiter
            }
        };

        let back = forward.chars().rev().collect();

        (forward, back)
    }
}

/// Quick implementation of a "writer"-style accumulation buffer for
/// rich-text styled messages.
///
/// Technically, this is internally mutable, but that is side-stepped
/// by true mutable references for the duration of this object's lifetime,
/// which should be kept as short as possible while building up your rich-text
/// message.
///
/// When `ToString::to_string` is called on this, it swaps out the internal
/// buffer for a new empty one and returns the populated buffer, avoiding
/// any extra cloning. Therefore, `to_string` should not be called manually if
/// you intend to keep the contents of your message.
#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct RichTextWriter {
    text: RefCell<String>,
}

impl RichTextWriter {
    #[inline]
    pub fn new() -> RichTextWriter {
        RichTextWriter::default()
    }

    /// Write object to the buffer
    pub fn write(&mut self, item: impl fmt::Display) -> &mut Self {
        write!(self.text.get_mut(), "{}", item).unwrap();
        self
    }

    /// Write object to the buffer, with a newline at the end
    pub fn writeln(&mut self, item: impl fmt::Display) -> &mut Self {
        write!(self.text.get_mut(), "{}\n", item).unwrap();
        self
    }

    /// Add a single newline to the buffer
    pub fn newline(&mut self) -> &mut Self {
        self.text.get_mut().push('\n');
        self
    }

    /// Add `count` newlines to the buffer
    pub fn newlines(&mut self, count: usize) -> &mut Self {
        self.text.get_mut().extend(iter::repeat('\n').take(count));
        self
    }
}

impl ToString for RichTextWriter {
    fn to_string(&self) -> String {
        self.text.replace(String::new())
    }
}

impl<'a> ToString for &'a RichTextWriter {
    fn to_string(&self) -> String {
        self.text.replace(String::new())
    }
}

impl<'a> ToString for &'a mut RichTextWriter {
    fn to_string(&self) -> String {
        self.text.replace(String::new())
    }
}

/// Internal trait for formatting individual items
pub trait RichTextFormattable: Sealed {
    /// Formats the item with the given outer style
    fn format(&self, style: Style) -> String;
}

/// Regular text
pub struct Text<T: ToString>(pub T);

/// Inline code
pub struct Code<T: ToString>(pub T);

/// Code block with language
pub struct CodeBlock<'a, T: ToString> {
    pub code: T,
    pub language: Cow<'a, str>,
}

impl<T: ToString> Sealed for Text<T> {}
impl<T: ToString> Sealed for Code<T> {}
impl<'a, T: ToString> Sealed for CodeBlock<'a, T> {}

/// A single rich-text item, be it text, inline code or code block
#[derive(Debug, Clone)]
pub struct Item<T: RichTextFormattable> {
    item: T,
    style: Style,
}

impl<T> fmt::Display for Item<T>
where
    T: RichTextFormattable,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.item.format(self.style))
    }
}

impl<T: ToString> Item<Text<T>> {
    /// Create regular text item
    #[inline]
    pub fn text(value: T) -> Item<Text<T>> {
        Item {
            item: Text(value),
            style: Style::empty(),
        }
    }

    /// Convert regular text to inline code
    pub fn into_code(self) -> Item<Code<T>> {
        Item {
            item: Code(self.item.0),
            style: self.style,
        }
    }

    /// Convert regular text to code block
    pub fn into_code_block<'a>(self, lang: impl Into<Cow<'a, str>>) -> Item<CodeBlock<'a, T>> {
        Item {
            item: CodeBlock {
                code: self.item.0,
                language: lang.into(),
            },
            style: self.style,
        }
    }
}

impl<T: ToString> Item<Code<T>> {
    /// Convert inline code to regular text
    #[inline]
    pub fn into_text(self) -> Item<Text<T>> {
        Item {
            item: Text(self.item.0),
            style: self.style,
        }
    }

    /// Create new inline code item
    #[inline]
    pub fn code(code: T) -> Item<Code<T>> {
        Item {
            item: Code(code),
            style: Style::empty(),
        }
    }

    /// Convert inline code to code block
    pub fn into_code_block<'a>(self, lang: impl Into<Cow<'a, str>>) -> Item<CodeBlock<'a, T>> {
        Item {
            item: CodeBlock {
                code: self.item.0,
                language: lang.into(),
            },
            style: self.style,
        }
    }
}

impl<'a, T: ToString> Item<CodeBlock<'a, T>> {
    /// Convert code block to regular text
    #[inline]
    pub fn into_text(self) -> Item<Text<T>> {
        Item {
            item: Text(self.item.code),
            style: self.style,
        }
    }

    /// Convert code block to inline code
    #[inline]
    pub fn into_code(self) -> Item<Code<T>> {
        Item {
            item: Code(self.item.code),
            style: self.style,
        }
    }

    /// Create new code block item with language and code content
    #[inline]
    pub fn code_block(lang: impl Into<Cow<'a, str>>, code: T) -> Item<CodeBlock<'a, T>> {
        Item {
            item: CodeBlock {
                code,
                language: lang.into(),
            },
            style: Style::empty(),
        }
    }
}

impl<T> Item<T>
where
    T: RichTextFormattable,
{
    /// Add `UNDERLINE` style to item
    ///
    /// Example: <u>Hello, World!</u>
    #[inline(always)]
    pub fn underline(mut self) -> Self {
        self.style |= Style::UNDERLINE;
        self
    }

    /// Add `STRIKETHROUGH` style to item
    ///
    /// Example: <s>Hello, World!</s>
    #[inline(always)]
    pub fn strikethrough(mut self) -> Self {
        self.style |= Style::STRIKETHROUGH;
        self
    }

    /// Add `ITALIC` style to item
    ///
    /// Example: <i>Hello, World!</i>
    #[inline(always)]
    pub fn italic(mut self) -> Self {
        self.style |= Style::ITALIC;
        self
    }

    /// Add `BOLD` style to item
    ///
    /// Example: <b>Hello, World!</b>
    #[inline(always)]
    pub fn bold(mut self) -> Self {
        self.style |= Style::BOLD;
        self
    }

    /// Add styles to the item
    #[inline(always)]
    pub fn style(mut self, style: Style) -> Self {
        self.style |= style;
        self
    }
}

/// Convenience methods for regular types
pub trait RichTextExt: ToString + Sized {
    /// Create a text item with <u>underline</u> style applied
    #[inline]
    fn underline(self) -> Item<Text<Self>> {
        Item::text(self).underline()
    }

    /// Create a text item with <s>strikethrough</s> style applied
    #[inline]
    fn strikethrough(self) -> Item<Text<Self>> {
        Item::text(self).strikethrough()
    }

    /// Create a text item with <i>italic</i> style applied
    #[inline]
    fn italic(self) -> Item<Text<Self>> {
        Item::text(self).italic()
    }

    /// Create a text item with <b>bold</b> style applied
    #[inline]
    fn bold(self) -> Item<Text<Self>> {
        Item::text(self).bold()
    }

    /// Create an inline code item with no style
    #[inline]
    fn code(self) -> Item<Code<Self>> {
        Item::code(self)
    }

    /// Create a code block item with the given language, and no style
    #[inline]
    fn code_block<'a>(self, lang: impl Into<Cow<'a, str>>) -> Item<CodeBlock<'a, Self>> {
        Item::code_block(lang, self)
    }
}

impl<T> RichTextExt for T where T: ToString + Sized {}

impl<T: ToString> RichTextFormattable for Text<T> {
    fn format(&self, style: Style) -> String {
        lazy_static! {
            static ref SYMBOLS: Regex = Regex::new(r#"([\\*_~`])"#).unwrap();
        }

        let unescaped = self.0.to_string();

        let escaped = SYMBOLS.replace_all(&unescaped, r#"\$0"#);

        let (forward_de, back_de) = style.delimiter();

        format!("{1}{0}{2}", escaped, forward_de, back_de)
    }
}

impl<T: ToString> RichTextFormattable for Code<T> {
    fn format(&self, style: Style) -> String {
        let value = self.0.to_string();

        let mut max_ticks = 0;

        for c in value.chars() {
            if c == '`' {
                max_ticks += 1;
            } else {
                max_ticks = 0;
            }
        }

        let (forward_de, back_de) = style.delimiter();

        if max_ticks == 0 {
            format!("{1}`{0}`{2}", value, forward_de, back_de)
        } else {
            let mut ticks = String::with_capacity(max_ticks + 1);

            // this may not be correct. It needs to be tested in discord more.
            // I may have to split the inline like I do code blocks
            ticks.extend(iter::repeat('`').take(max_ticks + 1));

            format!("{2}{1}{0}{1}{3}", value, ticks, forward_de, back_de)
        }
    }
}

impl<'a, T: ToString> RichTextFormattable for CodeBlock<'a, T> {
    fn format(&self, style: Style) -> String {
        let value = self.code.to_string();

        let split = value.split("```");

        let (forward_de, back_de) = style.delimiter();

        split
            .map(|block| {
                format!(
                    "{2}```{1}\n{0}\n```{3}\n",
                    block, self.language, forward_de, back_de
                )
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::string::ToString;

    // emulates `send_message` and takes ownership of the object
    fn consumer<T: ToString>(value: T) -> String {
        value.to_string()
    }

    #[test]
    fn basic_richtext() {
        assert_eq!(
            Item::text("hello\\*").underline().to_string(),
            "__hello\\\\\\*__"
        );
        assert_eq!(
            Item::code("test").strikethrough().italic().to_string(),
            "~~_`test`_~~"
        );
        assert_eq!(
            Item::code_block("rust", "test```thing").bold().to_string(),
            "**```rust\ntest\n```**\n**```rust\nthing\n```**\n"
        )
    }

    #[test]
    fn richtext_writer() {
        let res = consumer(
            RichTextWriter::new()
                .writeln(Item::text("hello").underline())
                .write("Regular text")
                .newlines(4)
                .writeln(420)
                .writeln(Item::code("test").strikethrough().italic())
                .writeln("special text".bold())
                .write(101.code()),
        );

        assert_eq!(
            res,
            "__hello__\nRegular text\n\n\n\n420\n~~_`test`_~~\n**special text**\n`101`"
        );
    }
}
