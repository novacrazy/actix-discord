//! Bearer/Bot Context

use std::fmt;
use std::sync::Arc;

use actix::prelude::*;
use actix_web::client::ClientConnector;

use crate::identity::Identity;

use crate::internal::context::{ContextBuilder, ContextInner};
use crate::internal::Sealed;

/// Context defines a single bearer/bot user and all
/// inner functionality required for that,
/// such as rate-limiting and caching.
///
/// `Context`'s are internally mutable and shared,
/// meaning you can `clone` a context and it'll still
/// refer to the same bearer/bot within the same process.
#[derive(Clone)]
pub struct Context(pub(crate) Arc<ContextInner>);

impl AsRef<Context> for Context {
    #[inline(always)]
    fn as_ref(&self) -> &Self {
        self
    }
}

impl fmt::Debug for Context {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Context")
            .field("identity", self.identity())
            .finish()
    }
}

impl Sealed for Context {}

fn _assert_context_threadsafe() {
    crate::internal::assert_threadsafe::<Context>();
}

impl std::cmp::PartialEq for Context {
    #[inline]
    fn eq(&self, other: &Context) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }
}

// We can't implement Deref for a private object (ContextInner),
// so these will do.
impl Context {
    /// Creates a new `Context` with the given identity.
    ///
    /// `Context`s are internally reference counted and
    /// internally mutable. You can clone it around and it'll
    /// still refer back to the same context.
    #[inline]
    pub fn new(identity: Identity) -> Context {
        ContextBuilder::new(identity).finish()
    }

    /// Creates a new `Context` with the given identity **AND**
    /// a `ClientConnector` which will be used for connection pooling.
    ///
    /// `Context`s are internally reference counted and
    /// internally mutable. You can clone it around and it'll
    /// still refer back to the same context.
    #[inline]
    pub fn with_connector(identity: Identity, conn: Addr<ClientConnector>) -> Context {
        ContextBuilder::new(identity).conn(conn).finish()
    }

    #[inline(always)]
    pub fn identity(&self) -> &Identity {
        &self.inner().identity
    }

    #[cfg(feature = "internal")]
    #[inline(always)]
    pub fn inner(&self) -> &ContextInner {
        &self.0
    }

    #[cfg(not(feature = "internal"))]
    #[inline(always)]
    pub(crate) fn inner(&self) -> &ContextInner {
        &self.0
    }
}
