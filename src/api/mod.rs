//! Contains the models and async requests for
//! interacting with the Discord REST API

use futures::Future;

use failure::Error;

use crate::models::permission::Permission;

pub mod context;
pub mod endpoints;
pub mod rate_limit;
pub mod select;

pub use self::context::Context;

use crate::internal::Sealed;

/// Get required permissions to execute the action.
///
/// This trait is designed as an heirarchy, where the user
/// should implement only the functions it requires.
///
/// Only `required_with_owner` will be called, and by default other methods delegate to that.
///
/// At least one of `required`, `required_contextual` or `required_with_owner`
/// should be implemented for this trait.
///
/// This trait is left intentionally unsealed for user implementation.
pub trait Permitted {
    type Owner;

    /// Base requirements not accounting for contextual operations.
    ///
    /// **SHOULD NOT BE CALLED DIRECTLY**, call `required_contextual` or `required_with_owner` for
    /// all required permissions.
    #[inline(always)]
    fn required(&self) -> Permission {
        Permission::all()
    }

    /// Contextual requirements of the given request
    #[inline(always)]
    fn required_contextual(&self, _ctx: &Context) -> Permission {
        self.required()
    }

    /// Contextual requirements of the given request,
    /// plus knowledge about the owning/requesting object
    #[inline(always)]
    fn required_with_owner(&self, ctx: &Context, _owner: &Self::Owner) -> Permission {
        self.required_contextual(ctx)
    }
}

/// Instance Endpoint
///
/// Describes an API endpoint that uses a model instance
pub trait Endpoint<T: Sealed>: Sealed {
    type Result;

    /// Send the request to this endpoint
    fn exec(&self, ctx: &Context, args: T) -> Box<dyn Future<Item = Self::Result, Error = Error>>;
}

/// Static Endpoint
///
/// Describes a standalone API endpoint that doesn't require a model instance to use.
///
/// These usually return a model instance.
pub trait StaticEndpoint<T: Sealed>: Sealed {
    type Result;

    /// Send the request to this endpoint
    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>>;
}

pub trait InvertedStaticEndpoint<T, K: Sealed>: Sealed
where
    T: StaticEndpoint<K>,
{
    /// Send the request to this endpoint
    fn exec_static(
        ctx: &Context,
        args: T,
    ) -> Box<dyn Future<Item = <T as StaticEndpoint<K>>::Result, Error = Error>>;
}

impl<T, K: Sealed> InvertedStaticEndpoint<T, K> for K
where
    T: StaticEndpoint<K>,
{
    #[inline(always)]
    fn exec_static(
        ctx: &Context,
        args: T,
    ) -> Box<dyn Future<Item = <T as StaticEndpoint<K>>::Result, Error = Error>> {
        args.exec(ctx)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use futures::Future;

    use crate::models::user::User;

    use crate::api::endpoints::user::GetUser;

    #[test]
    #[ignore]
    fn inverted_endpoint_compile() {
        // get context from somewhere
        let ctx: Context = unsafe { std::mem::uninitialized() };

        // GetUser example
        let _ = GetUser(None).exec(&ctx).and_then(|_current_user| Ok(()));

        // Or alternatively
        let _ = User::exec_static(&ctx, GetUser(None)).and_then(|_current_user| Ok(()));
    }
}
