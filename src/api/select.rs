use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Select<Id: Serialize> {
    /// select most recent items
    #[serde(skip)] // skip, because no value at all is most recent
    MostRecent,

    /// select items around this ID
    Around(Id),

    /// select items before this ID
    Before(Id),

    /// select items after this ID
    After(Id),
}

impl<Id: Serialize> Default for Select<Id> {
    #[inline]
    fn default() -> Select<Id> {
        Select::MostRecent
    }
}
