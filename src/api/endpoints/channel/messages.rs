use crate::api::endpoints::prelude::*;

use crate::models::channel::Channel;
use crate::models::message::embed::Embed;
use crate::models::message::{Message, MessageId};
use crate::models::snowflake::SnowFlake;

/// Returns the messages for a channel.
///
/// If operating on a guild channel, this endpoint requires the `VIEW_CHANNEL`
/// permission to be present on the current user.
///
/// If the current user is missing the `READ_MESSAGE_HISTORY` permission
/// in the channel then this will return no messages (since they cannot read the message history).
///
/// Returns an array of message objects on success.
#[derive(Debug, Clone, Serialize)]
pub struct GetMessages {
    #[serde(flatten, default)]
    pub select: Select<MessageId>,

    /// max number of messages to return (1-100)
    ///
    /// Default is 50 if given `None`
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub limit: Option<u64>,
}

seal_and_permit!(GetMessages: Channel, VIEW_CHANNEL);

impl GetMessages {
    #[inline]
    pub fn sanitize(mut self) -> Self {
        self.limit = self.limit.map(|limit| limit.max(100).min(1));
        self
    }
}

impl Endpoint<GetMessages> for Channel {
    type Result = Vec<Message>;

    fn exec(
        &self,
        ctx: &Context,
        args: GetMessages,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/channels/{}/messages", self.id()))
                .json_body(args.sanitize())
                .send::<GetMessages>(None)
                .parse_json(),
        )
    }
}

/// Returns a specific message in the channel.
///
/// If operating on a guild channel, this endpoints requires the `READ_MESSAGE_HISTORY`
/// permission to be present on the current user.
///
/// Returns a message object on success.
#[derive(Debug, Clone, Serialize)]
pub struct GetMessage(pub MessageId);

seal_and_permit!(GetMessage: Channel, READ_MESSAGE_HISTORY);

impl Endpoint<GetMessage> for Channel {
    type Result = Message;

    fn exec(
        &self,
        ctx: &Context,
        args: GetMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/channels/{}/messages/{}", self.id(), args.0))
                .send::<GetMessage>(None)
                .parse_json(),
        )
    }
}

/// Post a message to a guild text or DM channel.
///
/// If operating on a guild channel, this endpoint requires the `SEND_MESSAGES`
/// permission to be present on the current user. If the tts field is set to true,
/// the `SEND_TTS_MESSAGES` permission is required for the message to be spoken.
///
/// Returns a message object.
///
/// Fires a Message Create Gateway event. See message formatting for
/// more information on how to properly format messages.
///
/// The maximum request size when sending a message is 8MB.
///
/// > Before using this endpoint, you must connect to and identify with a gateway at least once.
#[derive(Debug, Default, Clone, Serialize)]
pub struct CreateMessage {
    /// the message contents (up to 2000 characters)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    /// a nonce that can be used for optimistic message sending
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub nonce: Option<SnowFlake>,

    /// true if this is a TTS message
    pub tts: bool,

    /// the contents of the file being sent
    ///
    /// TODO
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file: Option<()>,

    /// embedded rich content
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub embed: Option<Box<Embed>>,
}

impl Sealed for CreateMessage {}

impl Permitted for CreateMessage {
    type Owner = Channel;

    #[inline]
    fn required(&self) -> Permission {
        Permission::SEND_MESSAGES.or_if(self.tts, Permission::SEND_TTS_MESSAGES)
    }
}

impl Endpoint<CreateMessage> for Channel {
    type Result = Message;

    fn exec(
        &self,
        _ctx: &Context,
        _args: CreateMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

/// Delete multiple messages in a single request.
///
/// This endpoint can only be used on guild channels and
/// requires the `MANAGE_MESSAGES` permission.
///
/// Returns a 204 empty response on success.
///
/// Fires multiple `Message Delete` Gateway events.
///
/// Any message IDs given that do not exist or are invalid
/// will count towards the minimum and maximum message count
/// (currently 2 and 100 respectively). Additionally,
/// duplicated IDs will only be counted once.
///
/// This endpoint will not delete messages older than 2 weeks,
/// and will fail if any message provided is older than that.
#[derive(Debug, Clone, Serialize)]
pub struct BulkDeleteMessages(pub Vec<MessageId>);

seal_and_permit!(BulkDeleteMessages: Channel, MANAGE_MESSAGES);

impl Endpoint<BulkDeleteMessages> for Channel {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        args: BulkDeleteMessages,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::POST)
                .api(format!("/channels/{}/messages/bulk-delete", self.id()))
                .json_body(args)
                .send::<BulkDeleteMessages>(None)
                .finish(),
        )
    }
}
