use crate::api::endpoints::prelude::*;

use crate::models::channel::{Channel, ChannelId};
use crate::models::message::Message;
use crate::models::permission::Overwrite;

pub mod invite;
pub mod messages;
pub mod permissions;

/// Get a channel by ID.
#[derive(Debug, Default, Clone, Copy)]
pub struct GetChannel(pub ChannelId);

seal_and_permit!(GetChannel: Channel);

impl StaticEndpoint<Channel> for GetChannel {
    type Result = Channel;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/channels/{}", self.0))
                .send::<GetChannel>(None)
                .parse_json(),
        )
    }
}

/// Update a channels settings.
///
/// Requires the `MANAGE_CHANNELS` permission for the guild.
///
/// Returns a `Channel` on success, and a 400 BAD REQUEST on invalid parameters.
///
/// Fires a Channel Update Gateway event.
///
/// If modifying a category, individual Channel Update events will fire
/// for each child channel that also changes.
#[derive(Debug, Default, Clone, Serialize)]
pub struct ModifyChannel {
    /// 2-100 character channel name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// the position of the channel in the left-hand listing
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub position: Option<u64>,

    /// 0-1024 character channel topic
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub topic: Option<String>,

    /// whether the channel is nsfw
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub nsfw: Option<bool>,

    /// amount of seconds a user has to wait before sending another message (0-120);
    ///
    /// bots, as well as users with the permission `manage_messages` or `manage_channel`,
    /// are unaffected.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub rate_limit_per_user: Option<u64>,

    /// the bitrate (in bits) of the voice channel; 8000 to 96000 (128000 for VIP servers)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub bitrate: Option<u64>,

    /// the user limit of the voice channel; 0 refers to no limit, 1 to 99 refers to a user limit
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user_limit: Option<u64>,

    /// channel or category-specific permissions
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub permission_overwrites: Vec<Overwrite>,

    /// id of the new parent category for a channel
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub parent_id: Option<ChannelId>,
}

seal_and_permit!(ModifyChannel: Channel, MANAGE_CHANNELS);

impl Endpoint<ModifyChannel> for Channel {
    type Result = Channel;

    fn exec(
        &self,
        ctx: &Context,
        args: ModifyChannel,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::PATCH)
                .api(format!("/channels/{}", self.id()))
                .json_body(args)
                .send::<ModifyChannel>(None)
                .parse_json(),
        )
    }
}

/// Delete a channel, or close a private message.
///
/// Requires the `MANAGE_CHANNELS` permission for the guild.
///
/// Deleting a category does not delete its child channels;
/// they will have their parent_id removed and a Channel Update Gateway
/// event will fire for each of them.
///
/// Returns a channel object on success.
///
/// Fires a Channel Delete Gateway event.
#[derive(Debug, Default, Clone, Copy)]
pub struct DeleteChannel;

seal_and_permit!(DeleteChannel: Channel, MANAGE_CHANNELS);

impl Endpoint<DeleteChannel> for Channel {
    type Result = Channel;

    fn exec(
        &self,
        ctx: &Context,
        _args: DeleteChannel,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!("/channels/{}", self.id()))
                .send::<DeleteChannel>(None)
                .parse_json(),
        )
    }
}

/// Post a typing indicator for the specified channel.
///
/// Generally bots should not implement this route. However,
/// if a bot is responding to a command and expects the computation
/// to take a few seconds, this endpoint may be called to let the
/// user know that the bot is processing their message.
///
/// Returns a 204 empty response on success.
///
/// Fires a Typing Start Gateway event.
#[derive(Debug, Default, Clone, Copy)]
pub struct TriggerTyping;

seal_and_permit!(TriggerTyping: Channel);

impl Endpoint<TriggerTyping> for Channel {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: TriggerTyping,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::POST)
                .api(format!("/channels/{}/typing", self.id()))
                .send::<TriggerTyping>(None)
                .finish(),
        )
    }
}

/// Returns all pinned messages in the channel
#[derive(Debug, Clone)]
pub struct GetPins;

seal_and_permit!(GetPins: Channel);

impl Endpoint<GetPins> for Channel {
    type Result = Vec<Message>;

    fn exec(
        &self,
        ctx: &Context,
        _args: GetPins,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/channels/{}/pins", self.id()))
                .send::<GetPins>(None)
                .parse_json(),
        )
    }
}
