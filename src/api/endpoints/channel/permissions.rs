use crate::api::endpoints::prelude::*;

use crate::models::channel::Channel;
use crate::models::permission::Overwrite;

/// Edit the channel permission overwrites for a user or role in a channel.
///
/// Only usable for guild channels.
///
/// Requires the `MANAGE_ROLES` permission.
#[derive(Debug, Clone)]
pub struct EditOverwrite(pub Overwrite);

seal_and_permit!(EditOverwrite: Channel, MANAGE_ROLES);

impl Endpoint<EditOverwrite> for Channel {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        args: EditOverwrite,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::PUT)
                .api(format!(
                    "/channels/{}/permissions/{}",
                    self.id(),
                    args.0.id()
                ))
                .json_body(args.0)
                .send::<EditOverwrite>(None)
                .finish(),
        )
    }
}

/// Delete a channel permission overwrite for a user
/// or role in a channel.
///
/// Only usable for guild channels.
///
/// Requires the `MANAGE_ROLES` permission.
#[derive(Debug, Clone)]
pub struct DeleteOverwrite(pub Overwrite);

seal_and_permit!(DeleteOverwrite: Channel, MANAGE_ROLES);

impl Endpoint<DeleteOverwrite> for Channel {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        args: DeleteOverwrite,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!(
                    "/channels/{}/permissions/{}",
                    self.id(),
                    args.0.id()
                ))
                .send::<DeleteOverwrite>(None)
                .finish(),
        )
    }
}
