use crate::api::endpoints::prelude::*;

use crate::models::channel::Channel;
use crate::models::invite::Invite;

/// Returns a list of invite objects (with invite metadata) for the channel.
///
/// Only usable for guild channels.
///
/// Requires the 'MANAGE_CHANNELS' permission.
#[derive(Debug, Clone)]
pub struct GetInvites;

seal_and_permit!(GetInvites: Channel, MANAGE_CHANNELS);

impl Endpoint<GetInvites> for Channel {
    type Result = Vec<Invite>;

    fn exec(
        &self,
        ctx: &Context,
        _args: GetInvites,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/channels/{}/invites", self.id()))
                .send::<GetInvites>(None)
                .parse_json(),
        )
    }
}

/// Create a new invite object for the channel.
///
/// Only usable for guild channels.
///
/// Requires the `CREATE_INSTANT_INVITE` permission.
///
/// Returns an invite object.
#[derive(Debug, Default, Clone, Serialize)]
pub struct CreateInvite {
    /// duration of invite in seconds before expiry, or 0 for never
    ///
    /// Defaults to 86400 (24 hours) if `None`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_age: Option<u64>,

    /// max number of uses or 0 for unlimited
    ///
    /// Defaults to Unlimited if `None`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_uses: Option<u64>,

    /// whether this invite only grants temporary membership
    #[serde(skip_serializing_if = "Option::is_none")]
    pub temporary: Option<bool>,

    /// if true, don't try to reuse a similar invite (useful for creating many unique one time use invites)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unique: Option<bool>,
}

seal_and_permit!(CreateInvite: Channel, CREATE_INSTANT_INVITE);

impl Endpoint<CreateInvite> for Channel {
    type Result = Invite;

    fn exec(
        &self,
        ctx: &Context,
        args: CreateInvite,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::POST)
                .api(format!("/channels/{}/invites", self.id()))
                .json_body(args)
                .send::<CreateInvite>(None)
                .parse_json(),
        )
    }
}
