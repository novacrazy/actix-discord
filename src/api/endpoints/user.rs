use crate::api::endpoints::prelude::*;

use hashbrown::HashMap;

use crate::models::channel::Channel;
use crate::models::guild::{Guild, GuildId};
use crate::models::user::{Connection, User, UserId};

#[derive(Debug, Default, Clone, Copy)]
pub struct GetUser(pub Option<UserId>);

impl Sealed for GetUser {}

impl StaticEndpoint<User> for GetUser {
    type Result = User;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .map(|req| match self.0 {
                    Some(id) => req.api(format!("/users/{}", id)),
                    None => req.api("/users/@me"),
                })
                .send::<GetUser>(None)
                .parse_json(),
        )
    }
}

/// Modify the requester's user account settings.
#[derive(Debug, Clone, Serialize)]
pub struct ModifyUser {
    /// users username, if changed may cause the users discriminator to be randomized.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,

    /// if passed, modifies the user's avatar
    #[serde(skip_serializing_if = "Option::is_none")]
    pub avatar: Option<()>,
}

impl Sealed for ModifyUser {}

impl StaticEndpoint<User> for ModifyUser {
    type Result = User;

    fn exec(self, _ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct GetCurrentGuilds {
    #[serde(flatten, default)]
    pub select: Select<GuildId>,
}

impl Sealed for GetCurrentGuilds {}

impl StaticEndpoint<User> for GetCurrentGuilds {
    type Result = Vec<Guild>;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api("/users/@me/guilds")
                .json_body(self)
                .send::<GetCurrentGuilds>(None)
                .parse_json(),
        )
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(untagged)]
pub enum CreateDM {
    /// Create a new DM channel with a user.
    Direct(UserId),

    /// Create a new group DM channel with multiple users.
    Group {
        /// access tokens of users that have granted your app the gdm.join scope
        access_tokens: Vec<String>,

        /// a dictionary of user ids to their respective nicknames
        nicks: HashMap<UserId, String>,
    },
}

impl Sealed for CreateDM {}

impl StaticEndpoint<User> for CreateDM {
    type Result = Channel;

    fn exec(self, _ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct GetConnections;

impl Sealed for GetConnections {}

impl StaticEndpoint<User> for GetConnections {
    type Result = Vec<Connection>;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api("/users/@me/connections")
                .send::<GetConnections>(None)
                .parse_json(),
        )
    }
}
