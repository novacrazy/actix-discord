use crate::api::endpoints::prelude::*;

use crate::models::channel::Channel;
use crate::models::guild::{
    integration::Integration, role::Role, DefaultMessageNotificationLevel,
    ExplicitContentFilterLevel, Guild, GuildId, VerificationLevel,
};
use crate::models::invite::Invite;
use crate::models::voice::VoiceRegionId;

#[derive(Debug, Default, Clone, Copy)]
pub struct GetGuild(pub GuildId);

seal_and_permit!(GetGuild: Context);

impl StaticEndpoint<Guild> for GetGuild {
    type Result = Guild;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/guilds/{}", self.0))
                .send::<GetGuild>(None)
                .parse_json(),
        )
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct DeleteGuild;

seal_and_permit!(DeleteGuild: Guild, GUILD_OWNER);

impl Endpoint<DeleteGuild> for Guild {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: DeleteGuild,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!("/guilds/{}", self.id()))
                .send::<DeleteGuild>(None)
                .finish(),
        )
    }
}

#[derive(Debug, Default, Clone, Serialize)]
pub struct CreateGuild {
    /// name of the guild (2-100 characters)
    pub name: String,

    /// Voice Region
    pub region: Option<VoiceRegionId>,

    /// image for the guild icon
    pub icon: Option<String>, //TODO replace with image structure

    pub verification_level: VerificationLevel,

    pub default_message_notifications: DefaultMessageNotificationLevel,

    pub explicit_content_filter: ExplicitContentFilterLevel,

    /// new guild roles
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub roles: Vec<Role>,

    /// new guild's channels
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub channels: Vec<Channel>,
}

impl Sealed for CreateGuild {}

impl StaticEndpoint<Guild> for CreateGuild {
    type Result = Guild;

    fn exec(self, _ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CreateChannel {}

impl Sealed for CreateChannel {}

impl Endpoint<CreateChannel> for Guild {
    type Result = Channel;

    fn exec(
        &self,
        _ctx: &Context,
        _args: CreateChannel,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct GetIntegrations;

impl Sealed for GetIntegrations {}

impl Endpoint<GetIntegrations> for Guild {
    type Result = Vec<Integration>;

    fn exec(
        &self,
        ctx: &Context,
        _args: GetIntegrations,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/guilds/{}/integrations", self.id()))
                .send::<GetIntegrations>(None)
                .parse_json(),
        )
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct GetInvites;

impl Sealed for GetInvites {}

impl Endpoint<GetInvites> for Guild {
    type Result = Vec<Invite>;

    fn exec(
        &self,
        ctx: &Context,
        _args: GetInvites,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/guilds/{}/invites", self.id()))
                .send::<GetInvites>(None)
                .parse_json(),
        )
    }
}
