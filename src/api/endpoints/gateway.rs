use crate::api::endpoints::prelude::*;

use crate::models::gateway::GatewayEndpoint;

#[derive(Debug, Clone, Copy)]
pub struct GetGatewayEndpoint;

seal_and_permit!(GetGatewayEndpoint);

impl StaticEndpoint<GatewayEndpoint> for GetGatewayEndpoint {
    type Result = GatewayEndpoint;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(ctx.identity().select("/gateway", "/gateway/bot"))
                .send::<GetGatewayEndpoint>(Some(RateLimitedBehavior::Delay { retries: 5 }))
                .parse_json(),
        )
    }
}
