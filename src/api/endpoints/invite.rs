use crate::api::endpoints::prelude::*;

use crate::models::invite::Invite;

#[derive(Debug, Default, Clone, Serialize)]
pub struct GetInvite {
    #[serde(skip)]
    pub code: String,

    /// whether the invite should contain approximate member counts
    pub with_counts: bool,
}

impl Sealed for GetInvite {}

impl StaticEndpoint<Invite> for GetInvite {
    type Result = Invite;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!("/invites/{}", self.code))
                .send::<GetInvite>(None)
                .parse_json(),
        )
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct DeleteInvite;

impl Sealed for DeleteInvite {}

impl Endpoint<DeleteInvite> for Invite {
    type Result = Invite;

    fn exec(
        &self,
        ctx: &Context,
        _args: DeleteInvite,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!("/invites/{}", self.code().as_ref()))
                .send::<DeleteInvite>(None)
                .parse_json(),
        )
    }
}
