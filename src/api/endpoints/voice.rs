use crate::api::endpoints::prelude::*;

use crate::models::voice::VoiceRegion;

#[derive(Debug, Clone, Default, Serialize)]
pub struct GetVoiceRegions;

impl Sealed for GetVoiceRegions {}

impl StaticEndpoint<VoiceRegion> for GetVoiceRegions {
    type Result = Vec<VoiceRegion>;

    fn exec(self, ctx: &Context) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api("/voice/regions")
                .send::<GetVoiceRegions>(None)
                .parse_json(),
        )
    }
}
