use api::endpoints::prelude::*;

use models::channel::{Channel, ChannelId};
use models::webhook::Webhook;

/// Create a new webhook.
///
/// Requires the `MANAGE_WEBHOOKS` permission.
#[derive(Debug, Serialize, Deserialize)]
pub struct CreateWebhook {
    /// name of the webhook (2-32 characters)
    name: String,

    /// image for the default webhook avatar
    avatar: Option<String>,
}

impl Sealed for CreateWebhook {}

impl Endpoint<CreateWebhook> for Channel {
    type Result = Webhook;

    fn exec(&self, ctx: &Context, args: CreateWebhook) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
            .method(Method::POST)
            .api(format!("/channels/{}/webhooks", self.id()))
            .build(|b| b.json(args))
            .send::<CreateWebhook>(None)
            .json()
        )
    }
}

/// Returns a list of channel webhook objects.
///
/// Requires the `MANAGE_WEBHOOKS` permission.
pub struct GetWebhooks;

impl Sealed for GetWebhooks {}

impl Endpoint<GetWebhooks> for Channel {
    type Result = Vec<Webhook>;

    fn exec(&self, ctx: &Context, _args: GetWebhooks) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
            .method(Method::GET)
            .api(format!("/channels/{}/webhooks", self.id()))
            .send::<GetWebhooks>(None)
            .json()
        )
    }
}