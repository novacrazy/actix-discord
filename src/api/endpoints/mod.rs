/// Simple "prelude" for endpoints, just to clean up the boilerplate
#[macro_use]
pub(crate) mod prelude {
    pub use actix_web::http::Method;
    pub use actix_web::Error as ActixError;

    pub use futures::{future, Future};

    pub use failure::Error;

    pub use crate::error::DiscordError;

    pub use crate::models::permission::Permission;
    pub use crate::models::snowflake::SnowFlake;

    pub use crate::api::rate_limit::RateLimitedBehavior;
    pub use crate::api::select::Select;
    pub use crate::api::{Context, Endpoint, Permitted, StaticEndpoint};

    pub use crate::internal::request::RateLimitedRequest;
    pub use crate::internal::Sealed;

    macro_rules! seal_and_permit {
        ($t:ty $(:$l:lifetime)* $(,$perm:ident)*) => {
            impl $(<$l>)* Sealed for $t {}

            impl $(<$l>)* Permitted for $t {
                type Owner = Context;

                #[inline]
                fn required(&self) -> Permission {
                    Permission::empty() $(| Permission::$perm)*
                }
            }
        };

        ($t:ty $(:$l:lifetime)* :$owner:ty $(,$perm:ident)*) => {
            impl $(<$l>)* Sealed for $t {}

            impl $(<$l>)* Permitted for $t {
                type Owner = $owner;

                #[inline]
                fn required(&self) -> Permission {
                    Permission::empty() $(| Permission::$perm)*
                }
            }
        };
    }
}

pub mod audit;
pub mod channel;
pub mod gateway;
pub mod guild;
pub mod invite;
pub mod message;
pub mod user;
pub mod voice;
