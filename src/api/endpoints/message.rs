use std::fmt::Write;

use crate::api::endpoints::prelude::*;

use crate::models::emoji::Emoji;
use crate::models::message::embed::Embed;
use crate::models::message::Message;
use crate::models::user::{User, UserId};

#[derive(Debug, Clone, Serialize)]
pub struct EditMessage {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub embed: Option<Box<Embed>>,
}

impl Sealed for EditMessage {}

impl Permitted for EditMessage {
    type Owner = Message;

    fn required_with_owner(&self, ctx: &Context, owner: &Message) -> Permission {
        if owner
            .author()
            .map(|author| author.id() == ctx.user().id())
            .unwrap_or(false)
        {
            // No required permissions if we own the message
            Permission::empty()
        } else {
            // Otherwise we need permission to manage messages
            Permission::MANAGE_MESSAGES
        }
    }
}

impl Endpoint<EditMessage> for Message {
    type Result = Message;

    fn exec(
        &self,
        _ctx: &Context,
        _args: EditMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(future::empty())
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct DeleteMessage;

impl Sealed for DeleteMessage {}

impl Endpoint<DeleteMessage> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: DeleteMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!(
                    "/channels/{}/messages/{}",
                    self.channel_id(),
                    self.id(),
                ))
                .send::<DeleteMessage>(None)
                .finish(),
        )
    }
}

/// Pin a message in a channel.
///
/// Requires the `MANAGE_MESSAGES` permission.
#[derive(Debug, Clone)]
pub struct PinMessage;

impl Sealed for PinMessage {}

impl Endpoint<PinMessage> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: PinMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::PUT)
                .api(format!(
                    "/channels/{}/pins/{}",
                    self.channel_id(),
                    self.id()
                ))
                .send::<PinMessage>(None)
                .finish(),
        )
    }
}

/// Delete a pinned message in a channel.
///
/// Requires the `MANAGE_MESSAGES` permission.
#[derive(Debug, Clone)]
pub struct UnpinMessage;

impl Sealed for UnpinMessage {}

impl Endpoint<UnpinMessage> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: UnpinMessage,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!(
                    "/channels/{}/pins/{}",
                    self.channel_id(),
                    self.id()
                ))
                .send::<UnpinMessage>(None)
                .finish(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct CreateReaction<'a>(pub &'a Emoji);

impl<'a> Sealed for CreateReaction<'a> {}

impl<'a> Endpoint<CreateReaction<'a>> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        args: CreateReaction<'_>,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::PUT)
                .api(format!(
                    "/channels/{}/messages/{}/reactions/{}/@me",
                    self.channel_id(),
                    self.id(),
                    args.0,
                ))
                .send::<CreateReaction<'_>>(None)
                .finish(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct DeleteReaction<'a> {
    pub user: Option<UserId>,
    pub reaction: &'a Emoji,
}

impl<'a> Sealed for DeleteReaction<'a> {}

impl<'a> Permitted for DeleteReaction<'a> {
    type Owner = Message;

    #[inline]
    fn required(&self) -> Permission {
        match self.user {
            Some(_) => Permission::MANAGE_MESSAGES,
            None => Permission::empty(),
        }
    }
}

impl<'a> Endpoint<DeleteReaction<'a>> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        args: DeleteReaction<'a>,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        let DeleteReaction { user, reaction } = args;

        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api({
                    let mut uri = format!(
                        "/channels/{}/messages/{}/reactions/{}/",
                        self.channel_id(),
                        self.id(),
                        reaction
                    );

                    if let Some(user) = user {
                        write!(uri, "{}", user).unwrap();
                    } else {
                        uri.push_str("@me");
                    }

                    uri
                })
                .send::<DeleteReaction<'_>>(None)
                .finish(),
        )
    }
}

#[derive(Debug, Clone, Copy)]
pub struct DeleteAllReactions;

seal_and_permit!(DeleteAllReactions, MANAGE_MESSAGES);

impl Endpoint<DeleteAllReactions> for Message {
    type Result = ();

    fn exec(
        &self,
        ctx: &Context,
        _args: DeleteAllReactions,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::DELETE)
                .api(format!(
                    "/channels/{}/messages/{}/reactions",
                    self.channel_id(),
                    self.id()
                ))
                .send::<DeleteAllReactions>(None)
                .finish(),
        )
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct GetReactionUsers<'a> {
    #[serde(skip)]
    pub reaction: &'a Emoji,

    #[serde(flatten)]
    pub select: Select<UserId>,

    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub limit: Option<u64>,
}

impl<'a> Sealed for GetReactionUsers<'a> {}

impl<'a> Endpoint<GetReactionUsers<'a>> for Message {
    type Result = Vec<User>;

    fn exec(
        &self,
        ctx: &Context,
        args: GetReactionUsers<'a>,
    ) -> Box<dyn Future<Item = Self::Result, Error = Error>> {
        Box::new(
            RateLimitedRequest::build(ctx)
                .method(Method::GET)
                .api(format!(
                    "/channels/{}/messages/{}/reactions/{}",
                    self.channel_id(),
                    self.id(),
                    args.reaction,
                ))
                .json_body(args)
                .send::<GetReactionUsers<'_>>(None)
                .parse_json(),
        )
    }
}
