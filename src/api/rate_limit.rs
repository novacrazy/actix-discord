//! Discord's API rate limits requests in order to prevent abuse and
//! overload of our services. Rate limits are applied on a per-route
//! basis (meaning they can be different for each route called),
//! with the exception of an additional global rate limit spanning
//! across the entire API. Not every endpoint has an endpoint-specific
//! ratelimit, so for those endpoints there is only the global rate limit applied.
//!
//! By "per-route," we mean that unique rate limits exist for the path
//! you are accessing on our API, not including the HTTP method
//! (GET, POST, PUT, DELETE) and including major parameters. This means
//! that different HTTP methods (for example, both GET and DELETE) share
//! the same rate limit if the route is the same. Additionally, rate limits
//! take into account major parameters in the URL. For example,
//! `/channels/:channel_id` and `/channels/:channel_id/messages/:message_id`
//! both take `channel_id` into account when generating rate limits since it's
//! the major parameter. Currently, the only major parameters are `channel_id`,
//! `guild_id`, and `webhook_id`.
//!
//! >There is currently a single exception to the above rule regarding different
//! HTTP methods sharing the same rate limit, and that is for the
//! deletion of messages. Deleting
//! messages falls under a separate, higher rate limit so that bots are
//! able to more quickly delete content from channels (which is useful for moderation bots).
//!
//! Because we may change rate limits at any time and rate limits can be
//! different per application, *rate limits should not be hard coded into
//! your bot/application*. In order to properly support our dynamic rate limits,
//! your bot/application should parse for our rate limits in response
//! headers and locally prevent exceeding of the limits as they change.
//!
//! >Routes for controlling emojis do
//! not follow the normal rate limit conventions. These routes are specifically
//! limited on a per-guild basis to prevent abuse. This means that the quota
//! returned by our APIs may be inaccurate, and you may encounter 429s.

use std::sync::atomic::Ordering;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};

use chrono::NaiveDateTime;

use crate::internal::atomic::{AtomicI64, AtomicU64};

pub mod headers {
    pub const X_RATELIMIT_GLOBAL: &str = "X-RateLimit-Global";
    pub const X_RATELIMIT_LIMIT: &str = "X-RateLimit-Limit";
    pub const X_RATELIMIT_REMAINING: &str = "X-RateLimit-Remaining";
    pub const X_RATELIMIT_RESET: &str = "X-RateLimit-Reset";
}

/// For every API request made, we return optional HTTP response headers
/// containing the rate limit encountered during your request.
#[derive(Debug)]
pub struct RateLimiting {
    /// `X-RateLimit-Limit` - The number of requests that can be made
    pub limit: AtomicU64,

    /// `X-RateLimit-Remaining` - The number of remaining requests that can be made
    pub remaining: AtomicI64,

    /// `X-RateLimit-Reset` - Epoch time (seconds since 00:00:00 UTC on January 1, 1970)
    /// at which the rate limit resets
    pub reset: AtomicU64,
}

fn _assert_rate_limiting_threadsafe() {
    crate::internal::assert_threadsafe::<RateLimiting>();
}

impl Default for RateLimiting {
    fn default() -> RateLimiting {
        RateLimiting::new(0, 0, 0)
    }
}

impl RateLimiting {
    pub fn new(limit: u64, remaining: u64, reset: u64) -> RateLimiting {
        RateLimiting {
            limit: AtomicU64::new(limit),
            remaining: AtomicI64::new(remaining as i64),
            reset: AtomicU64::new(reset),
        }
    }

    pub fn reset_datetime(&self) -> NaiveDateTime {
        NaiveDateTime::from_timestamp(self.reset.load(Ordering::SeqCst) as i64, 0)
    }

    pub fn reset_instant(&self, now: Instant) -> Instant {
        let time_since_epoch = Duration::from_secs(self.reset.load(Ordering::SeqCst));

        let reset_time = UNIX_EPOCH + time_since_epoch;

        let time_until_reset = reset_time.duration_since(SystemTime::now());

        match time_until_reset {
            Ok(future_duration) => now + future_duration,
            Err(_) => now,
        }
    }
}

/// In the case that a rate limit is exceeded, the API will return a HTTP 429 response code with a JSON body.
///
/// Note that the normal rate-limiting headers will be sent in this response.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct RateLimitExceeded {
    /// A message saying you are being rate limited.
    pub message: String,

    /// The number of milliseconds to wait before submitting another request.
    pub retry_after: u64,

    /// A value indicating if you are being globally rate limited or not
    pub global: bool,
}

/// If a request is found to be rate-limited,
/// it can either be cancelled immediately,
/// or it could wait on the rate limiting to let up and try again.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum RateLimitedBehavior {
    /// Delay the request until the rate limiting resets
    Delay { retries: i64 },
    /// Cancel the rate limited request immediately,
    /// returning an error.
    Cancel,
}

impl RateLimitedBehavior {
    pub fn retries(&self) -> i64 {
        match *self {
            RateLimitedBehavior::Delay { retries } => retries,
            _ => 0,
        }
    }
}
