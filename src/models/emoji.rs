use std::fmt::{self, Write};
use std::sync::Arc;

//use crate::models::common::{self, Mentionable};
use crate::models::guild::role::RoleId;
use crate::models::user::User;

use crate::models::resource::{ImageFormat, Resource};

declare_snowflake_newtype! {
    /// Unique Emoji ID
    EmojiId: Emoji
}

impl Emoji {
    /// Create a new unicode emoji
    #[inline]
    pub fn unicode(symbol: char) -> Emoji {
        Emoji::Unicode { _id: None, symbol }
    }
}

pub type EmojiName = smallstr::SmallString<[u8; 32]>;

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Emoji {
    /// Fully formed Discord Emoji
    Full {
        #[access(copy)]
        #[serde(default)]
        id: EmojiId,

        name: EmojiName,

        /// roles this emoji is whitelisted to
        #[serde(default, skip_serializing_if = "Option::is_none")]
        roles: Option<Vec<RoleId>>,

        /// user that created this emoji
        #[access(clone)]
        #[serde(default, skip_serializing_if = "Option::is_none")]
        user: Option<Arc<User>>,

        /// whether this emoji must be wrapped in colons
        require_colons: bool,

        /// whether this emoji is managed
        managed: bool,

        /// whether this emoji is animated
        animated: bool,
    },
    Partial {
        id: EmojiId,
        name: EmojiName,

        #[doc(hidden)]
        #[access(ignore)]
        #[serde(default, skip)]
        __non_exhaustive: (),
    },
    /// Unicode-only emoji without a specific id
    Unicode {
        // Unicode-only emojis have a null id field
        #[serde(rename = "id", skip_serializing)]
        #[doc(hidden)]
        _id: Option<EmojiId>,

        /// Unicode symbol emoji
        #[serde(rename = "name")]
        symbol: char,
    },
}

impl Emoji {
    pub fn image(&self) -> Option<Resource> {
        self.id().map(|id| {
            Resource::new(
                "emojis",
                ImageFormat::GIF | ImageFormat::PNG,
                id.to_string(),
                None,
                self.animated().unwrap_or(false),
            )
        })
    }
}

impl From<char> for Emoji {
    fn from(c: char) -> Emoji {
        Emoji::unicode(c)
    }
}

impl fmt::Display for Emoji {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Emoji::Full { id, ref name, .. } | Emoji::Partial { id, ref name, .. } => {
                write!(f, "{}:{}", id, name.as_ref())
            }
            Emoji::Unicode { symbol, .. } => f.write_char(symbol),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json as json;

    #[test]
    fn parse_example_emojis() {
        const FULL_EMOJI: &str = r#"
            {
                "id": "41771983429993937",
                "name": "LUL",
                "roles": [ "41771983429993000", "41771983429993111" ],
                "user": {
                    "username": "Luigi",
                    "discriminator": "0002",
                    "id": "96008815106887111",
                    "avatar": "5500909a3274e1812beb4e8de6631111"
                },
                "require_colons": true,
                "managed": false,
                "animated": false
            }
        "#;

        const UNICODE_EMOJI: &str = r#"{
            "id": null,
            "name": "🔥"
        }"#;

        const PARTIAL_EMOJI: &str = r#"{
            "id": "41771983429993937",
            "name": "LUL"
        }"#;

        let full: Emoji = json::from_str(FULL_EMOJI).unwrap();
        let partial: Emoji = json::from_str(PARTIAL_EMOJI).unwrap();
        let unicode: Emoji = json::from_str(UNICODE_EMOJI).unwrap();

        assert!(full.is_full());
        assert!(partial.is_partial());
        assert!(unicode.is_unicode());
    }
}
