use chrono::{DateTime, Utc};

use crate::models::media::Dimensions;

pub type EmbedType = smallstr::SmallString<[u8; 4]>;

#[derive(Debug, Clone, Default, AutoAccessor, Serialize, Deserialize)]
pub struct Embed {
    /// title of embed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,

    /// type of embed (always "rich" for webhook embeds)
    #[serde(default, skip_serializing_if = "Option::is_none", rename = "type")]
    pub kind: Option<EmbedType>,

    /// description of embed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,

    /// url of embed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,

    /// timestamp of embed content
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[access(copy)]
    pub timestamp: Option<DateTime<Utc>>,

    /// color code of the embed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub color: Option<u32>,

    /// footer information
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub footer: Option<EmbedFooter>,

    /// thumbnail information
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumbnail: Option<EmbedMediaData>,

    /// media information
    #[serde(default, flatten)]
    pub media: Option<EmbedMedia>,

    /// provider information
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub provider: Option<EmbedProvider>,

    /// author information
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub author: Option<EmbedAuthor>,

    /// fields information
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub fields: Vec<EmbedField>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum EmbedMedia {
    /// image information
    Image(EmbedMediaData),

    /// video information
    Video(EmbedMediaData),
}

#[derive(Debug, Clone, AutoAccessor, Default, Serialize, Deserialize)]
pub struct EmbedMediaData {
    /// source url of media (only supports http(s) and attachments)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,

    /// a proxied url of the media
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub proxy_url: Option<String>,

    #[access(copy)]
    #[serde(default, flatten)]
    pub dimensions: Option<Dimensions>,
}

#[derive(Debug, Clone, AutoAccessor, Default, Serialize, Deserialize)]
pub struct EmbedProvider {
    /// name of provider
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// url of provider
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}

#[derive(Debug, Clone, AutoAccessor, Default, Serialize, Deserialize)]
pub struct EmbedAuthor {
    /// name of author
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// url of author
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,

    /// url of author icon (only supports http(s) and attachments)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub icon_url: Option<String>,

    /// a proxied url of author icon
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub proxy_icon_url: Option<String>,
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct EmbedFooter {
    /// footer text
    pub text: String,

    /// url of footer icon (only supports http(s) and attachments)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub icon_url: Option<String>,

    /// a proxied url of footer icon
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub proxy_icon_url: Option<String>,
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct EmbedField {
    /// name of the field
    pub name: String,

    /// value of the field
    pub value: String,

    /// whether or not this field should display inline
    #[serde(default)]
    pub inline: bool,
}
