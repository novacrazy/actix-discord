use crate::models::media::Dimensions;

declare_snowflake_newtype! {
    AttachmentId:+ Attachment
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct Attachment {
    #[access(copy)]
    pub id: AttachmentId,

    /// name of file attached
    filename: String,

    /// size of file in bytes
    size: u64,

    /// source url of file
    url: String,

    /// a proxied url of file
    proxy_url: String,

    #[serde(flatten, default)]
    dimensions: Option<Dimensions>,
}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json as json;

    #[test]
    fn parse_attachment() {
        const EXAMPLE_ATTACHMENT_A: &str = r#"{
            "id": "80351110224678912",
            "filename": "test.png",
            "size": 10,
            "url": "test",
            "proxy_url": "test",
            "width": 10,
            "height": 20
        }"#;

        const EXAMPLE_ATTACHMENT_B: &str = r#"{
            "id": "80351110224678912",
            "filename": "test.png",
            "size": 10,
            "url": "test",
            "proxy_url": "test",
            "width": null,
            "height": null
        }"#;

        let a: Attachment =
            json::from_str(EXAMPLE_ATTACHMENT_A).expect("Could not parse example attachment");
        let b: Attachment =
            json::from_str(EXAMPLE_ATTACHMENT_B).expect("Could not parse example attachment");

        assert_eq!(
            a.dimensions,
            Some(Dimensions {
                height: 20,
                width: 10
            })
        );
        assert_eq!(b.dimensions, None);
    }
}
