use std::sync::Arc;

use crate::models::channel::ChannelId;
use crate::models::guild::GuildId;
use crate::models::user::User;

declare_snowflake_newtype! {
    /// Unique Webhook ID
    WebhookId:+ Webhook
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct Webhook {
    #[access(copy)]
    pub id: WebhookId,

    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub guild_id: Option<GuildId>,

    #[access(copy)]
    pub channel_id: ChannelId,

    #[access(clone)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user: Option<Arc<User>>,

    #[serde(default)]
    pub name: Option<String>,

    #[serde(default)]
    pub avatar: Option<String>,

    pub token: String,
}
