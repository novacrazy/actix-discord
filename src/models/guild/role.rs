use crate::models::common::Mentionable;
use crate::models::permission::Permission;
use crate::models::snowflake::IsSnowFlake;

declare_snowflake_newtype! {
    /// Role Id
    RoleId:+ Role
}

#[derive(Debug, Default, AutoAccessor, Clone, Serialize, Deserialize)]
pub struct Role {
    #[serde(skip_serializing_if = "IsSnowFlake::is_null")]
    #[access(copy)]
    pub id: RoleId,

    /// role name
    pub name: String,

    /// integer representation of hexadecimal color code
    pub color: u32,

    /// if this role is pinned in the user listing
    pub hoist: bool,

    /// position of this role
    pub position: u64,

    /// permission bit set
    #[access(copy)]
    pub permissions: Permission,

    /// whether this role is managed by an integration
    pub managed: bool,

    /// whether this role is mentionable
    pub mentionable: bool,
}

impl Mentionable for Role {
    fn mention(&self) -> Option<String> {
        if self.mentionable {
            Some(format!("<@&{}>", self.id))
        } else {
            None
        }
    }
}
