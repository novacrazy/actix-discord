use std::sync::Arc;

use crate::models::channel::ChannelId;
use crate::models::emoji::Emoji;
use crate::models::id::IdTable;
use crate::models::permission::Permission;
use crate::models::resource::{ImageFormat, Resource};
use crate::models::user::UserId;
use crate::models::voice::VoiceRegionId;

pub mod integration;
pub mod member;
pub mod role;

pub use self::integration::Integration;
pub use self::member::Member;
pub use self::role::{Role, RoleId};

declare_snowflake_newtype! {
    /// Unique Guild ID
    GuildId:+ Guild
}

pub type GuildName = smallstr::SmallString<[u8; 64]>;

#[derive(Debug, AutoAccessor, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Guild {
    Available {
        #[access(copy)]
        id: GuildId,

        /// guild name (2-100 characters)
        name: GuildName,

        /// icon hash
        icon: Option<String>,

        /// splash hash
        splash: Option<String>,

        /// whether or not the user is the owner of the guild
        #[serde(default)]
        owner: bool,

        /// id of owner
        #[access(copy)]
        owner_id: UserId,

        /// total permissions for the user in the guild (does not include channel overrides)
        #[access(copy)]
        #[serde(default)]
        permissions: Permission,

        /// voice region id for the guild
        #[serde(default)]
        region: Option<VoiceRegionId>,

        /// afk timeout in seconds
        #[serde(default)]
        afk_timeout: u64,

        /// is this guild embeddable (e.g. widget)
        #[serde(default)]
        embed_enabled: bool,

        /// if not null, the channel id that the widget will generate an invite to
        #[access(copy)]
        #[serde(default)]
        embed_channel_id: Option<ChannelId>,

        /// verification level required for the guild
        #[access(copy)]
        verification_level: VerificationLevel,

        /// default message notifications level
        #[access(copy)]
        default_message_notifications: DefaultMessageNotificationLevel,

        /// explicit content filter level
        #[access(copy)]
        explicit_content_filter: ExplicitContentFilterLevel,

        /// roles in the guild
        roles: IdTable<RoleId, Arc<Role>>,

        /// custom guild emojis
        emojis: Vec<Arc<Emoji>>,

        /// is this guild unavailable
        #[serde(default)]
        unavailable: bool,
    },
    Partial {
        id: GuildId,
        name: GuildName,
        icon: Option<String>,
        splash: Option<String>,

        #[doc(hidden)]
        #[access(ignore)]
        #[serde(default, skip)]
        __non_exhaustive: (),
    },
    Unavailable {
        id: GuildId,
        unavailable: bool,
    },
}

impl Guild {
    /// Get the image endpoint for the guild icon
    pub fn icon_image(&self) -> Option<Resource> {
        self.icon().map(|icon| {
            Resource::new(
                "icons",
                ImageFormat::PNG | ImageFormat::JPEG | ImageFormat::WEBP,
                self.id().to_string(),
                Some(icon.clone()),
                false,
            )
        })
    }

    /// Get the image endpoint for the guild splash image
    pub fn splash_image(&self) -> Option<Resource> {
        self.splash().map(|splash| {
            Resource::new(
                "splashes",
                ImageFormat::PNG | ImageFormat::JPEG | ImageFormat::WEBP,
                self.id().to_string(),
                Some(splash.clone()),
                false,
            )
        })
    }
}

pub struct GuildEmbed {
    /// whether the embed is enabled
    pub enabled: bool,

    /// the embed channel id
    pub channel_id: Option<ChannelId>,
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum DefaultMessageNotificationLevel {
        AllMessages     = 0,
        OnlyMentions    = 1,
    }
}

impl Default for DefaultMessageNotificationLevel {
    fn default() -> DefaultMessageNotificationLevel {
        DefaultMessageNotificationLevel::AllMessages
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum ExplicitContentFilterLevel {
        Disabled            = 0,
        MembersWithoutRoles = 1,
        AllMembers          = 2,
    }
}

impl Default for ExplicitContentFilterLevel {
    fn default() -> ExplicitContentFilterLevel {
        ExplicitContentFilterLevel::Disabled
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    /// Multi-Factor Authentication Level
    pub enum MFALevel {
        None        = 0,
        Elevated    = 1,
    }
}

impl Default for MFALevel {
    fn default() -> MFALevel {
        MFALevel::None
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum VerificationLevel {
        /// unrestricted
        None        = 0,

        /// must have verified email on account
        Low         = 1,

        /// must be registered on Discord for longer than 5 minutes
        Medium      = 2,

        /// (╯°□°）╯︵ ┻━┻ - must be a member of the server for longer than 10 minutes
        High        = 3,

        /// ┻━┻ミヽ(ಠ益ಠ)ﾉ彡┻━┻ - must have a verified phone number
        VeryHigh    = 4,
    }
}

impl Default for VerificationLevel {
    fn default() -> VerificationLevel {
        VerificationLevel::None
    }
}

impl_serde_for_enum_primitive!(DefaultMessageNotificationLevel);
impl_serde_for_enum_primitive!(ExplicitContentFilterLevel);
impl_serde_for_enum_primitive!(MFALevel);
impl_serde_for_enum_primitive!(VerificationLevel);
