use std::ops::Deref;
use std::sync::Arc;

use chrono::{DateTime, Utc};

use crate::models::guild::role::RoleId;
use crate::models::id::HasId;
use crate::models::user::{User, UserId};

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct Member {
    /// the user this guild member represents
    #[access(clone)]
    pub user: Arc<User>,

    /// this users guild nickname (if one is set)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub nick: Option<String>,

    /// array of role object ids
    pub roles: Vec<RoleId>,

    /// when the user joined the guild
    #[access(copy)]
    pub joined_at: DateTime<Utc>,

    /// whether the user is deafened
    pub deaf: bool,

    /// whether the user is muted
    pub mute: bool,
}

impl HasId<UserId> for Member {
    #[inline]
    fn id(&self) -> UserId {
        self.user.id()
    }
}

impl Deref for Member {
    type Target = User;

    fn deref(&self) -> &User {
        &self.user
    }
}
