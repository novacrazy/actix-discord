use std::sync::Arc;

use chrono::{DateTime, Utc};

use crate::models::guild::role::RoleId;
use crate::models::user::User;

declare_snowflake_newtype! {
    /// Unique Account ID
    IntegrationAccountId:+ IntegrationAccount
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct IntegrationAccount {
    /// id of the account
    #[access(copy)]
    pub id: IntegrationAccountId,

    /// name of the account
    pub name: String,
}

declare_snowflake_newtype! {
    IntegrationId:+ Integration
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Integration {
    Full {
        /// integration id
        #[access(copy)]
        id: IntegrationId,

        /// integration name
        name: String,

        /// integration type (twitch, youtube, etc)
        #[serde(rename = "type")]
        kind: String,

        /// is this integration enabled
        enabled: bool,

        /// is this integration syncing
        syncing: bool,

        /// id that this integration uses for "subscribers"
        #[access(copy)]
        role_id: RoleId,

        /// the behavior of expiring subscribers
        expire_behavior: u64,

        /// the grace period before expiring subscribers
        expire_grace_period: u64,

        /// user for this integration
        #[access(clone)]
        user: Arc<User>,

        /// integration account information
        account: IntegrationAccount,

        /// when this integration was last synced
        #[access(copy)]
        synced_at: DateTime<Utc>,
    },
    Partial {
        id: IntegrationId,

        #[doc(hidden)]
        #[access(ignore)]
        #[serde(default, skip)]
        __non_exhaustive: (),
    },
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct Ban {
    /// the reason for the ban
    #[serde(default)]
    pub reason: Option<String>,

    /// the banned user
    #[access(clone)]
    pub user: Arc<User>,
}
