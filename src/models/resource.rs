bitflags! {
    pub struct ImageFormat: u32 {
        /// Portable Network Graphic
        const PNG  = 0b0001;

        /// Web Picture
        const WEBP = 0b0010;

        /// Graphical Interchange Format
        const GIF  = 0b0100;

        /// Joint Photographic Experts Group
        const JPEG = 0b1000;
    }
}

impl ImageFormat {
    pub(crate) fn extension(self) -> &'static str {
        match self {
            ImageFormat::PNG => ".png",
            ImageFormat::WEBP => ".webp",
            ImageFormat::GIF => ".gif",
            ImageFormat::JPEG => ".jpg",
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Resource {
    base: &'static str,
    mask: ImageFormat,
    id: String,
    hash: Option<String>,
    animated: bool,
}

impl Resource {
    pub(crate) fn new(
        base: &'static str,
        mask: ImageFormat,
        id: String,
        hash: Option<String>,
        animated: bool,
    ) -> Resource {
        Resource {
            base,
            mask,
            id,
            hash,
            animated,
        }
    }

    pub fn image(&self, format: ImageFormat) -> Option<String> {
        if format.contains(self.mask) {
            let mut url = format!("{}/{}", self.base, self.id);

            if let Some(ref hash) = self.hash {
                if self.animated {
                    url.push_str(&format!("/a_{}{}", hash, format.extension()));
                } else {
                    url.push_str(&format!("/{}{}", hash, format.extension()));
                }
            } else {
                url.push_str(format.extension());
            }

            Some(url)
        } else {
            None
        }
    }
}
