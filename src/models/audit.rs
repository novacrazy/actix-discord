use std::sync::Arc;

use crate::models::snowflake::SnowFlake;

use crate::models::application::ApplicationId;
use crate::models::channel::ChannelId;
use crate::models::guild::role::Role;
use crate::models::guild::{
    DefaultMessageNotificationLevel, ExplicitContentFilterLevel, MFALevel, VerificationLevel,
};
use crate::models::id::IdTable;
use crate::models::permission::{Overwrite, Permission};
use crate::models::user::{User, UserId};
use crate::models::webhook::{Webhook, WebhookId};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AuditLog {
    /// list of webhooks found in the audit log
    pub webhooks: IdTable<WebhookId, Webhook>,

    /// list of users found in the audit log
    pub users: IdTable<UserId, User>,

    /// list of audit log entires
    #[serde(rename = "audit_log_entries")]
    pub entries: IdTable<AuditLogEntryId, AuditLogEntry>,
}

declare_snowflake_newtype!(AuditLogEntryId:+ AuditLogEntry);

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct AuditLogEntry {
    /// id of the entry
    #[access(copy)]
    pub id: AuditLogEntryId,

    /// id of the affected entity (webhook, user, role, etc.)
    #[access(copy)]
    #[serde(default)]
    pub target_id: Option<SnowFlake>,

    /// changes made to the target_id
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub changes: Vec<AuditLogChange>,

    /// the user who made the changes
    #[access(copy)]
    pub user_id: UserId,

    /// type of action that occured
    #[access(copy)]
    pub action_type: AuditLogEvent,

    /// additional info for certain action types
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub options: Option<OptionalAuditEntryInfo>,

    /// the reason for the change
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub reason: Option<String>,
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
#[serde(untagged)]
pub enum OptionalAuditEntryInfo {
    MemberPrune {
        /// number of days after which inactive members were kicked
        delete_number_days: String,

        /// number of members removed by the prune
        members_removed: String,
    },
    MessageDelete {
        /// channel in which the messages were deleted
        #[access(copy)]
        channel_id: ChannelId,

        /// number of deleted messages
        count: String,
    },
    ChannelOverwrite {
        /// id of the overwritten entity
        #[access(copy)]
        id: ChannelId,

        /// type of overwritten entity ("member" or "role")
        #[serde(rename = "type")]
        kind: String,

        /// name of the role if type is "role"
        role_name: String,
    },
}

macro_rules! audit_log_change {
    ($(#[$($attrs:tt)*])* pub enum $st:ident { $( $(#[$($field_attrs:tt)*])* $name:ident: $d:ty => $affected:ident$(,)*),* }) => {
        $(#[$($attrs)*])*
        pub enum $st {
            #[doc(hidden)]
            __NonExhaustive,
            $(
                $(#[$($field_attrs)*])*
                $name {
                    #[serde(default, skip_serializing_if = "Option::is_none")]
                    new_value: Option<$d>,
                    #[serde(default, skip_serializing_if = "Option::is_none")]
                    old_value: Option<$d>,
                }
            ),*
        }

        impl $st {
            /// Returns what data structures are affected by this change so they can be updated
            pub fn affects(&self) -> AuditLogChangeAffect {
                match self {
                    $($st::$name {..} => AuditLogChangeAffect::$affected,)*
                    _ => AuditLogChangeAffect::Other
                }
            }
        }
    };
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum AuditLogChangeAffect {
    Guild,
    Channel,
    Role,
    Invite,
    User,
    Other,
}

audit_log_change! {
    #[derive(Debug, Clone, Serialize, Deserialize)]
    #[serde(tag = "key")]
    pub enum AuditLogChange {
        /// name changed
        #[serde(rename = "name")]
        Name: String => Guild,

        /// icon changed
        #[serde(rename = "icon_hash")]
        IconHash: String => Guild,

        /// invite splash page artwork changed
        #[serde(rename = "splash_hash")]
        SplashHash: String => Guild,

        /// Owner changed
        #[serde(rename = "owner_id")]
        OwnerId: UserId => Guild,

        /// Region changed
        #[serde(rename = "region")]
        Region: String => Guild,

        ///afk channel changed
        #[serde(rename = "afk_channel_id")]
        AFKChannelId: ChannelId => Guild,

        ///afk timeout duration changed
        #[serde(rename = "afk_timeout")]
        AKFTimeout: u64 => Guild,

        ///two-factor auth requirement changed
        #[serde(rename = "mfa_level")]
        MFALevel: MFALevel => Guild,

        /// required verification level changed
        #[serde(rename = "verification_level")]
        VerificationLevel: VerificationLevel => Guild,

        /// change in whose messages are scanned and deleted for explicit content in the server
        #[serde(rename = "explicit_content_filter")]
        ExplicitContentFilter: ExplicitContentFilterLevel => Guild,

        /// default message notification level changed
        #[serde(rename = "default_message_notifications")]
        DefaultMessageNotifications: DefaultMessageNotificationLevel => Guild,

        /// guild invite vanity url changed
        #[serde(rename = "vanity_url_code")]
        VanityUrlCode: String => Guild,

        /// New role added
        #[serde(rename = "$add")]
        AddRole: Vec<Arc<Role>> => Guild,

        /// Role removed
        #[serde(rename = "$remove")]
        RemoveRole: Vec<Arc<Role>> => Guild,

        /// change in number of days after which inactive and role-unassigned members are kicked
        #[serde(rename = "prune_delete_days")]
        PruneDeleteDays: u64 => Guild,

        /// server widget enabled/disable
        #[serde(rename = "widget_enabled")]
        WidgetEnabled: bool => Guild,

        /// channel id of the server widget changed
        #[serde(rename = "widget_channel_id")]
        WidgetChannelId: ChannelId => Guild,

        /// text or voice channel position changed
        #[serde(rename = "position")]
        Position: u64 => Channel,

        /// text channel topic changed
        #[serde(rename = "topic")]
        Topic: String => Channel,

        /// voice channel bitrate changed
        #[serde(rename = "bitrate")]
        Bitrate: u64 => Channel,

        /// permissions on a channel changed
        #[serde(rename = "permission_overwrites")]
        PermissionOverwrites: Vec<Overwrite> => Channel,

        /// channel nsfw restriction changed
        #[serde(rename = "nsfw")]
        Nsfw: bool => Channel,

        /// application id of the added or removed webhook or bot
        #[serde(rename = "application_id")]
        ApplicationId: ApplicationId => Channel,

        /// permissions for a role changed
        #[serde(rename = "permissions")]
        Permissions: Role => Role,

        /// role color changed
        #[serde(rename = "color")]
        Color: u32 => Role,

        /// role is now displayed/no longer displayed separate from online users
        #[serde(rename = "hoist")]
        Hoist: bool => Role,

        /// role is now mentionable/unmentionable
        #[serde(rename = "mentionable")]
        Mentionable: bool => Role,

        /// a permission on a text or voice channel was allowed for a role
        #[serde(rename = "allow")]
        Allow: Permission => Role,

        /// a permission on a text or voice channel was denied for a role
        #[serde(rename = "deny")]
        Deny: Permission => Role,

        /// invite code changed
        #[serde(rename = "code")]
        Code: String => Invite,

        /// channel for invite code changed
        #[serde(rename = "channel_id")]
        Channel: ChannelId => Invite,

        /// person who created invite code changed
        #[serde(rename = "inviter_id")]
        Inviter: UserId => Invite,

        /// change to max number of times invite code can be used
        #[serde(rename = "max_uses")]
        MaxUses: u64 => Invite,

        /// number of times invite code used changed
        #[serde(rename = "uses")]
        Uses: u64 => Invite,

        /// how long invite code lasts changed
        #[serde(rename = "max_age")]
        MaxAge: u64 => Invite,

        /// invite code is temporary/never expires
        #[serde(rename = "temporary")]
        Temporary: bool => Invite,

        /// user server deafened/undeafened
        #[serde(rename = "deaf")]
        Deaf: bool => User,

        /// user server muted/unmuteds
        #[serde(rename = "mute")]
        Mute: bool => User,

        /// user nickname changed
        #[serde(rename = "nick")]
        Nick: String => User,

        /// user avatar changed
        #[serde(rename = "avatar_hash")]
        AvatarHash: String => User,

        /// the id of the changed entity - sometimes used in conjunction with other keys
        #[serde(rename = "id")]
        Identifier: SnowFlake => Other,

        /// type of entity created
        #[serde(rename = "type")]
        Type: AuditLogChangeType => Other,
    }
}

/// type of entity created
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum AuditLogChangeType {
    String(String),
    ChannelId(ChannelId),
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum AuditLogEvent {
        GuildUpdate             = 1,
        ChannelCreate           = 10,
        ChannelUpdate           = 11,
        ChannelDelete           = 12,
        ChannelOverwriteCreate  = 13,
        ChannelOverwriteUpdate  = 14,
        ChannelOverwriteDelete  = 15,
        MemberKick              = 20,
        MemberPrune             = 21,
        MemberBanAdd            = 22,
        MemberBanRemove         = 23,
        MemberUpdate            = 24,
        MemberRoleUpdate        = 25,
        RoleCreate              = 30,
        RoleUpdate              = 31,
        RoleDelete              = 32,
        InviteCreate            = 40,
        InviteUpdate            = 41,
        InviteDelete            = 42,
        WebhookCreate           = 50,
        WebhookUpdate           = 51,
        WebhookDelete           = 52,
        EmojiCreate             = 60,
        EmojiUpdate             = 61,
        EmojiDelete             = 62,
        MessageDelete           = 72,
    }
}

impl_serde_for_enum_primitive!(AuditLogEvent);
