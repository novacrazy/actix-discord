use std::sync::Arc;

use chrono::{DateTime, Utc};

use crate::models::channel::Channel;
use crate::models::guild::Guild;
use crate::models::user::User;

use crate::internal::Sealed;

pub type InviteCode = smallstr::SmallString<[u8; 16]>;

/// Represents a code that when used, adds a user to a guild or group DM channel.
#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Invite {
    Full {
        /// the invite code (unique ID)
        code: InviteCode,

        /// the guild this invite is for
        #[access(clone)]
        guild: Option<Arc<Guild>>,

        /// the channel this invite is for
        #[access(clone)]
        channel: Arc<Channel>,

        /// approximate count of online members
        #[serde(default)]
        approximate_presence_count: Option<u64>,

        /// approximate count of total members
        #[serde(default)]
        approximate_member_count: Option<u64>,
    },
    Partial {
        code: InviteCode,

        #[doc(hidden)]
        #[access(ignore)]
        #[serde(default, skip)]
        __non_exhaustive: (),
    },
}

impl Sealed for Invite {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InviteMetadata {
    /// user who created the invite
    pub inviter: Arc<User>,

    /// number of times this invite has been used
    pub uses: u64,

    /// max number of times this invite can be used
    pub max_uses: u64,

    /// duration (in seconds) after which the invite expires
    pub max_age: u64,

    /// whether this invite only grants temporary membership
    pub temporary: bool,

    /// when this invite was created
    pub created_at: DateTime<Utc>,

    /// whether this invite is revoked
    pub revoked: bool,
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_json as json;

    #[test]
    fn parse_example_invite() {
        const INVITE_FIXTURE: &str = r#"
        {
            "code": "0vCdhLbwjZZTWZLD",
            "guild": {
                "id": "165176875973476352",
                "name": "CS:GO Fraggers Only",
                "splash": null,
                "icon": null
            },
            "channel": {
                "id": "165176875973476352",
                "name": "illuminati",
                "type": 0
            }
        }
        "#;

        let invite: Invite =
            json::from_str(INVITE_FIXTURE).expect("Unable to parse example invite");

        assert!(invite.is_full());
    }
}
