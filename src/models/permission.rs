//! https://discordapp.com/developers/docs/topics/permissions

use std::fmt::{self, Write};

declare_snowflake_newtype! {
    /// Unique Overwrite ID
    OverwriteId:+ Overwrite
}

#[derive(Debug, AutoAccessor, Clone, Copy, Serialize, Deserialize)]
pub struct Overwrite {
    #[access(copy)]
    #[serde(skip_serializing)]
    pub id: OverwriteId,

    /// "member" for a user or "role" for a role
    #[access(copy)]
    pub kind: OverwriteType,

    /// the bitwise value of all allowed permissions
    #[access(copy)]
    pub allow: Permission,

    /// the bitwise value of all disallowed permissions
    #[access(copy)]
    pub deny: Permission,
}

impl Overwrite {
    pub fn apply(&self, mut permissions: Permission) -> Permission {
        permissions.remove(self.deny);
        permissions.insert(self.allow);

        permissions
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum OverwriteType {
    Role,
    Member,
}

bitflags! {
    #[derive(Default)]
    pub struct Permission: u64 {
        /// Allows creation of instant invites
        const CREATE_INSTANT_INVITE = 0x00000001;

        /// Allows kicking members
        const KICK_MEMBERS          = 0x00000002;

        /// Allows banning members
        const BAN_MEMBERS           = 0x00000004;

        /// Allows all permissions and bypasses channel permission overwrites
        const ADMINISTRATOR         = 0x00000008;

        /// Allows management and editing of channels
        const MANAGE_CHANNELS       = 0x00000010;

        /// Allows management and editing of the guild
        const MANAGE_GUILD          = 0x00000020;

        /// Allows for the addition of reactions to messages
        const ADD_REACTIONS         = 0x00000040;

        /// Allows for viewing of audit logs
        const VIEW_AUDIT_LOG        = 0x00000080;

        /// Allows guild members to view a channel, which includes reading messages in text channels
        const VIEW_CHANNEL          = 0x00000400;

        /// Allows for sending messages in a channel
        const SEND_MESSAGES         = 0x00000800;

        /// Allows for sending of /tts messages
        const SEND_TTS_MESSAGES     = 0x00001000;

        /// Allows for deletion of other users messages
        const MANAGE_MESSAGES       = 0x00002000;

        /// Links sent by users with this permission will be auto-embedded
        const EMBED_LINKS           = 0x00004000;

        /// Allows for uploading images and files
        const ATTACH_FILES          = 0x00008000;

        /// Allows for reading of message history
        const READ_MESSAGE_HISTORY  = 0x00010000;

        /// Allows for using the @everyone tag to notify all users in a channel, and the @here tag to notify all online users in a channel
        const MENTION_EVERYONE      = 0x00020000;

        /// Allows the usage of custom emojis from other servers
        const USE_EXTERNAL_EMOJIS   = 0x00040000;

        /// Allows for joining of a voice channel
        const CONNECT               = 0x00100000;

        /// Allows for speaking in a voice channel
        const SPEAK                 = 0x00200000;

        /// Allows for muting members in a voice channel
        const MUTE_MEMBERS          = 0x00400000;

        /// Allows for deafening of members in a voice channel
        const DEAFEN_MEMBERS        = 0x00800000;

        /// Allows for moving of members between voice channels
        const MOVE_MEMBERS          = 0x01000000;

        /// Allows for using voice-activity-detection in a voice channel
        const USE_VAD               = 0x02000000;

        /// Allows for modification of own nickname
        const CHANGE_NICKNAME       = 0x04000000;

        /// Allows for modification of other users nicknames
        const MANAGE_NICKNAMES      = 0x08000000;

        /// Allows management and editing of roles
        const MANAGE_ROLES          = 0x10000000;

        /// Allows management and editing of webhooks
        const MANAGE_WEBHOOKS       = 0x20000000;

        /// Allows management and editing of emojis
        const MANAGE_EMOJIS         = 0x40000000;

        /// Custom Permission representing user is guild owner
        const GUILD_OWNER           = 0x80000000000;

        /// All text channel permissions
        const TEXT_CHANNEL_PERMISSIONS =
            Self::CREATE_INSTANT_INVITE.bits    |
            Self::MANAGE_CHANNELS.bits          |
            Self::ADD_REACTIONS.bits            |
            Self::VIEW_CHANNEL.bits             |
            Self::SEND_MESSAGES.bits            |
            Self::SEND_TTS_MESSAGES.bits        |
            Self::MANAGE_MESSAGES.bits          |
            Self::EMBED_LINKS.bits              |
            Self::ATTACH_FILES.bits             |
            Self::READ_MESSAGE_HISTORY.bits     |
            Self::MENTION_EVERYONE.bits         |
            Self::USE_EXTERNAL_EMOJIS.bits      |
            Self::MANAGE_ROLES.bits             |
            Self::MANAGE_WEBHOOKS.bits;

        /// All voice channel permissions
        const VOICE_CHANNEL_PERMISSIONS =
            Self::CREATE_INSTANT_INVITE.bits    |
            Self::MANAGE_CHANNELS.bits          |
            Self::VIEW_CHANNEL.bits             |
            Self::CONNECT.bits                  |
            Self::SPEAK.bits                    |
            Self::MUTE_MEMBERS.bits             |
            Self::DEAFEN_MEMBERS.bits           |
            Self::MOVE_MEMBERS.bits             |
            Self::USE_VAD.bits                  |
            Self::MANAGE_ROLES.bits             |
            Self::MANAGE_WEBHOOKS.bits;


        const REQUIRES_2FA =
            Self::KICK_MEMBERS.bits             |
            Self::BAN_MEMBERS.bits              |
            Self::ADMINISTRATOR.bits            |
            Self::MANAGE_CHANNELS.bits          |
            Self::MANAGE_GUILD.bits             |
            Self::MANAGE_MESSAGES.bits          |
            Self::MANAGE_ROLES.bits             |
            Self::MANAGE_WEBHOOKS.bits          |
            Self::MANAGE_EMOJIS.bits;
    }
}

impl_serde_for_bitflags!(Permission);

macro_rules! impl_perm_names {
    ($($perm:ident,)*) => {
        const PERM_NAMES: &'static [(Permission, &'static str)] = &[
            $((Permission::$perm, stringify!($perm)),)*
        ];
    }
}

impl Permission {
    #[inline(always)]
    pub fn owner() -> Permission {
        Permission::all()
    }

    #[inline(always)]
    pub fn admin() -> Permission {
        Permission::all() - Permission::GUILD_OWNER
    }

    /// Combines permissions if the condition is met
    #[inline(always)]
    pub fn or_if(self, condition: bool, perm: Permission) -> Permission {
        if condition {
            self | perm
        } else {
            self
        }
    }

    #[inline(always)]
    pub fn requires_2fa(self) -> bool {
        self.intersects(Permission::REQUIRES_2FA)
    }

    impl_perm_names!(
        CREATE_INSTANT_INVITE,
        KICK_MEMBERS,
        BAN_MEMBERS,
        ADMINISTRATOR,
        MANAGE_CHANNELS,
        MANAGE_GUILD,
        ADD_REACTIONS,
        VIEW_AUDIT_LOG,
        VIEW_CHANNEL,
        SEND_MESSAGES,
        SEND_TTS_MESSAGES,
        MANAGE_MESSAGES,
        EMBED_LINKS,
        ATTACH_FILES,
        READ_MESSAGE_HISTORY,
        MENTION_EVERYONE,
        USE_EXTERNAL_EMOJIS,
        CONNECT,
        SPEAK,
        MUTE_MEMBERS,
        DEAFEN_MEMBERS,
        MOVE_MEMBERS,
        USE_VAD,
        CHANGE_NICKNAME,
        MANAGE_NICKNAMES,
        MANAGE_ROLES,
        MANAGE_WEBHOOKS,
        MANAGE_EMOJIS,
        GUILD_OWNER,
    );

    pub fn print<W>(self, sep: &str, f: &mut W) -> fmt::Result
    where
        W: Write,
    {
        let mut iter = Permission::PERM_NAMES
            .iter()
            .filter(|(perm, _)| self.contains(*perm));

        match iter.next() {
            Some((_, name)) => f.write_str(name)?,
            None => return Ok(()),
        }

        for (_, name) in iter {
            f.write_str(sep)?;
            f.write_str(name)?;
        }

        Ok(())
    }
}
