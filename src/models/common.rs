pub trait Mentionable {
    fn mention(&self) -> Option<String>;
}
