pub trait Opcode {
    /// Can this opcode be sent from the client?
    fn client(&self) -> bool;

    /// Can this opcode be recieved from the server?
    fn server(&self) -> bool;
}

enum_from_primitive! {
    /// All gateway events in Discord are tagged with an opcode that denotes the payload type.
    ///
    /// Your connection to our gateway may also sometimes close. When it does,
    /// you will receive a close code that tells you what happened.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum GatewayOpcode {
        /// dispatches an event
        Dispatch            = 0,

        /// used for ping checking
        Heartbeat           = 1,

        /// used for client handshake
        Identify            = 2,

        /// used to update the client status
        StatusUpdate        = 3,

        /// used to join/move/leave voice channels
        VoiceStatusUpdate   = 4,

        /// used to resume a closed connection
        Resume              = 6,

        /// used to tell clients to reconnect to the gateway
        Reconnect           = 7,

        /// used to request guild members
        RequestGuildMembers = 8,

        /// used to notify client they have an invalid session id
        InvalidSession      = 9,

        /// sent immediately after connecting,
        /// contains heartbeat and server debug information
        Hello               = 10,

        /// sent immediately following a client heartbeat that was received
        HeartbeatACK        = 11,
    }
}

enum_from_primitive! {
    /// Our voice gateways have their own set of opcodes and close codes.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum GatewayVoiceOpcode {
        /// begin a voice websocket connection
        Identify            = 0,

        /// select the voice protocol
        SelectProtocol      = 1,

        /// complete the websocket handshake
        Ready               = 2,

        /// keep the websocket connection alive
        Heartbeat           = 3,

        /// describe the session
        SessionDescription  = 4,

        /// indicate which users are speaking
        Speaking            = 5,

        /// sent immediately following a received client heartbeat
        HeartbeatACK        = 6,

        /// resume a connection
        Resume              = 7,

        /// the continuous interval in milliseconds after which the client should send a heartbeat
        Hello               = 8,

        /// acknowledge Resume
        Resumed             = 9,

        /// a client has disconnected from the voice channel
        ClientDisconnect    = 10,
    }
}

impl Opcode for GatewayOpcode {
    fn client(&self) -> bool {
        match self {
            GatewayOpcode::Heartbeat
            | GatewayOpcode::Identify
            | GatewayOpcode::StatusUpdate
            | GatewayOpcode::VoiceStatusUpdate
            | GatewayOpcode::Resume
            | GatewayOpcode::RequestGuildMembers => true,
            _ => false,
        }
    }

    fn server(&self) -> bool {
        match self {
            GatewayOpcode::Dispatch
            | GatewayOpcode::Heartbeat
            | GatewayOpcode::Reconnect
            | GatewayOpcode::InvalidSession
            | GatewayOpcode::Hello
            | GatewayOpcode::HeartbeatACK => true,
            _ => false,
        }
    }
}

impl Opcode for GatewayVoiceOpcode {
    fn client(&self) -> bool {
        match self {
            GatewayVoiceOpcode::Identify
            | GatewayVoiceOpcode::SelectProtocol
            | GatewayVoiceOpcode::Heartbeat
            | GatewayVoiceOpcode::Speaking
            | GatewayVoiceOpcode::Resume => true,
            _ => false,
        }
    }

    fn server(&self) -> bool {
        match self {
            GatewayVoiceOpcode::Ready
            | GatewayVoiceOpcode::SessionDescription
            | GatewayVoiceOpcode::HeartbeatACK
            | GatewayVoiceOpcode::Hello
            | GatewayVoiceOpcode::Resumed
            | GatewayVoiceOpcode::ClientDisconnect => true,
            _ => false,
        }
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum GatewayCloseEvent {
        /// We're not sure what went wrong. Try reconnecting?
        UnknownError            = 4000,
        /// You sent an invalid Gateway opcode or an invalid payload for an opcode. Don't do that!
        UnknownOpcode           = 4001,
        /// You sent an invalid payload to us. Don't do that!
        DecodeError             = 4002,
        /// You sent us a payload prior to identifying.
        NotAuthenticated        = 4003,
        /// The account token sent with your identify payload is incorrect.
        AuthenticationFailed    = 4004,
        /// You sent more than one identify payload. Don't do that!
        AlreadyAuthenticated    = 4005,
        /// The sequence sent when resuming the session was invalid. Reconnect and start a new session.
        InvalidSequence         = 4007,
        /// Woah nelly! You're sending payloads to us too quickly. Slow it down!
        RateLimited             = 4008,
        /// Your session timed out. Reconnect and start a new one.
        SessionTimeout          = 4009,
        /// You sent us an invalid shard when identifying.
        InvalidShard            = 4010,
        /// The session would have handled too many guilds -
        /// you are required to shard your connection in order to connect.
        ShardingRequired        = 4011,
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum GatewayVoiceCloseEvent {
        /// You sent an invalid opcode.
        UnknownOpcode           = 4001,
        /// You sent a payload before identifying with the Gateway.
        NotAuthenticated        = 4003,
        /// The token you sent in your identify payload is incorrect.
        AuthenticationFailed    = 4004,
        /// You sent more than one identify payload. Stahp.
        AlreadyAuthenticated    = 4005,
        /// Your session is no longer valid.
        SessionNoLongerValid    = 4006,
        /// Your session has timed out.
        SessionTimeout          = 4009,
        /// We can't find the server you're trying to connect to.
        ServerNotFound          = 4011,
        /// We didn't recognize the protocol you sent.
        UnknownProtocol         = 4012,
        /// Oh no! You've been disconnected! Try resuming.
        Disconnected            = 4014,
        /// The server crashed. Our bad! Try resuming.
        VoiceServerCrashed      = 4015,
        /// We didn't recognize your encryption.
        UnknownEncryptionMode   = 4016,
    }
}

impl_serde_for_enum_primitive!(GatewayOpcode);
impl_serde_for_enum_primitive!(GatewayVoiceOpcode);
impl_serde_for_enum_primitive!(GatewayCloseEvent);
impl_serde_for_enum_primitive!(GatewayVoiceCloseEvent);

enum_from_primitive! {
    /// Along with the HTTP error code, our API can also return more
    /// detailed error codes through a code key in the JSON error response.
    ///
    /// The response will also contain a message key containing a more friendly error string.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum JsonResponseCode {
        UnknownAccount              = 10001,
        UnknownApplication          = 10002,
        UnknownChannel              = 10003,
        UnknownGuild                = 10004,
        UnknownIntegration          = 10005,
        UnknownInvite               = 10006,
        UnknownMember               = 10007,
        UnknownMessage              = 10008,
        UnknownOverwrite            = 10009,
        UnknownProvider             = 10010,
        UnknownRole                 = 10011,
        UnknownToken                = 10012,
        UnknownUser                 = 10013,
        UnknownEmoji                = 10014,
        /// Bots cannot use this endpoint
        BearerOnlyEndpoint          = 20001,
        /// Only bots can use this endpoint
        BotOnlyEndpoint             = 20002,
        /// Maximum number of guilds reached (100)
        MaxGuildsReached            = 30001,
        /// Maximum number of friends reached (1000)
        MaxFriendsReached           = 30002,
        /// Maximum number of pins reached (50)
        MaxPinsReached              = 30003,
        /// Maximum number of guild roles reached (250)
        MaxRolesReached             = 30005,
        /// Maximum number of reactions reached (20)
        MaxReactionsReached         = 30010,
        /// Maximum number of guild channels reached (500)
        MaxChannelsReached          = 30013,
        Unauthorized                = 40001,
        MissingAccess               = 50001,
        InvalidAccountType          = 50002,
        /// Cannot execute action on a DM channel
        InvalidActionForDM          = 50003,
        WidgetDisabled              = 50004,
        /// Cannot edit a message authored by another user
        CannotEditMessage           = 50005,
        /// Cannot send an empty message
        CannotSendEmpty             = 50006,
        /// Cannot send messages to this user
        CannotMessageUser           = 50007,
        /// Cannot send messages in a voice channel
        CannotMessageVoice          = 50008,
        /// Channel verification level is too high
        VerificationLevelTooHigh    = 50009,
        /// OAuth2 application does not have a bot
        OAuth2DoesNotHaveBot        = 50010,
        /// OAuth2 application limit reached
        OAuth2LimitReached          = 50011,
        InvalidOAuthState           = 50012,
        MissingPermissions          = 50013,
        /// Invalid authentication token
        InvalidAuthentication       = 50014,
        /// Note is too long
        NoteTooLong                 = 50015,
        /// Provided too few or too many messages to delete. Must provide at least 2 and fewer than 100 messages to delete.
        InvalidBulkDelete           = 50016,
        /// A message can only be pinned to the channel it was sent in
        InvalidChannelPin           = 50019,
        /// Cannot execute action on a system message
        InvalidAction               = 50021,
        /// A message provided was too old to bulk delete
        TooOldToDelete              = 50034,
        InvalidFormBody             = 50035,
        /// An invite was accepted to a guild the application's bot is not in
        InvalidInvitation           = 50036,
        InvalidAPIVersion           = 50041,
        ReactionBlocked             = 90001,
    }
}

impl_serde_for_enum_primitive!(JsonResponseCode);

enum_from_primitive! {
    /// RPC is the local Discord server running on localhost.
    ///
    /// Access to the RPC server is gated behind a whitelist.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum RPCErrorCode {
        /// sent when an unknown error occurred
        UnknownError                    = 1000,
        /// sent when an invalid payload is received
        InvalidPayload                  = 4000,
        /// sent when the command name specified is invalid
        InvalidCommand                  = 4002,
        /// sent when the guild id specified is invalid
        InvalidGuild                    = 4003,
        /// sent when the event name specified is invalid
        InvalidEvent                    = 4004,
        /// sent when the channel id specified is invalid
        InvalidChannel                  = 4005,
        /// sent when the user doesn't have the permission required to access the requested resource
        InvalidPermissions              = 4006,
        /// sent when an invalid OAuth2 application ID is used to authorize or authenticate with
        InvalidClientID                 = 4007,
        /// sent when an invalid OAuth2 application origin is used to authorize or authenticate with
        InvalidOrigin                   = 4008,
        /// sent when an invalid OAuth2 token is used to authorize or authenticate with
        InvalidToken                    = 4009,
        /// sent when the user id specified is invalid
        InvalidUser                     = 4010,
        /// sent when a standard OAuth2 error occurred; check the data object for the OAuth 2 error information
        OAuth2Error                     = 5000,
        /// sent when an asyncronous SELECT_TEXT_CHANNEL/SELECT_VOICE_CHANNEL command times out
        SelectChannelTimedOut           = 5001,
        /// sent when an asyncronous GET_GUILD command times out
        GetGuildTimedOut                = 5002,
        /// sent when you try to join a user to a voice channel but the user is already in one
        SelectVoiceForceRequired        = 5003,
        /// sent when you try to capture a shortcut key when already capturing one
        CaptureShortcutAlreadyListening = 5004,
    }
}

enum_from_primitive! {
    /// RPC is the local Discord server running on localhost.
    ///
    /// Access to the RPC server is gated behind a whitelist.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum RCPCloseEvent {
        /// sent when you connect to the RPC server with an invalid client ID
        InvalidClientID = 4000,
        /// sent when you connect to the RPC server with an invalid origin
        InvalidOrigin   = 4001,
        /// sent when the RPC Server rejects your connection to a ratelimit
        Ratelimited     = 4002,
        /// sent when the OAuth2 token associated with a connection is revoked
        TokenRevoke     = 4003,
        /// sent when the RPC Server version specified in the connection string is not valid
        InvalidVersion  = 4004,
        /// sent when the encoding specified in the connection string is not valid
        InvalidEncoding = 4005,
    }
}

impl_serde_for_enum_primitive!(RPCErrorCode);
impl_serde_for_enum_primitive!(RCPCloseEvent);
