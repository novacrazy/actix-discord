//! Handling of Gateway payloads

use crate::models::gateway::codes::GatewayOpcode;

#[derive(Debug, Clone)]
pub struct GatewayPayload {
    pub op: GatewayOpcode,

    //#[serde(rename = "d")]
    pub data: (),

    //#[serde(default, rename = "s", skip_serializing_if = "Option::is_none")]
    pub sequence: Option<u64>,

    //#[serde(default, rename = "t", skip_serializing_if = "Option::is_none")]
    pub event: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum GatewayPayloadData {}
