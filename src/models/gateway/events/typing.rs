//! Sent when a user starts typing in a channel.

use crate::models::channel::ChannelId;
use crate::models::guild::GuildId;
use crate::models::user::UserId;

use crate::models::timestamp::UnixTimestamp;

use crate::models::gateway::codes::GatewayOpcode;
use crate::models::gateway::events::GatewayEvent;

/// Sent when a user starts typing in a channel.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TypingStart {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub channel_id: Option<ChannelId>,

    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub guild_id: Option<GuildId>,

    pub user_id: UserId,

    /// unix time (in seconds) of when the user started typing
    pub timestamp: UnixTimestamp,
}

impl GatewayEvent for TypingStart {
    fn opcode() -> GatewayOpcode {
        GatewayOpcode::Dispatch
    }
}
