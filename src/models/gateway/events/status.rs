use crate::models::presence::Activity;
use crate::models::timestamp::UnixTimestamp;

use crate::models::gateway::codes::GatewayOpcode;
use crate::models::gateway::events::GatewayEvent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateStatus {
    // unix time (in milliseconds) of when the client went idle,
    // or `None` if the client is not idle
    #[serde(default)]
    pub since: Option<UnixTimestamp>,

    /// the user's new activity, if any.
    ///
    /// Renamed from "game" in Discord's API
    #[serde(default, rename = "game")]
    pub activity: Option<Activity>,

    /// the user's new status
    pub status: Status,

    /// whether or not the client is afk
    #[serde(default)]
    pub afk: bool,
}

impl GatewayEvent for UpdateStatus {
    fn opcode() -> GatewayOpcode {
        GatewayOpcode::StatusUpdate
    }
}

/// User status
///
/// https://discordapp.com/developers/docs/topics/gateway#update-status-status-types
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    /// Online
    Online,

    /// Do Not Disturb
    #[serde(rename = "dnd")]
    DoNotDisturb,

    /// AFK
    Idle,

    /// Invisible and shown as offline
    Invisible,

    /// Offline
    Offline,
}

impl From<Status> for UpdateStatus {
    fn from(status: Status) -> UpdateStatus {
        UpdateStatus {
            since: None,
            activity: None,
            status,
            afk: status == Status::Idle,
        }
    }
}
