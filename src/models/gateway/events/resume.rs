use crate::models::gateway::codes::GatewayOpcode;
use crate::models::gateway::events::GatewayEvent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Resume {
    pub session_token: String,

    pub session_id: String,

    #[serde(rename = "seq")]
    pub last_sequence: u64,
}

impl GatewayEvent for Resume {
    fn opcode() -> GatewayOpcode {
        GatewayOpcode::Resume
    }
}
