use crate::models::guild::role::RoleId;
use crate::models::guild::GuildId;
use crate::models::presence::Activity;
use crate::models::user::User;

use crate::models::gateway::events::status::Status;

/// A user's presence is their current state on a guild.
///
/// This event is sent when a user's presence or info,
/// such as name or avatar, is updated.
#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct PresenceUpdate {
    /// the user presence is being updated for
    user: User,

    /// roles this user is in
    roles: Vec<RoleId>,

    /// the user's current primary activity, if any
    #[serde(default, skip_serializing_if = "Option::is_none")]
    game: Option<Activity>,

    /// id of the guild
    guild_id: GuildId,

    /// User status
    status: Status,

    /// user's current activities
    activities: Vec<Activity>,
}
