use crate::models::gateway::codes::GatewayOpcode;

pub trait GatewayEvent {
    fn opcode() -> GatewayOpcode;
}

pub mod guild;
pub mod heartbeat;
pub mod identify;
pub mod presence;
pub mod resume;
pub mod status;
pub mod typing;
