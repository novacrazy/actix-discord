use std::ops::Deref;

use chrono::{DateTime, Utc};

use crate::models::channel::{Channel, ChannelId};
use crate::models::guild::member::Member;
use crate::models::guild::Guild;
use crate::models::id::IdTable;
use crate::models::voice::VoiceState;

use crate::models::gateway::events::presence::PresenceUpdate;

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct GuildInit {
    /// when this guild was joined at
    pub joined_at: DateTime<Utc>,

    /// whether this is considered a large guild
    pub large: bool,

    /// total number of members in this guild
    pub member_count: u64,

    /// (without the guild_id key)
    pub voice_states: Vec<VoiceState>,

    /// users in the guild
    pub members: Vec<Member>,

    /// channels in the guild
    pub channels: IdTable<ChannelId, Channel>,

    /// presences of the users in the guild
    pub presences: Vec<PresenceUpdate>,
}

/// This event can be sent in three different scenarios:
///
/// 1. When a user is initially connecting, to lazily load and backfill information for all unavailable guilds sent in the Ready event.
/// 2. When a Guild becomes available again to the client.
/// 3. When the current user joins a new Guild.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GuildCreate {
    #[serde(flatten)]
    pub guild: Guild,

    /// Extra fields only present for available
    /// guilds on `GUILD_CREATE` events.
    #[serde(flatten, default)]
    pub init: Option<GuildInit>,
}

impl Deref for GuildCreate {
    type Target = Guild;

    fn deref(&self) -> &Guild {
        &self.guild
    }
}
