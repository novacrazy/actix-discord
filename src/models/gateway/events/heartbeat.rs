use crate::models::gateway::codes::GatewayOpcode;
use crate::models::gateway::events::GatewayEvent;

/// Used to maintain an active gateway connection.
///
/// Must be sent every heartbeat_interval milliseconds after
/// the Opcode 10 Hello payload is received.
///
/// The inner d key is the last sequence number—s—received by the client.
///
/// If you have not yet received one, send null.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Heartbeat(Option<u64>);

impl GatewayEvent for Heartbeat {
    fn opcode() -> GatewayOpcode {
        GatewayOpcode::Heartbeat
    }
}
