use crate::identity::Identity;

use crate::models::gateway::codes::GatewayOpcode;
use crate::models::gateway::events::GatewayEvent;

use crate::models::gateway::events::status::UpdateStatus;

/// Used to trigger the initial handshake with the gateway.
///
/// https://discordapp.com/developers/docs/topics/gateway#identify
#[derive(Debug, Clone, Serialize)]
pub struct Identify {
    /// authentication token
    pub token: Identity,

    pub properties: ConnectionProperties,

    /// whether this connection supports compression of packets
    #[serde(default)]
    pub compress: bool,

    /// value between 50 and 250, total number of members where the
    /// gateway will stop sending offline members in the guild member list
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub large_threshold: Option<u8>,

    /// used for Guild Sharding
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub shard: Option<[u64; 2]>,

    /// presence structure for initial presence information
    #[serde(default, rename = "presence", skip_serializing_if = "Option::is_none")]
    pub status: Option<UpdateStatus>,
}

impl GatewayEvent for Identify {
    fn opcode() -> GatewayOpcode {
        GatewayOpcode::Identify
    }
}

/// https://discordapp.com/developers/docs/topics/gateway#identify-identify-connection-properties
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ConnectionProperties {
    /// your operating system
    #[serde(rename = "$os")]
    pub os: String,

    /// your library name
    #[serde(rename = "$browser")]
    pub browser: String,

    /// your library name
    #[serde(rename = "$device")]
    pub device: String,
}

impl ConnectionProperties {
    pub fn new<A, B, C>(os: A, browser: B, device: C) -> ConnectionProperties
    where
        A: Into<String>,
        B: Into<String>,
        C: Into<String>,
    {
        ConnectionProperties {
            os: os.into(),
            browser: browser.into(),
            device: device.into(),
        }
    }
}
