pub mod codes;
pub mod events;
pub mod payload;

use crate::internal::Sealed;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct GatewayEndpoint {
    pub url: String,

    #[serde(default)]
    pub shards: u32,
}

impl Sealed for GatewayEndpoint {}
