use std::fmt;
use std::iter::{FromIterator, IntoIterator};
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

use hashbrown::HashMap;

use serde::{
    de::{self, Deserialize, Deserializer, SeqAccess},
    ser::{Serialize, SerializeSeq, Serializer},
};

use crate::models::snowflake::IsSnowFlake;

pub trait HasId<T: IsSnowFlake> {
    fn id(&self) -> T;
}

impl<'a, T, Id: IsSnowFlake> HasId<Id> for &'a T
where
    T: HasId<Id>,
{
    #[inline(always)]
    fn id(&self) -> Id {
        (**self).id()
    }
}

impl<'a, T, Id: IsSnowFlake> HasId<Id> for &'a mut T
where
    T: HasId<Id>,
{
    #[inline(always)]
    fn id(&self) -> Id {
        (**self).id()
    }
}

impl<T, Id: IsSnowFlake> HasId<Id> for Arc<T>
where
    T: HasId<Id>,
{
    #[inline]
    fn id(&self) -> Id {
        (**self).id()
    }
}

#[inline]
pub fn get_id<H, T>(value: H) -> T
where
    H: HasId<T>,
    T: IsSnowFlake,
{
    value.id()
}

/// Maps a sequence of objects with ids to a
/// hashtable where their ids are the key
///
/// The `IdTable` is serialized and deserialized as a sequence.
#[derive(Debug, Clone)]
pub struct IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    table: HashMap<Id, T>,
}

impl<Id, T> IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    #[inline(always)]
    pub(crate) fn is_empty(&self) -> bool {
        self.table.is_empty()
    }
}

impl<Id, T> FromIterator<T> for IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = T>,
    {
        IdTable {
            table: HashMap::from_iter(iter.into_iter().map(|value| (value.id(), value))),
        }
    }
}

impl<Id, T> Default for IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    fn default() -> IdTable<Id, T> {
        IdTable {
            table: HashMap::default(),
        }
    }
}

impl<Id, T> Deref for IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    type Target = HashMap<Id, T>;

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.table
    }
}

impl<Id, T> DerefMut for IdTable<Id, T>
where
    T: HasId<Id>,
    Id: IsSnowFlake,
{
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.table
    }
}

impl<Id, T> Serialize for IdTable<Id, T>
where
    T: HasId<Id> + Serialize,
    Id: IsSnowFlake,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.table.len()))?;

        for item in self.table.values() {
            seq.serialize_element(item)?;
        }

        seq.end()
    }
}

impl<'de, Id, T> Deserialize<'de> for IdTable<Id, T>
where
    T: HasId<Id> + Deserialize<'de>,
    Id: IsSnowFlake,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct Visitor<'de, Id, T>
        where
            T: HasId<Id> + Deserialize<'de>,
            Id: IsSnowFlake,
        {
            _marker: PhantomData<(&'de Id, T)>,
        };

        impl<'de, Id, T> Default for Visitor<'de, Id, T>
        where
            T: HasId<Id> + Deserialize<'de>,
            Id: IsSnowFlake,
        {
            #[inline(always)]
            fn default() -> Visitor<'de, Id, T> {
                Visitor {
                    _marker: PhantomData,
                }
            }
        }

        impl<'de, Id, T> de::Visitor<'de> for Visitor<'de, Id, T>
        where
            T: HasId<Id> + Deserialize<'de>,
            Id: IsSnowFlake,
        {
            type Value = IdTable<Id, T>;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("a sequence")
            }

            fn visit_seq<S>(self, mut seq: S) -> Result<Self::Value, S::Error>
            where
                S: SeqAccess<'de>,
            {
                let mut table: Self::Value = IdTable {
                    table: HashMap::with_capacity(seq.size_hint().unwrap_or(0).min(4096)),
                };

                while let Some(value) = seq.next_element::<T>()? {
                    table.insert(value.id(), value);
                }

                Ok(table)
            }
        }

        deserializer.deserialize_seq(Visitor::default())
    }
}
