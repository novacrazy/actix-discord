use std::sync::Arc;

use crate::models::channel::ChannelId;
use crate::models::guild::{member::Member, GuildId};
use crate::models::user::UserId;

/// Used to represent a user's voice connection status.
#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct VoiceState {
    /// the guild id this voice state is for
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub guild_id: Option<GuildId>,

    /// the channel id this user is connected to
    #[access(copy)]
    pub channel_id: Option<ChannelId>,

    /// the user id this voice state is for
    #[access(copy)]
    pub user_id: UserId,

    /// the guild member this voice state is for
    #[access(clone)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub member: Option<Arc<Member>>,

    /// the session id for this voice state
    pub session_id: String,

    /// whether this user is deafened by the server
    pub deaf: bool,

    /// whether this user is muted by the server
    pub mute: bool,

    /// whether this user is locally deafened
    pub self_deaf: bool,

    /// whether this user is locally muted
    pub self_mute: bool,

    /// whether this user is muted by the current user
    pub suppress: bool,
}

pub type VoiceRegionId = String;

#[derive(Debug, Clone, AutoAccessor, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct VoiceRegion {
    /// unique ID for the region
    pub id: VoiceRegionId,

    /// name of the region
    pub name: String,

    /// true if this is a vip-only server
    pub vip: bool,

    /// true for a single server that is closest to the current user's client
    pub optimal: bool,

    /// whether this is a deprecated voice region (avoid switching to these)
    pub deprecated: bool,

    /// whether this is a custom voice region (used for events/etc)
    pub custom: bool,
}

impl crate::internal::Sealed for VoiceRegion {}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json as json;

    #[test]
    fn voice_state() {
        const EXAMPLE_VOICE_STATE: &str = r#"
        {
            "channel_id": "157733188964188161",
            "user_id": "80351110224678912",
            "session_id": "90326bd25d71d39b9ef95b299e3872ff",
            "deaf": false,
            "mute": false,
            "self_deaf": false,
            "self_mute": true,
            "suppress": false
        }
        "#;

        let _: VoiceState =
            json::from_str(EXAMPLE_VOICE_STATE).expect("Failed to parse example voice state");
    }
}
