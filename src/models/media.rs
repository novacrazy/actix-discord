pub struct Avatar {}

impl Avatar {}

#[derive(Debug, Clone, Copy, AutoAccessor, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Dimensions {
    pub width: u64,
    pub height: u64,
}

#[cfg(feature = "image")]
mod image_features {
    use image;
}
