use chrono::{DateTime, Utc};

use crate::models::application::ApplicationId;
use crate::models::common::Mentionable;
use crate::models::guild::GuildId;
use crate::models::id::IdTable;
use crate::models::message::MessageId;
use crate::models::permission::{Overwrite, OverwriteId};
use crate::models::snowflake::IsSnowFlake;
use crate::models::user::UserId;

declare_snowflake_newtype! {
    /// Unique Channel ID
    ChannelId:+ Channel
}

pub type ChannelName = smallstr::SmallString<[u8; 64]>;

#[derive(Debug, AutoAccessor, Clone, Serialize, Deserialize)]
pub struct Channel {
    /// the id of this channel
    #[serde(skip_serializing_if = "IsSnowFlake::is_null")]
    #[access(copy)]
    pub id: ChannelId,

    /// the name of the channel (2-100 characters)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    // cannot be null, but can be nonexistent
    pub name: Option<ChannelName>,

    /// the type of channel
    #[access(copy)]
    #[serde(rename = "type")]
    pub kind: ChannelType,

    /// the id of the guild
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none", rename = "guild")]
    pub guild_id: Option<GuildId>,

    /// sorting position of the channel
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub position: Option<u64>,

    /// explicit permission overwrites for members and roles
    #[serde(
        default,
        skip_serializing_if = "IdTable::is_empty",
        rename = "permission_overwrites"
    )]
    pub overwrites: IdTable<OverwriteId, Overwrite>,

    /// the channel topic (0-1024 characters)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub topic: Option<String>,

    /// whether the channel is nsfw
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub nsfw: Option<bool>,

    /// the id of the last message sent in this channel (may not point to an existing or valid message)
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub last_message_id: Option<MessageId>,

    /// the bitrate (in bits) of the voice channel
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub bitrate: Option<u64>,

    /// the user limit of the voice channel
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user_limit: Option<u64>,

    /// amount of seconds a user has to wait before sending another message
    /// (0-120); bots, as well as users with the
    /// permission `manage_messages` or `manage_channel`, are unaffected
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub rate_limit_per_user: Option<u64>,

    /// icon hash
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub icon: Option<String>,

    /// id of the DM creator
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub owner: Option<UserId>,

    /// application id of the group DM creator if it is bot-created
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub application: Option<ApplicationId>,

    /// id of the parent category for a channel
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub parent: Option<ChannelId>,

    /// when the last pinned message was pinned
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub last_pin_timestamp: Option<DateTime<Utc>>,
}

impl Mentionable for Channel {
    fn mention(&self) -> Option<String> {
        Some(format!("<#{}>", self.id()))
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum ChannelType {
        GuildText           = 0,
        DirectMessage       = 1,
        GuildVoice          = 2,
        GroupDirectMessage  = 3,
        GuildCategory       = 4,
    }
}

impl Default for ChannelType {
    fn default() -> ChannelType {
        ChannelType::GuildText
    }
}

impl_serde_for_enum_primitive!(ChannelType);

#[cfg(test)]
mod test {
    use super::*;

    use serde_json as json;

    #[test]
    fn guild_text_channel() {
        const EXAMPLE_GUILD_TEXT_CHANNEL: &str = r#"
        {
            "id": "41771983423143937",
            "guild_id": "41771983423143937",
            "name": "general",
            "type": 0,
            "position": 6,
            "permission_overwrites": [],
            "rate_limit_per_user": 2,
            "nsfw": true,
            "topic": "24/7 chat about how to gank Mike #2",
            "last_message_id": "155117677105512449",
            "parent_id": "399942396007890945"
        }
        "#;

        let channel: Channel = json::from_str(EXAMPLE_GUILD_TEXT_CHANNEL)
            .expect("Unable to parse example guild text channel");

        assert_eq!(channel.kind(), ChannelType::GuildText);
    }

    #[test]
    fn guild_voice_channel() {
        const EXAMPLE_GUILD_VOICE_CHANNEL: &str = r#"
        {
            "id": "155101607195836416",
            "guild_id": "41771983423143937",
            "name": "ROCKET CHEESE",
            "type": 2,
            "nsfw": false,
            "position": 5,
            "permission_overwrites": [],
            "bitrate": 64000,
            "user_limit": 0,
            "parent_id": null
        }
        "#;

        let _: Channel = json::from_str(EXAMPLE_GUILD_VOICE_CHANNEL)
            .expect("Unable to parse example guild voice channel");
    }

    #[test]
    fn dm_channel() {
        const EXAMPLE_DM_CHANNEL: &str = r#"
        {
            "last_message_id": "3343820033257021450",
            "type": 1,
            "id": "319674150115610528",
            "recipients": [
                {
                    "username": "test",
                    "discriminator": "9999",
                    "id": "82198898841029460",
                    "avatar": "33ecab261d4681afa4d85a04691c4a01"
                }
            ]
        }
        "#;

        let _: Channel =
            json::from_str(EXAMPLE_DM_CHANNEL).expect("Unable to parse example DM channel");
    }

    #[test]
    fn group_dm_channel() {
        const EXAMPLE_GROUP_DM_CHANNEL: &str = r#"
        {
            "name": "Some test channel",
            "icon": null,
            "recipients": [
                {
                    "username": "test",
                    "discriminator": "9999",
                    "id": "82198898841029460",
                    "avatar": "33ecab261d4681afa4d85a04691c4a01"
                },
                {
                    "username": "test2",
                    "discriminator": "9999",
                    "id": "82198810841029460",
                    "avatar": "33ecab261d4681afa4d85a10691c4a01"
                }
            ],
            "last_message_id": "3343820033257021450",
            "type": 3,
            "id": "319674150115710528",
            "owner_id": "82198810841029460"
        }
        "#;

        let _: Channel = json::from_str(EXAMPLE_GROUP_DM_CHANNEL)
            .expect("Unable to parse example group DM channel");
    }
}
