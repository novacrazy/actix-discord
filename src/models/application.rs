declare_snowflake_newtype! {
    /// Unique Application ID
    ApplicationId
}
