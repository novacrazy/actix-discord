//! Data structures used by Discord

#[macro_use]
pub mod snowflake;
pub mod timestamp;

pub mod application;
pub mod audit;
pub mod channel;
pub mod common;
pub mod emoji;
pub mod gateway;
pub mod guild;
pub mod id;
pub mod invite;
pub mod media;
pub mod message;
pub mod permission;
pub mod presence;
pub mod resource;
pub mod user;
pub mod voice;
pub mod webhook;

pub mod prelude {
    pub use crate::models::{
        audit::AuditLog, channel::Channel, emoji::Emoji, guild::Guild, invite::Invite,
        message::Message, permission::Permission, presence::Activity, user::User, webhook::Webhook,
    };
}
