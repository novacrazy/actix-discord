//! Naive Unix timestamp handling

use std::ops;

use chrono::NaiveDateTime;

/// A naive Unix timestamp that serializes to/from milliseconds
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct UnixTimestamp(#[serde(with = "chrono::naive::serde::ts_milliseconds")] NaiveDateTime);

impl UnixTimestamp {
    /// Create a new instance from milliseconds from the Unix epoch
    pub fn from_millis(millis: u64) -> UnixTimestamp {
        UnixTimestamp(NaiveDateTime::from_timestamp(
            (millis / 1000) as i64,
            ((millis % 1000) * 1_000_000) as u32,
        ))
    }
}

impl From<NaiveDateTime> for UnixTimestamp {
    fn from(naive: NaiveDateTime) -> UnixTimestamp {
        UnixTimestamp(naive)
    }
}

impl ops::Deref for UnixTimestamp {
    type Target = NaiveDateTime;

    #[inline]
    fn deref(&self) -> &NaiveDateTime {
        &self.0
    }
}

impl PartialEq<NaiveDateTime> for UnixTimestamp {
    fn eq(&self, rhs: &NaiveDateTime) -> bool {
        self.0 == *rhs
    }
}
