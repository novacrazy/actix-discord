//! `SnowFlake` unique identifier type

#![allow(clippy::trivially_copy_pass_by_ref)]

use std::fmt;
use std::hash::Hash;
use std::num::ParseIntError;
use std::str::FromStr;

use serde::{
    de::{self, Deserialize, Deserializer},
    ser::{Serialize, Serializer},
};

use crate::models::timestamp::UnixTimestamp;

use crate::internal::Sealed;

pub trait IsSnowFlake:
    Sealed + fmt::Debug + Clone + Copy + PartialEq + Eq + Hash + 'static
{
    fn raw(&self) -> SnowFlake;

    fn is_null(&self) -> bool {
        self.raw() == SnowFlake::default()
    }
}

pub trait IdFor: Sealed {
    type Model;
}

/// Discord utilizes Twitter's snowflake format for uniquely
/// identifiable descriptors (IDs). These IDs are guaranteed to
/// be unique across all of Discord, except in some unique scenarios
/// in which child objects share their parent's ID. Because Snowflake IDs
/// are up to 64 bits in size (e.g. a uint64), they are always returned
/// as strings in the HTTP API to prevent integer overflows in some languages.
///
/// See Gateway ETF/JSON for more information regarding Gateway encoding.
///
/// [https://discordapp.com/developers/docs/reference#snowflakes](https://discordapp.com/developers/docs/reference#snowflakes)
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct SnowFlake(pub u64);

impl Sealed for SnowFlake {}

impl SnowFlake {
    /// Milliseconds since Discord Epoch, the first second of 2015 or 1420070400000.
    #[inline]
    pub fn timestamp_discord_epoch(&self) -> u64 {
        self.0 >> 22
    }

    /// Unix timestamp
    #[inline]
    pub fn timestamp(&self) -> UnixTimestamp {
        UnixTimestamp::from_millis(self.timestamp_discord_epoch() + 1420070400000)
    }

    /// Internal worker ID
    #[inline]
    pub fn worker(&self) -> u64 {
        (self.0 & 0x3E0000) >> 17
    }

    /// Internal process ID
    #[inline]
    pub fn process(&self) -> u64 {
        (self.0 & 0x1F000) >> 12
    }

    /// For every ID that is generated on that process, this number is incremented.
    #[inline]
    pub fn increment(&self) -> u64 {
        self.0 & 0xFFF
    }

    /// Checks if the ID is valid
    #[inline]
    pub fn is_valid(&self) -> bool {
        *self != Self::default()
    }

    /// Checks if the ID is invalid
    #[inline]
    pub fn is_invalid(&self) -> bool {
        *self == Self::default()
    }
}

impl Into<u64> for SnowFlake {
    #[inline(always)]
    fn into(self) -> u64 {
        self.0
    }
}

impl IsSnowFlake for SnowFlake {
    #[inline]
    fn raw(&self) -> SnowFlake {
        *self
    }
}

impl fmt::Display for SnowFlake {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

impl FromStr for SnowFlake {
    type Err = ParseIntError;

    #[inline(always)]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().map(SnowFlake)
    }
}

impl Serialize for SnowFlake {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0.to_string())
    }
}

impl<'de> Deserialize<'de> for SnowFlake {
    fn deserialize<D>(deserializer: D) -> Result<SnowFlake, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> de::Visitor<'de> for Visitor {
            type Value = SnowFlake;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("a 64-bit integer or string that can be parsed into one")
            }

            fn visit_str<E>(self, value: &str) -> Result<SnowFlake, E>
            where
                E: de::Error,
            {
                match value.parse() {
                    Ok(sf) => Ok(SnowFlake(sf)),
                    Err(e) => Err(E::custom(format!("{}", e))),
                }
            }

            fn visit_u64<E>(self, value: u64) -> Result<SnowFlake, E>
            where
                E: de::Error,
            {
                Ok(SnowFlake(value))
            }
        }

        deserializer.deserialize_any(Visitor)
    }
}

macro_rules! declare_snowflake_newtype {
    ($(#[$($attrs:tt)*])* $name:ident $(: $model:ty)* $(:+ $model2:ty)*) => {
        $(#[$($attrs)*])*
        #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
        #[serde(transparent)]
        pub struct $name($crate::models::snowflake::SnowFlake);

        impl $name {
            #[inline]
            pub unsafe fn cast<T>(self) -> T
            where
                T: From<$crate::models::snowflake::SnowFlake>
            {
                T::from(self.0)
            }
        }

        impl $crate::models::snowflake::IsSnowFlake for $name {
            #[inline]
            fn raw(&self) -> $crate::models::snowflake::SnowFlake {
                self.0
            }
        }

        impl $crate::internal::Sealed for $name {}

        $(
            impl $crate::internal::Sealed for $model {}

            impl $crate::models::snowflake::IdFor for $name {
                type Model = $model;
            }
        )*

        $(
            impl $crate::internal::Sealed for $model2 {}

            impl $crate::models::snowflake::IdFor for $name {
                type Model = $model2;
            }

            impl $crate::models::id::HasId<$name> for $model2 {
                #[inline]
                fn id(&self) -> $name {
                    self.id()
                }
            }
        )*

        impl std::ops::Deref for $name {
            type Target = $crate::models::snowflake::SnowFlake;

            #[inline]
            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl PartialEq<$crate::models::snowflake::SnowFlake> for $name {
            #[inline]
            fn eq(&self, rhs: &$crate::models::snowflake::SnowFlake) -> bool {
                self.0 == *rhs
            }
        }

        impl From<$crate::models::snowflake::SnowFlake> for $name {
            #[inline]
            fn from(snowflake: $crate::models::snowflake::SnowFlake) -> $name {
                $name(snowflake)
            }
        }

        impl From<$name> for $crate::models::snowflake::SnowFlake {
            #[inline]
            fn from(subtype: $name) -> $crate::models::snowflake::SnowFlake {
                subtype.0
            }
        }

        impl Into<u64> for $name {
            #[inline(always)]
            fn into(self) -> u64 {
                self.0.into()
            }
        }

        impl std::fmt::Display for $name {
            #[inline]
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                std::fmt::Display::fmt(&self.0, f)
            }
        }

        impl std::str::FromStr for $name {
            type Err = std::num::ParseIntError;

            #[inline(always)]
            fn from_str(s: &str) -> Result<Self, Self::Err> {
                s.parse().map($name)
            }
        }
    };
}
