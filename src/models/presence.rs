use crate::models::application::ApplicationId;
use crate::models::timestamp::UnixTimestamp;

/// Bots are only able to send `name`, `type`/`kind`, and optionally `url`.
#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Activity {
    Bearer {
        /// the activity's name
        name: String,

        /// activity type
        #[access(copy)]
        #[serde(rename = "type")]
        kind: ActivityType,

        /// stream url, is validated when type is 1
        #[serde(default, skip_serializing_if = "Option::is_none")]
        url: Option<String>,

        /// unix timestamps for start and/or end of the game
        #[access(copy)]
        #[serde(default, skip_serializing_if = "Option::is_none")]
        timestamps: Option<ActivityTimestamps>,

        /// application id for the game
        #[access(copy)]
        #[serde(default, skip_serializing_if = "Option::is_none")]
        application_id: Option<ApplicationId>,

        /// what the player is currently doing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        details: Option<String>,

        /// the user's current party status
        #[serde(default, skip_serializing_if = "Option::is_none")]
        state: Option<String>,

        /// information for the current party of the player
        #[serde(default, skip_serializing_if = "Option::is_none")]
        party: Option<ActivityParty>,

        /// images for the presence and their hover texts
        #[serde(default, skip_serializing_if = "Option::is_none")]
        assets: Option<ActivityAssets>,

        /// secrets for Rich Presence joining and spectating
        #[serde(default, skip_serializing_if = "Option::is_none")]
        secrets: Option<ActivitySecrets>,

        /// whether or not the activity is an instanced game session
        #[serde(default)]
        instance: bool,

        /// activity flags ORd together, describes what the payload includes
        #[access(copy)]
        #[serde(default, skip_serializing_if = "Option::is_none")]
        flags: Option<ActivityFlag>,
    },
    Bot {
        name: String,

        #[serde(rename = "type")]
        kind: ActivityType,

        #[serde(default, skip_serializing_if = "Option::is_none")]
        url: Option<String>,
    },
}

impl Activity {
    /// Create a simple `Activity` suitable for use by a bot
    #[inline]
    pub fn bot_activity(name: String, kind: ActivityType, url: Option<String>) -> Activity {
        Activity::Bot { name, kind, url }
    }
}

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum ActivityType {
        /// Game. `Playing {name}` e.g. "Playing Rocket League"
        Game        = 0,
        /// Streaming. `Streaming {name}` e.g. "Streaming Rocket League"
        Streaming   = 1,
        /// Listening. `Listening to {name}` e.g. "Listening to Spotify"
        Listening   = 2,
    }
}

impl Default for ActivityType {
    fn default() -> ActivityType {
        ActivityType::Game
    }
}

impl_serde_for_enum_primitive!(ActivityType);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ActivityTimestamps {
    /// unix time (in milliseconds) of when the activity started
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub start: Option<UnixTimestamp>,

    /// unix time (in milliseconds) of when the activity ends
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub end: Option<UnixTimestamp>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ActivityParty {
    /// the id of the party
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// used to show the party's current and maximum size
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub size: Option<[u64; 2]>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ActivityAssets {
    /// the id for a large asset of the activity, usually a snowflake
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub large_image: Option<String>,

    /// text displayed when hovering over the large image of the activity
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub large_text: Option<String>,

    /// the id for a small asset of the activity, usually a snowflake
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub small_image: Option<String>,

    /// text displayed when hovering over the small image of the activity
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub small_text: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ActivitySecrets {
    /// the secret for joining a party
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub join: Option<String>,

    /// the secret for spectating a game
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub spectate: Option<String>,

    /// the secret for a specific instanced match
    #[serde(default, rename = "match", skip_serializing_if = "Option::is_none")]
    pub instanced_match: Option<String>,
}

bitflags! {
    /// activity flags, describes what the payload includes
    #[derive(Default)]
    pub struct ActivityFlag: u32 {
        const INSTANCE      = 1;
        const JOIN          = 1 << 1;
        const SPECTATE      = 1 << 2;
        const JOIN_REQUEST  = 1 << 3;
        const SYNC          = 1 << 4;
        const PLAY          = 1 << 5;
    }
}

impl_serde_for_bitflags!(ActivityFlag);

#[cfg(test)]
mod test {
    use super::*;

    use serde_json as json;

    #[test]
    fn test_activity() {
        const EXAMPLE_ACTIVITY: &str = r#"
        {
            "name": "Rocket League",
            "type": 1,
            "url": "https://www.twitch.tv/123"
        }
        "#;

        let activity: Activity =
            json::from_str(EXAMPLE_ACTIVITY).expect("Failed to parse example activity");

        // It shouldn't parse to a Bot activity even if it matches
        assert!(activity.is_bearer());
    }

    #[test]
    fn test_activity_with_rich_presence() {
        const EXAMPLE_ACTIVITY_WITH_RICH_PRESENCE: &str = r#"
        {
            "name": "Rocket League",
            "type": 0,
            "application_id": "379286085710381999",
            "state": "In a Match",
            "details": "Ranked Duos: 2-1",
            "timestamps": {
                "start": 15112000660000
            },
            "party": {
                "id": "9dd6594e-81b3-49f6-a6b5-a679e6a060d3",
                "size": [2, 2]
            },
            "assets": {
                "large_image": "351371005538729000",
                "large_text": "DFH Stadium",
                "small_image": "351371005538729111",
                "small_text": "Silver III"
            },
            "secrets": {
                "join": "025ed05c71f639de8bfaa0d679d7c94b2fdce12f",
                "spectate": "e7eb30d2ee025ed05c71ea495f770b76454ee4e0",
                "match": "4b2fdce12f639de8bfa7e3591b71a0d679d7c93f"
            }
        }
        "#;

        let _: Activity = json::from_str(EXAMPLE_ACTIVITY_WITH_RICH_PRESENCE)
            .expect("Failed to parse example activity with rich presence");
    }
}
