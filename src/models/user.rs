use crate::models::common::Mentionable;
use crate::models::id::IdTable;

use crate::models::guild::integration::{Integration, IntegrationId};

declare_snowflake_newtype! {
    /// Unique User ID
    UserId:+ User
}

pub type UserDiscriminator = smallstr::SmallString<[u8; 4]>;

#[derive(Debug, Clone, Default, AutoAccessor, Serialize, Deserialize)]
pub struct User {
    /// the user's id
    #[access(copy)]
    pub id: UserId,

    /// the user's username, not unique across the platform
    pub username: String,

    /// the user's 4-digit discord-tag
    pub discriminator: UserDiscriminator,

    /// the user's avatar hash
    #[serde(default)]
    pub avatar: Option<String>,

    /// whether the user belongs to an OAuth2 application
    #[serde(default)]
    pub bot: bool,

    /// whether the user has two factor enabled on their account
    #[serde(default)]
    pub mfa_enabled: bool,

    /// the user's chosen language option
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub locale: Option<String>,

    /// whether the email on this account has been verified
    #[serde(default)]
    pub verified: bool,

    /// the user's email
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,

    /// the flags on a user's account
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Flag::is_empty")]
    pub flags: Flag,

    /// the type of Nitro subscription on a user's account
    #[access(copy)]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub premium_type: Option<PremiumType>,
}

impl Mentionable for User {
    fn mention(&self) -> Option<String> {
        Some(format!("<@{}>", self.id))
    }
}

bitflags! {
    #[derive(Default)]
    pub struct Flag: u32 {
        /// Discord Employee
        const DISCORD_EMPLOYEE  = 1;

        /// Dicsord Partner
        const DISCORD_PARTNER   = 1 << 1;

        /// HypeSquad Events
        const HYPE_SQUAD        = 1 << 2;

        /// Bug Hunter
        const BUG_HUNTER        = 1 << 3;

        /// House Bravery
        const HOUSE_BRAVERY     = 1 << 6;

        /// House Brilliance
        const HOUSE_BRILLIANCE  = 1 << 7;

        /// House Balance
        const HOUSE_BALANCE     = 1 << 8;

        /// Early Supporter
        const EARLY_SUPPORTER   = 1 << 9;
    }
}

impl_serde_for_bitflags!(Flag);

enum_from_primitive! {
    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub enum PremiumType {
        NitroClassic    = 0,
        Nitro           = 1,
    }
}

impl_serde_for_enum_primitive!(PremiumType);

declare_snowflake_newtype! {
    /// Unique Connection ID
    ConnectionId:+ Connection
}

#[derive(Debug, Clone, AutoAccessor, Serialize, Deserialize)]
pub struct Connection {
    /// id of the connection account
    #[access(copy)]
    pub id: ConnectionId,

    /// the username of the connection account
    pub name: String,

    /// the service of the connection (twitch, youtube)
    #[serde(rename = "type")]
    pub kind: String,

    /// whether the connection is revoked
    pub revoked: bool,

    /// an array of partial server integrations
    pub integration: IdTable<IntegrationId, Integration>,
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::models::snowflake::SnowFlake;

    use serde_json as json;

    #[test]
    fn parse_example_user() {
        const EXAMPLE_USER: &str = r#"{
            "id": "80351110224678912",
            "username": "Nelly",
            "discriminator": "1337",
            "avatar": "8342729096ea3675442027381ff50dfe",
            "verified": true,
            "email": "nelly@discordapp.com"
        }"#;

        let u: User = json::from_str(EXAMPLE_USER).expect("Could not parse example user!");

        assert_eq!(u.id, SnowFlake(80351110224678912));
        assert_eq!(u.username, "Nelly");
        assert_eq!(u.bot, false);
        assert_eq!(u.email, Some("nelly@discordapp.com".to_owned()));
    }
}
