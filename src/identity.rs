//! Access tokens for bots and bearers

use std::fmt;

use serde::ser::{Serialize, Serializer};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Identity {
    token: String,
    agent: String,
    bot: bool,
}

impl Identity {
    /// Create a new Bearer (Human) Identity with the given token and application agent
    pub fn bearer(token: impl Into<String>, agent: impl Into<String>) -> Identity {
        Identity {
            token: token.into(),
            agent: agent.into(),
            bot: false,
        }
    }

    /// Create a new Bot Identity with the given token and application agent
    pub fn bot(token: impl Into<String>, agent: impl Into<String>) -> Identity {
        Identity {
            token: token.into(),
            agent: agent.into(),
            bot: true,
        }
    }

    /// Create a new Bot Identity and generate the user agent for you from the URL and version
    pub fn bot_auto(
        token: impl Into<String>,
        url: impl AsRef<str>,
        version: impl AsRef<str>,
    ) -> Identity {
        Identity {
            token: token.into(),
            agent: format!("DiscordBot ({}, {})", url.as_ref(), version.as_ref()),
            bot: true,
        }
    }

    /// Returns `true` if the identity is a Bearer (Human)
    #[inline]
    pub fn is_bearer(&self) -> bool {
        !self.bot
    }

    /// Return `true` if the identity is a Bot
    #[inline]
    pub fn is_bot(&self) -> bool {
        self.bot
    }

    /// Return the token
    #[inline]
    pub fn token(&self) -> &String {
        &self.token
    }

    /// Return the agent
    #[inline]
    pub fn agent(&self) -> &String {
        &self.agent
    }

    pub fn format_agent(&self) -> String {
        if self.bot {
            format!("Discord Bot ({})", self.agent)
        } else {
            self.agent.clone()
        }
    }

    #[inline]
    pub fn select<T>(&self, bearer: T, bot: T) -> T {
        if self.bot {
            bot
        } else {
            bearer
        }
    }

    /// Returns either `"Bot"` or `"Bearer"`
    #[inline]
    pub fn format_bot_bearer(&self) -> &'static str {
        self.select("Bearer", "Bot")
    }

    #[inline]
    pub fn authorization(&self) -> String {
        self.to_string()
    }
}

impl fmt::Display for Identity {
    fn fmt(&self, out: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(out, "{} {}", self.format_bot_bearer(), self.token)
    }
}

impl Serialize for Identity {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}
