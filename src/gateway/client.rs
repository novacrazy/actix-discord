use std::fmt;
use std::sync::Arc;
use std::time::Duration;

use serde_json as json;

use actix::prelude::*;
use actix_web::client as http_client;
use actix_web::dev::MessageBody;
use actix_web::ws;
use actix_web::{Body, Error, HttpMessage, HttpResponse};

use futures::{future, Future, Stream};

use crate::models::gateway::GatewayEndpoint;

use crate::api::endpoints::gateway::GetGatewayEndpoint;
use crate::api::{Context as ApiContext, Endpoint, StaticEndpoint};

pub struct Gateway {
    /// Endpoint
    pub endpoint: GatewayEndpoint,

    /// API Context
    pub ctx: ApiContext,

    /// Websocket writer
    pub writer: ws::ClientWriter,

    /// Time between heartbeats
    pub heartbeat_interval: Duration,

    /// Incremental counter
    pub last_sequence: u64,
}

impl fmt::Debug for Gateway {
    fn fmt(&self, out: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            out,
            "{} Gateway at {}",
            self.ctx.identity().format_bot_bearer(),
            self.endpoint.url
        )
    }
}

impl Gateway {
    pub fn open(api_ctx: ApiContext) {
        Arbiter::spawn_fn(move || {
            // TODO: Create persistent connection
            GetGatewayEndpoint
                .exec(&api_ctx)
                .map_err(|e| eprintln!("Unknown Error: {}", e))
                .and_then(move |endpoint| {
                    ws::Client::new(format!(
                        "{}?v={}&encoding=json&compress=zlib-stream",
                        &endpoint.url,
                        super::GATEWAY_VERSION,
                    ))
                    .header("Authorization", api_ctx.identity().authorization())
                    .header("User-Agent", api_ctx.identity().format_agent())
                    .connect()
                    .map_err(|e| eprintln!("Could not connect to gateway: {}", e))
                    .map(|(reader, writer)| {
                        let _addr = Gateway::create(|ctx| {
                            Gateway::add_stream(reader, ctx);

                            Gateway {
                                endpoint,
                                ctx: api_ctx,
                                writer,
                                heartbeat_interval: Duration::from_secs(45), // default for Discord
                                last_sequence: 0,
                            }
                        });
                    })
                })
        });
    }
}

impl Actor for Gateway {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Context<Self>) {}

    fn stopped(&mut self, _ctx: &mut Context<Self>) {}
}

impl StreamHandler<ws::Message, ws::ProtocolError> for Gateway {
    fn handle(&mut self, msg: ws::Message, _ctx: &mut Context<Self>) {
        // parse GatewayPayload from text or binary response
        let _payload: Result<(), _> = match msg {
            ws::Message::Text(ref txt) => json::from_str(txt.as_str()),
            ws::Message::Binary(ref bin) => {
                // Attempt to parse from the raw binary data,
                // otherwise assume it's compressed and decompress it
                json::from_slice(bin.as_ref()).or_else(|_| {
                    use flate2::bufread::ZlibDecoder;
                    use std::io::Read;

                    let mut reader = ZlibDecoder::new(bin.as_ref());
                    let mut decoded = String::with_capacity(1024);

                    // Decode compressed messages
                    reader
                        .read_to_string(&mut decoded)
                        .expect("Invalid Compression");

                    json::from_str(&decoded)
                })
            }
            _ => panic!("Unable to interpret websocket message {:?}", msg),
        };

        // assert that a payload was parsed
        /*
        let _payload = payload.unwrap_or_else(|_e| {
            use std::str::from_utf8_unchecked;

            if let ws::Message::Binary(bin) = msg {
                println!("{}", unsafe { from_utf8_unchecked(bin.as_ref()) });
            }

            panic!("Invalid payload");
        });
        */

        unimplemented!()
    }

    fn started(&mut self, _ctx: &mut Context<Self>) {
        println!("Connected!");
    }

    fn finished(&mut self, ctx: &mut Context<Self>) {
        println!("Server disconnected!");

        ctx.stop()
    }
}
