use actix::prelude::*;

pub struct App {}

impl Actor for App {
    type Context = Context<Self>;
}
