//! Discord API library built on `actix-web`
//!
//! This crate is divided into five separate modules/layers:
//!
//! 1. User Identity
//!     * You or your bot's Discord authorization token and user agent
//! 2. Data-oriented Models
//!     * Provides core model data and little additional functionality
//! 3. Declarative REST API
//!     * Provides access to the Discord REST API using a Declarative API
//! 4. Convenience OOP Models
//!     * Combines the data models (2) with the declarative API (3) internally to provide a more
//!         convenient OOP API
//! 5. Gateway Client
//!     * Discord's real-time websocket-based system that provides users or bots with push events.

#![recursion_limit = "128"]
#![allow(unused_imports, unknown_lints)]
#![allow(clippy::unreadable_literal, clippy::large_enum_variant)]
#![cfg_attr(feature = "nightly", feature(integer_atomics))]

#[macro_use]
extern crate serde;
#[macro_use]
extern crate serde_shims;
#[macro_use]
extern crate enum_primitive;
#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate auto_accessor;

pub mod api;
pub mod app;
pub mod error;
pub mod gateway;
pub mod identity;
pub mod models;

#[cfg(feature = "oop")]
pub mod oop;

#[cfg(feature = "richtext")]
pub mod richtext;

#[cfg(feature = "internal")]
pub mod internal;

#[cfg(not(feature = "internal"))]
mod internal;
