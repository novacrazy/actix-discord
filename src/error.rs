use std::fmt::{self, Display};

use failure::Fail;

use crate::models::permission::Permission;

pub type DiscordResult<T> = Result<T, DiscordError>;

/// Current user is not permitted to perform some action
#[derive(Debug, Fail, Clone, Copy, PartialEq, Eq)]
pub struct NotPermittedError {
    /// Permissions the user had
    pub current: Permission,

    /// Permissions required to perform the action
    pub required: Permission,
}

impl NotPermittedError {
    /// Calculates the missing permissions
    #[inline(always)]
    pub fn missing(&self) -> Permission {
        self.required - self.current
    }
}

impl Display for NotPermittedError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.missing().print(", ", f)
    }
}

#[derive(Fail, Debug, Clone, PartialEq)]
pub enum DiscordError {
    #[fail(display = "Rate Limit Exceeded")]
    RateLimitExceeded,

    /// Error given when serde failed to parse a Discord response.
    ///
    /// Provides the structure path for the model being parsed.
    #[fail(display = "Serde Error: {}", path)]
    SerdeError { path: String },

    #[fail(display = "Not Permitted, Missing: '{}'", 0)]
    NotPermitted(NotPermittedError),

    #[fail(display = "Unknown error")]
    #[doc(hidden)]
    __NonExhaustive,
}

impl From<NotPermittedError> for DiscordError {
    #[inline]
    fn from(err: NotPermittedError) -> DiscordError {
        DiscordError::NotPermitted(err)
    }
}
