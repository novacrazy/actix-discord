use std::any::{Any, TypeId};
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Instant;

use atomic::Atomic;

use parking_lot::{RwLock, RwLockUpgradableReadGuard};

use hashbrown::HashMap;

use futures::{Async, Future, Poll};

use failure::Error;

use actix_web::client::{ClientRequest, ClientResponse, SendRequest};
use actix_web::http::{
    header::{AsHeaderName, HeaderMap},
    StatusCode,
};
use actix_web::HttpMessage;

use crate::error::DiscordError;

use crate::api::rate_limit::{headers::*, RateLimitedBehavior, RateLimiting};
use crate::api::Context;

#[derive(Debug)]
pub struct RateLimitTable {
    pub table: RwLock<HashMap<TypeId, RateLimiting>>,
    pub global: (AtomicBool, RateLimiting),
    pub behavior: Atomic<RateLimitedBehavior>,
}

impl Default for RateLimitTable {
    fn default() -> RateLimitTable {
        RateLimitTable {
            table: RwLock::new(HashMap::default()),
            global: (AtomicBool::new(false), RateLimiting::default()),
            behavior: Atomic::new(RateLimitedBehavior::Delay { retries: 5 }),
        }
    }
}

/// Stages of the RateLimitedFuture state-machine
pub enum RateLimitedFutureStage {
    Global,
    Route,
    Sending,
    Sent(SendRequest),
}

/// State-machine-style Future for sending a rate-limited request,
/// checking for global and per-route rate-limits and following the specified
/// delay or cancel behavior with the given number of
/// retries at the start of the request.
pub struct RateLimitedFuture {
    pub stage: RateLimitedFutureStage,
    pub ctx: Context,

    /// Time at which the rate-limiting is reset and we can try again
    pub reset: Instant,

    /// Uses an `Option` so it can be `.take()`-en upon sending
    pub req: Option<ClientRequest>,
    pub behavior: RateLimitedBehavior,
    pub retries: i64,
    pub id: TypeId,
}

impl RateLimitedFuture {
    pub fn new<T: Any>(
        ctx: Context,
        req: ClientRequest,
        behavior_override: Option<RateLimitedBehavior>,
    ) -> RateLimitedFuture {
        let behavior = behavior_override
            .unwrap_or_else(|| ctx.inner().rate_limit_table.behavior.load(Ordering::SeqCst));

        let retries = behavior.retries();

        RateLimitedFuture {
            stage: RateLimitedFutureStage::Global,
            ctx,
            reset: Instant::now(),
            req: Some(req),
            behavior,
            retries,
            id: TypeId::of::<T>(),
        }
    }
}

pub fn parse_header<T: FromStr>(headers: &HeaderMap, header: impl AsHeaderName) -> Option<T> {
    headers.get(header).and_then(|h| match h.to_str() {
        Ok(value) => value.parse().ok(),
        _ => None,
    })
}

impl Future for RateLimitedFuture {
    type Item = ClientResponse;
    type Error = Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        // extract these specifically for use in the `Sent` stage
        let RateLimitedFuture {
            ref ctx, ref id, ..
        } = self;

        // forward request first and foremost
        if let RateLimitedFutureStage::Sent(ref mut req) = self.stage {
            return match req.poll() {
                Ok(res) => {
                    Ok(res.map(|resp| {
                        let RateLimitTable {
                            global: (ref active_global, ref global_rl),
                            ref table,
                            ..
                        } = ctx.inner().rate_limit_table;

                        let (limit, remaining, reset): (Option<u64>, Option<u64>, Option<u64>) = {
                            let headers = resp.headers();

                            (
                                parse_header(headers, X_RATELIMIT_LIMIT),
                                parse_header(headers, X_RATELIMIT_REMAINING),
                                parse_header(headers, X_RATELIMIT_REMAINING),
                            )
                        };

                        if let StatusCode::TOO_MANY_REQUESTS = resp.status() {
                            let global = {
                                let headers = resp.headers();

                                parse_header(headers, X_RATELIMIT_GLOBAL).unwrap_or(false)
                            };

                            if global {
                                if let Some(limit) = limit {
                                    global_rl.limit.store(limit, Ordering::SeqCst);
                                }

                                if let Some(remaining) = remaining {
                                    global_rl
                                        .remaining
                                        .store(remaining as i64, Ordering::SeqCst);
                                }

                                if let Some(reset) = reset {
                                    global_rl.reset.store(reset, Ordering::SeqCst);
                                }

                                // activate global rate-limiting
                                active_global.store(true, Ordering::SeqCst);

                                // early return if global was all we had
                                return resp;
                            }
                        }

                        let table = table.upgradable_read();

                        if let Some(entry) = table.get(id) {
                            if let Some(limit) = limit {
                                entry.limit.store(limit, Ordering::SeqCst);
                            }

                            if let Some(remaining) = remaining {
                                entry.remaining.store(remaining as i64, Ordering::SeqCst);
                            }

                            if let Some(reset) = reset {
                                entry.reset.store(reset, Ordering::SeqCst);
                            }

                            // early return if we don't have to create a new entry
                            return resp;
                        }

                        let mut table = RwLockUpgradableReadGuard::upgrade(table);

                        table.insert(
                            *id,
                            // just set a high number here on the
                            // almost-never chance these aren't present
                            RateLimiting::new(
                                limit.unwrap_or(9999999),
                                remaining.unwrap_or(9999999),
                                reset.unwrap_or(0),
                            ),
                        );

                        resp
                    }))
                }
                Err(e) => Err(Error::from(e)),
            };
        }

        let now = Instant::now();

        // assume we are waiting on a rate-limit
        // if we aren't, proceed withthe checks
        // This acts like a poor-man's Delay
        if self.reset < now {
            let RateLimitTable {
                global: (ref active_global, ref global_rl),
                ref table,
                ..
            } = self.ctx.inner().rate_limit_table;

            // These stages are organized in such a way so that they can "fall-through"
            // immediately upon success, rather than having to wait for
            // another call to `poll`

            if let RateLimitedFutureStage::Global = self.stage {
                if active_global.load(Ordering::SeqCst) {
                    if global_rl.remaining.fetch_sub(1, Ordering::SeqCst) > 0 {
                        // if everything was good, continue on to per-route rate-limits
                        self.stage = RateLimitedFutureStage::Route;

                        // prepare retries for Route limiting
                        self.retries = self.behavior.retries();
                    } else {
                        match self.behavior {
                            // if the global rate limit was reset,
                            // wait until it resets and try again
                            RateLimitedBehavior::Delay { .. } if self.retries > 0 => {
                                self.reset = global_rl.reset_instant(now);

                                self.retries -= 1;
                            }
                            _ => {
                                return Err(Error::from(DiscordError::RateLimitExceeded));
                            }
                        }
                    }
                } else {
                    self.stage = RateLimitedFutureStage::Route;
                }
            }

            // check for per-route rate-limiting
            if let RateLimitedFutureStage::Route = self.stage {
                let table = table.read();

                match table.get(&self.id) {
                    Some(entry) => {
                        if entry.remaining.fetch_sub(1, Ordering::SeqCst) > 0 {
                            self.stage = RateLimitedFutureStage::Sending;
                        } else {
                            match self.behavior {
                                RateLimitedBehavior::Delay { .. } if self.retries > 0 => {
                                    self.reset = entry.reset_instant(now);

                                    self.retries -= 1;
                                }
                                _ => {
                                    return Err(Error::from(DiscordError::RateLimitExceeded));
                                }
                            }
                        }
                    }
                    None => self.stage = RateLimitedFutureStage::Sending,
                }
            }

            // Send the actual request
            if let RateLimitedFutureStage::Sending = self.stage {
                self.stage = RateLimitedFutureStage::Sent(self.req.take().unwrap().send());
            }
        }

        Ok(Async::NotReady)
    }
}
