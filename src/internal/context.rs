use std::ops::Deref;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::time::Duration;

use atomic::Atomic;

use parking_lot::RwLock;

use actix::prelude::*;
use actix_web::client::ClientConnector;

use crate::identity::Identity;

use crate::models::user::User;

use crate::api::rate_limit::RateLimitedBehavior;

#[cfg(feature = "oop")]
use crate::internal::oop::OopContext;

use crate::internal::rate::RateLimitTable;

pub struct ContextBuilder {
    identity: Identity,
    conn: Option<Addr<ClientConnector>>,
}

impl ContextBuilder {
    #[inline(always)]
    pub fn new(identity: Identity) -> ContextBuilder {
        ContextBuilder {
            identity,
            conn: None,
        }
    }

    #[inline(always)]
    pub fn conn(mut self, conn: Addr<ClientConnector>) -> Self {
        self.conn = Some(conn);
        self
    }

    pub fn finish(self) -> Context {
        let ContextBuilder { identity, conn } = self;

        let inner = ContextInner {
            identity,
            rate_limit_table: RateLimitTable::default(),

            timeout: Atomic::new(None),

            conn,

            user: RwLock::default(),

            #[cfg(feature = "oop")]
            oop: OopContext::default(),
        };

        Context(Arc::new(inner))
    }
}

pub struct ContextInner {
    pub identity: Identity,

    pub rate_limit_table: RateLimitTable,

    pub timeout: Atomic<Option<Duration>>,

    pub conn: Option<Addr<ClientConnector>>,

    pub user: RwLock<Arc<User>>,

    #[cfg(feature = "oop")]
    pub oop: OopContext,
}

#[cfg(feature = "oop")]
impl Deref for ContextInner {
    type Target = OopContext;

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.oop
    }
}

use crate::api::Context;

impl Context {
    #[cfg(feature = "internal")]
    #[inline(always)]
    pub fn rate_limit_table(&self) -> &RateLimitTable {
        &self.inner().rate_limit_table
    }

    pub fn set_rate_limit_behavior(&self, behavior: RateLimitedBehavior) {
        self.inner()
            .rate_limit_table
            .behavior
            .store(behavior, Ordering::SeqCst);
    }

    pub fn set_timeout(&self, timeout: Option<Duration>) {
        self.inner().timeout.store(timeout, Ordering::SeqCst);
    }

    pub fn user(&self) -> Arc<User> {
        let user = self.inner().user.read();

        user.clone()
    }
}
