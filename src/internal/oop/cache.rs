use std::any::{Any, TypeId};
use std::collections::VecDeque;
use std::fmt;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::time::{Duration, Instant};

use atomic::Atomic;

use parking_lot::{RwLock, RwLockUpgradableReadGuard, RwLockWriteGuard};

use hashbrown::HashMap;

use rayon::prelude::*;

use crate::models::snowflake::SnowFlake;

pub struct TypedCacheEntry<T: ?Sized> {
    /// Time this value was constructed
    pub now: Instant,

    /// Epoch during which this value was initially valid
    pub epoch: Instant,

    /// Value itself
    pub value: Arc<T>,
}

impl<T: ?Sized> Clone for TypedCacheEntry<T> {
    fn clone(&self) -> TypedCacheEntry<T> {
        TypedCacheEntry {
            now: self.now,
            epoch: self.epoch,
            value: self.value.clone(),
        }
    }
}

type UntypedCacheEntry = TypedCacheEntry<dyn Any + Send + Sync>;

impl<T> TypedCacheEntry<T>
where
    T: Any + Send + Sync,
{
    #[inline(always)]
    pub fn erase(self) -> UntypedCacheEntry {
        let TypedCacheEntry { now, epoch, value } = self;

        UntypedCacheEntry { now, epoch, value }
    }
}

pub struct TypedCacheTimeline<T: ?Sized> {
    pub kind: TypeId,
    pub seq: RwLock<VecDeque<TypedCacheEntry<T>>>,
}

impl<T: ?Sized> TypedCacheTimeline<T> {
    pub fn find(&self, epoch: Instant) -> Option<TypedCacheEntry<T>> {
        let seq = self.seq.read();

        // latest objects are at the end,
        // so search from the end for the first
        // object before or during the given epoch
        seq.iter()
            .rfind(|entry| entry.now < epoch || entry.epoch == epoch)
            .cloned()
    }

    pub fn push(&self, entry: TypedCacheEntry<T>) {
        let mut seq = self.seq.write();

        seq.push_back(entry);
    }
}

type UntypedCacheTimeline = TypedCacheTimeline<dyn Any + Send + Sync>;

pub type DowncastResult<T> = Result<TypedCacheEntry<T>, UntypedCacheEntry>;

impl UntypedCacheTimeline {
    pub fn from_value<T>(value: T, epoch: Instant) -> (Self, Arc<T>)
    where
        T: Any + Send + Sync,
    {
        let value = Arc::new(value);

        (
            UntypedCacheTimeline {
                kind: TypeId::of::<T>(),
                seq: RwLock::new({
                    let mut seq = VecDeque::with_capacity(1);

                    seq.push_back(UntypedCacheEntry {
                        value: value.clone(),
                        now: Instant::now(),
                        epoch,
                    });

                    seq
                }),
            },
            value,
        )
    }

    pub fn push_value<T>(&self, value: T, epoch: Instant) -> Arc<T>
    where
        T: Any + Send + Sync,
    {
        debug_assert_eq!(TypeId::of::<T>(), self.kind);

        let value = Arc::new(value);

        // construct this first so it doesn't extend the write lock
        let entry = UntypedCacheEntry {
            value: value.clone(),
            now: Instant::now(),
            epoch,
        };

        {
            let mut seq = self.seq.write();

            seq.push_back(entry);
        }

        value
    }

    pub fn find_downcast<T>(&self, epoch: Instant) -> Option<DowncastResult<T>>
    where
        T: Any + Send + Sync,
    {
        debug_assert_eq!(TypeId::of::<T>(), self.kind);

        self.find(epoch).map(|entry| {
            let UntypedCacheEntry { now, epoch, value } = entry;

            match Arc::downcast::<T>(value) {
                Ok(value) => Ok(TypedCacheEntry { value, now, epoch }),
                Err(value) => Err(UntypedCacheEntry { value, now, epoch }),
            }
        })
    }
}

pub type RawCacheTable = HashMap<SnowFlake, UntypedCacheTimeline>;

pub struct CacheTable {
    table: RwLock<RawCacheTable>,
    epoch: Atomic<Instant>,
}

impl Default for CacheTable {
    fn default() -> CacheTable {
        CacheTable {
            table: Default::default(),
            epoch: Atomic::new(Instant::now()),
        }
    }
}

impl CacheTable {
    pub fn end_epoch(&self, epoch: Instant) {
        // this isn't expected to be called constantly
        // from infinite threads, so we can dynamically
        // and opportunistically attempt to update the epoch
        loop {
            let old = self.epoch.load(Ordering::SeqCst);

            // only try if the old epoch is, well, older.
            if old < epoch {
                // attempt to replace the existing value if it hasn't
                // already been updated.
                if let Err(_) = self.epoch.compare_exchange_weak(
                    old,
                    epoch,
                    Ordering::SeqCst,
                    Ordering::Relaxed,
                ) {
                    // retry if it had changed between the load and exchange
                    continue;
                }
            }

            break;
        }
    }

    /// Prune all timelines in parallel, removing all
    /// but the most recent object.
    pub fn prune_fast(&self) {
        let epoch = self.epoch.load(Ordering::SeqCst);

        let table = self.table.read();

        table.par_iter().for_each(|(_, timeline)| {
            let mut seq = timeline.seq.write();

            if seq.len() > 1 {
                let most_recent = seq.iter().next_back().map(|entry| entry.now);

                seq.retain(|entry| entry.now > epoch || Some(entry.now) == most_recent);
            }
        });
    }

    /// Prune entire objects from the cache of the given type
    ///
    /// This should only be used for objects that can be dynamically retreived again.
    pub fn prune_full(&self, kind: TypeId) {
        let epoch = self.epoch.load(Ordering::SeqCst);

        let mut table = self.table.write();

        table.retain(|_, timeline| {
            // retain other kinds
            if timeline.kind != kind {
                return true;
            }

            let mut seq = timeline.seq.write();

            seq.retain(|entry| entry.now > epoch);

            !seq.is_empty()
        });
    }

    /// Get the TypeId of a specific cache entry, if any
    pub fn get_kind(&self, id: SnowFlake) -> Option<TypeId> {
        let table = self.table.read();

        table.get(&id).map(|cached| cached.kind)
    }

    pub fn select<T>(&self, id: SnowFlake, epoch: Instant) -> Option<DowncastResult<T>>
    where
        T: Any + Send + Sync,
    {
        let table = self.table.read();

        table
            .get(&id)
            .and_then(|timeline| timeline.find_downcast::<T>(epoch))
    }

    pub fn insert<F, T>(&self, id: SnowFlake, epoch: Instant, gen: F) -> Arc<T>
    where
        F: FnOnce() -> T,
        T: Any + Send + Sync,
    {
        let table = self.table.upgradable_read();

        if let Some(timeline) = table.get(&id) {
            return timeline.push_value(gen(), epoch);
        }

        let (timeline, value) = UntypedCacheTimeline::from_value(gen(), epoch);

        {
            let mut table = RwLockUpgradableReadGuard::upgrade(table);

            table.insert(id, timeline);
        }

        value
    }
}

use crate::api::Context;

impl Context {
    #[cfg(feature = "internal")]
    #[inline(always)]
    pub fn caches(&self) -> &CacheTable {
        &self.inner().caches
    }
}
