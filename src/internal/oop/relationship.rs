use std::sync::Arc;

use parking_lot::RwLock;

use hashbrown::HashMap;

use crate::models::guild::GuildId;
use crate::models::snowflake::IsSnowFlake;

pub type GuildRelationKey<K> = (GuildId, K);

pub type RawGuildRelationTable<K, T> = HashMap<GuildRelationKey<K>, Arc<T>>;

pub struct GuildRelationTable<K, T>
where
    K: IsSnowFlake,
{
    table: RwLock<RawGuildRelationTable<K, T>>,
}

impl<K, T> Default for GuildRelationTable<K, T>
where
    K: IsSnowFlake,
{
    fn default() -> GuildRelationTable<K, T> {
        GuildRelationTable {
            table: RwLock::new(HashMap::default()),
        }
    }
}

impl<K, T> GuildRelationTable<K, T>
where
    K: IsSnowFlake,
{
    pub fn get(&self, guild: GuildId, key: K) -> Option<Arc<T>> {
        let table = self.table.read();

        table.get(&(guild, key)).cloned()
    }

    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut RawGuildRelationTable<K, T>),
    {
        let mut table = self.table.write();

        f(&mut *table);
    }

    #[inline]
    pub fn table(&self) -> &RwLock<RawGuildRelationTable<K, T>> {
        &self.table
    }
}
