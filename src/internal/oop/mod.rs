pub mod cache;
pub mod relationship;

use self::cache::CacheTable;
use self::relationship::GuildRelationTable;

use crate::models::channel::{Channel, ChannelId};
use crate::models::guild::Member;
use crate::models::user::UserId;

#[derive(Default)]
pub struct OopContext {
    pub caches: CacheTable,
    pub members: GuildRelationTable<UserId, Member>,
    pub channels: GuildRelationTable<ChannelId, Channel>,
}
