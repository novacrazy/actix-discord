use std::fmt;
use std::sync::atomic::{AtomicPtr, Ordering};

#[cfg(not(feature = "nightly"))]
use atomic::Atomic;

#[cfg(not(feature = "nightly"))]
pub type AtomicI64 = Atomic<i64>;

#[cfg(not(feature = "nightly"))]
pub type AtomicU64 = Atomic<u64>;

#[cfg(feature = "nightly")]
pub use std::sync::atomic::{AtomicI64, AtomicU64};
