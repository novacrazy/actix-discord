use std::any::Any;
use std::sync::atomic::Ordering;

use futures::{Future, IntoFuture};

use bytes::Bytes;

use failure::Error;

use actix_web::client::{ClientRequest, ClientRequestBuilder, ClientResponse};
use actix_web::http::{header, Method};
use actix_web::{error::JsonPayloadError, HttpMessage};

use serde::{de::DeserializeOwned, ser::Serialize};

use crate::error::DiscordError;

use crate::api::rate_limit::RateLimitedBehavior;
use crate::api::Context;

use crate::internal::rate::RateLimitedFuture;

pub struct RateLimitedRequestBuilder<'a> {
    ctx: &'a Context,
    builder: ClientRequestBuilder,
}

pub struct RateLimitedRequest<'a> {
    ctx: &'a Context,
    req: ClientRequest,
}

pub struct Response<F: Future<Item = ClientResponse, Error = Error>>(F);

impl<'a> RateLimitedRequestBuilder<'a> {
    pub fn new(ctx: &'a Context) -> Self {
        let mut builder = ClientRequest::build();

        if let Some(ref conn) = ctx.inner().conn {
            builder.with_connector(conn.clone());
        }

        if let Some(timeout) = ctx.inner().timeout.load(Ordering::SeqCst) {
            builder.timeout(timeout);
        }

        builder
            .set_header(header::AUTHORIZATION, ctx.identity().authorization())
            .set_header(header::USER_AGENT, ctx.identity().format_agent());

        RateLimitedRequestBuilder { ctx, builder }
    }

    #[inline]
    pub fn api<U: AsRef<str>>(mut self, uri: U) -> Self {
        self.builder
            .uri(format!("https://discordapp.com/api{}", uri.as_ref()));
        self
    }

    #[inline]
    pub fn uri<U: AsRef<str>>(mut self, uri: U) -> Self {
        self.builder.uri(uri);
        self
    }

    #[inline]
    pub fn method(mut self, method: Method) -> Self {
        self.builder.method(method);
        self
    }

    #[inline(always)]
    pub fn map<F>(self, f: F) -> Self
    where
        F: FnOnce(RateLimitedRequestBuilder<'_>) -> RateLimitedRequestBuilder<'_>,
    {
        f(self)
    }

    pub fn json_body<T: Serialize>(mut self, body: T) -> RateLimitedRequest<'a> {
        RateLimitedRequest {
            ctx: self.ctx,
            req: self
                .builder
                .json(body)
                .expect("Unable to finish building request"),
        }
    }

    pub fn form_body<T: Serialize>(mut self, body: T) -> RateLimitedRequest<'a> {
        RateLimitedRequest {
            ctx: self.ctx,
            req: self
                .builder
                .form(body)
                .expect("Unable to finish building request"),
        }
    }

    pub fn finish(self) -> RateLimitedRequest<'a> {
        let RateLimitedRequestBuilder { ctx, mut builder } = self;

        RateLimitedRequest {
            ctx,
            req: builder.finish().expect("Unable to build request"),
        }
    }

    #[inline]
    pub fn send<T: Any>(
        self,
        behavior_override: Option<RateLimitedBehavior>,
    ) -> Response<impl Future<Item = ClientResponse, Error = Error>> {
        self.finish().send::<T>(behavior_override)
    }
}

impl<'a> RateLimitedRequest<'a> {
    #[inline]
    pub fn build(ctx: &'a Context) -> RateLimitedRequestBuilder<'a> {
        RateLimitedRequestBuilder::new(ctx)
    }

    pub fn send<T: Any>(
        self,
        behavior_override: Option<RateLimitedBehavior>,
    ) -> Response<impl Future<Item = ClientResponse, Error = Error>> {
        let RateLimitedRequest { ctx, req } = self;

        Response(RateLimitedFuture::new::<T>(
            ctx.clone(),
            req,
            behavior_override,
        ))
    }
}

impl<F> Response<F>
where
    F: Future<Item = ClientResponse, Error = Error>,
{
    #[inline]
    pub fn map_body<P, Q>(self, f: P) -> impl Future<Item = Q, Error = Error>
    where
        P: FnOnce(Bytes) -> Q,
    {
        self.0.and_then(|resp| resp.body().from_err()).map(f)
    }

    #[inline]
    pub fn parse_json<T: 'static>(self) -> impl Future<Item = T, Error = Error>
    where
        T: DeserializeOwned,
    {
        self.0
            .and_then(|resp| {
                let json = if let Ok(Some(mime)) = resp.mime_type() {
                    mime.subtype() == mime::JSON || mime.suffix() == Some(mime::JSON)
                } else {
                    false
                };

                if !json {
                    return Err(Error::from(JsonPayloadError::ContentType));
                }

                Ok(resp.body().from_err())
            })
            .flatten()
            .and_then(|bytes| {
                use serde_json as json;
                use serde_path_to_error;

                let mut de = json::Deserializer::from_slice(&bytes);

                serde_path_to_error::deserialize(&mut de).map_err(|err| {
                    Error::from(DiscordError::SerdeError {
                        path: err.path().to_string(),
                    })
                })
            })
    }

    #[inline]
    pub fn finish(self) -> impl Future<Item = (), Error = Error> {
        self.0.map(|_| ())
    }
}

impl<F> IntoFuture for Response<F>
where
    F: Future<Item = ClientResponse, Error = Error>,
{
    type Future = F;
    type Item = ClientResponse;
    type Error = Error;

    #[inline(always)]
    fn into_future(self) -> Self::Future {
        self.0
    }
}
