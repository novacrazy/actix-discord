#![allow(dead_code)]
//! Internal functionality

pub mod atomic;
pub mod context;
pub mod rate;
pub mod request;

#[cfg(feature = "oop")]
pub mod oop;

pub(crate) fn assert_threadsafe<T: Send + Sync>() {
    unreachable!()
}

/// Restricts some traits to types defined in this crate
///
/// More info [Here for C-SEALED](https://rust-lang-nursery.github.io/api-guidelines/future-proofing.html#c-sealed)
pub trait Sealed {}
