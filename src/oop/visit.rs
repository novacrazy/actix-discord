pub enum Visitor<T = ()> {
    Continue,
    Halt(T),
}

pub enum Visited<T = ()> {
    Completed,
    Halted(T),
}

impl<T> Into<Option<T>> for Visited<T> {
    #[inline]
    fn into(self) -> Option<T> {
        match self {
            Visited::Completed => None,
            Visited::Halted(value) => Some(value),
        }
    }
}

impl<T> From<()> for Visitor<T> {
    #[inline]
    fn from(_: ()) -> Visitor<T> {
        Visitor::Continue
    }
}

impl<E> From<Result<(), E>> for Visitor<E> {
    #[inline]
    fn from(res: Result<(), E>) -> Visitor<E> {
        match res {
            Ok(_) => Visitor::Continue,
            Err(e) => Visitor::Halt(e),
        }
    }
}

impl<T> From<Option<T>> for Visitor<T> {
    #[inline]
    fn from(value: Option<T>) -> Visitor<T> {
        match value {
            Some(value) => Visitor::Halt(value),
            None => Visitor::Continue,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    #[ignore]
    fn compile_visit() {
        let _v: Visitor = Visitor::Continue;
    }
}
