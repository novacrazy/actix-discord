use std::any::{Any, TypeId};
use std::ops::Deref;
use std::sync::Arc;
use std::time::Instant;

use futures::{Future, IntoFuture};

use failure::Error;

use crate::models::snowflake::SnowFlake;

use crate::api::Context;

use crate::internal::Sealed;

/// Internal snapshot structure
#[derive(Debug, Clone)]
struct SnapshotInner {
    ctx: Context,
    now: Instant,
}

impl Drop for SnapshotInner {
    fn drop(&mut self) {
        self.ctx.inner().caches.end_epoch(self.now);
    }
}

/// A context that has been frozen in time.
///
/// Cached values will only be considered valid if they were
/// created before this snapshot, or as a direct result of this
/// snapshot's actions.
///
/// This ensures a consistent, albeit potentially outdated,
/// context for the duration of an operation. That is to say,
/// this helps prevent some logical data-races caused by updates
/// from Discord, at the cost of not having the must
/// up-to-date information as possible.
#[derive(Debug, Clone)]
pub struct ContextSnapshot(Arc<SnapshotInner>);

impl Sealed for ContextSnapshot {}

fn _assert_context_snapshot_threadsafe() {
    crate::internal::assert_threadsafe::<ContextSnapshot>();
}

impl AsRef<Context> for ContextSnapshot {
    #[inline(always)]
    fn as_ref(&self) -> &Context {
        &self.0.ctx
    }
}

impl Deref for ContextSnapshot {
    type Target = Context;

    #[inline(always)]
    fn deref(&self) -> &Context {
        &self.0.ctx
    }
}

impl ContextSnapshot {
    /// Create a snapshot from a plain context at the current instant
    pub fn new(ctx: Context) -> ContextSnapshot {
        ContextSnapshot(Arc::new(SnapshotInner {
            ctx,
            now: Instant::now(),
        }))
    }

    /// Fast-forwards the snapshot to Now.
    ///
    /// This new snapshot should not be used with an old snapshot.
    pub fn refresh(self) -> ContextSnapshot {
        ContextSnapshot::new(self.as_ref().clone())
    }

    /// Returns the instant this snapshot was created
    #[inline]
    pub fn epoch(&self) -> Instant {
        self.0.now
    }

    /// Perform a cached request of some type with a SnowFlake ID.
    ///
    /// This cache will ignore objects created after the context snapshot is taken.
    pub fn cached_request<T, F, R>(
        &self,
        id: SnowFlake,
        f: F,
    ) -> impl Future<Item = Arc<T>, Error = Error>
    where
        F: FnOnce(&ContextSnapshot) -> R,
        R: Future<Item = T, Error = Error>,
        T: Any + 'static + Send + Sync,
    {
        let ctx = self.clone();

        self.inner()
            .caches
            .select(id, self.epoch())
            // get the Arc<T>
            .map(|entry| match entry {
                Ok(entry) => entry.value,
                _ => panic!("Cached SnowFlake did not return expected type!"),
            })
            // Convert None to Err(()),
            // so it can be converted to a ResultFuture
            .ok_or(())
            // Convert to ResultFuture<Arc<T>, ()>
            .into_future()
            // If None cache, do request
            .or_else(move |_: ()| {
                f(&ctx).map(move |value| ctx.inner().caches.insert(id, ctx.epoch(), || value))
            })
    }
}
