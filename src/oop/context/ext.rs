//! Global methods which use the Context directly

use futures::{Future, IntoFuture};

use failure::Error;

use crate::error::NotPermittedError;

use crate::models::guild::GuildId;
use crate::models::permission::Permission;
use crate::models::user::UserId;

use crate::api::{Context, Permitted};

use crate::oop::context::ContextSnapshot;
use crate::oop::models::guild::{builder::GuildBuilder, Guild};
use crate::oop::models::invite::Invite;
use crate::oop::models::user::{CurrentUser, User};
use crate::oop::permissions::PermittedOp;

/// Defines "global" Discord functionality that can be used anywhere with
/// little or no additional information.
pub trait ContextExt: AsRef<ContextSnapshot> {
    #[inline]
    fn context(&self) -> &ContextSnapshot {
        self.as_ref()
    }

    /// Returns an object representing the current user or bot
    #[inline]
    fn current_user(&self) -> CurrentUser {
        CurrentUser {
            ctx: self.context(),
        }
    }

    /// Retreives the User model for the given UserId
    #[inline]
    fn get_user(&self, id: UserId) -> Box<dyn Future<Item = User, Error = Error>> {
        Box::new(User::from_id(self.context(), id))
    }

    /// Retreives the Guild model for the given GuildId
    #[inline]
    fn get_guild(&self, id: GuildId) -> Box<dyn Future<Item = Guild, Error = Error>> {
        Box::new(Guild::from_id(self.context(), id))
    }

    /// Creates a new guild with the given builder
    #[inline]
    fn create_guild(&self, builder: GuildBuilder) -> Box<dyn Future<Item = Guild, Error = Error>> {
        Box::new(Guild::create(self.context(), builder))
    }

    /// Get channel/server Invite model from code
    #[inline]
    fn get_invite(
        &self,
        code: String,
        with_counts: bool,
    ) -> Box<dyn Future<Item = Invite, Error = Error>> {
        Box::new(Invite::from_code(self.context(), code, with_counts))
    }

    /// Given the current user permissions, determine if the operation should be executed,
    /// and return a Future that succedes if permissions pass.
    fn with_permission_async<T, F, Q, O>(
        &self,
        current: Permission,
        op: T,
        exec: F,
    ) -> PermittedOp<Q::Future>
    where
        T: Permitted<Owner = O>,
        Self: AsRef<O>,
        F: FnOnce(T) -> Q,
        Q: IntoFuture<Error = Error>,
    {
        let required = op.required_with_owner(self.context(), self.as_ref());

        if current.contains(required) {
            PermittedOp::new(Ok(exec(op).into_future()))
        } else {
            PermittedOp::new(Err(NotPermittedError { current, required }))
        }
    }

    /// Given the current user permissions, determine if the operation should be executed,
    /// and return the result if the permissions pass.
    fn with_permission<T, F, U, O>(&self, current: Permission, op: T, exec: F) -> Result<U, Error>
    where
        T: Permitted<Owner = O>,
        Self: AsRef<O>,
        F: FnOnce(T) -> U,
    {
        let required = op.required_with_owner(self.context(), self.as_ref());

        if current.contains(required) {
            Ok(exec(op))
        } else {
            Err(Error::from(NotPermittedError { current, required }))
        }
    }
}

impl<T> ContextExt for T where T: AsRef<ContextSnapshot> {}
