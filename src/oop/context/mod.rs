pub mod ext;
pub mod snapshot;

pub use self::ext::ContextExt;
pub use self::snapshot::ContextSnapshot;

/// Allows the raw data-only models to be converted to
/// the equivalent OOP Model, either by providing a context directly
/// or by borrowing a context from another OOP Model
pub trait WithContext<T>: Sized {
    /// Create equivalent OOP Model with the given context
    fn with_context(self, ctx: ContextSnapshot) -> T;

    /// Borrow the context from another model to promote
    /// the current type to the equivalent OOP Model
    #[inline]
    fn borrow_context<C>(self, other: &C) -> T
    where
        C: AsRef<ContextSnapshot>,
    {
        self.with_context(other.as_ref().clone())
    }
}
