use futures::Future;

use failure::Error;

use crate::models::guild::GuildId;
use crate::models::user::{User as UserModel, UserId};

use crate::api::select::Select;
use crate::api::{Context, InvertedStaticEndpoint};

use crate::api::endpoints::user;

use crate::oop::context::{ContextSnapshot, WithContext};
use crate::oop::models::channel::Channel;
use crate::oop::models::guild::Guild;

declare_oop!(User: crate::models::user::User);
declare_oop!(Connection: crate::models::user::Connection);

/// Special object representing the current user (You or your Bot)
///
/// Getting data from this has a slight overhead as it is shared within
/// the context itself, but it's still minimal.
pub struct CurrentUser<'a> {
    pub(in crate::oop) ctx: &'a ContextSnapshot,
}

impl<'a> AsRef<Context> for CurrentUser<'a> {
    #[inline]
    fn as_ref(&self) -> &Context {
        &self.ctx
    }
}

impl<'a> AsRef<ContextSnapshot> for CurrentUser<'a> {
    #[inline]
    fn as_ref(&self) -> &ContextSnapshot {
        &self.ctx
    }
}

impl<'a> CurrentUser<'a> {
    #[inline]
    pub fn id(&self) -> UserId {
        self.ctx.user().id()
    }

    #[inline]
    pub fn get(&self) -> User {
        self.ctx.user().borrow_context(self)
    }

    /// Update the current user's info from Discord and return the latest info
    pub fn refresh(&self) -> impl Future<Item = User, Error = Error> {
        let ctx = self.ctx.clone();

        UserModel::exec_static(&self.ctx, user::GetUser(None)).map(|user| user.with_context(ctx))
    }

    #[inline]
    pub fn modify(
        &self,
        username: Option<String>,
        avatar: Option<()>,
    ) -> impl Future<Item = User, Error = Error> {
        let ctx = self.ctx.clone();

        UserModel::exec_static(&self.ctx, user::ModifyUser { username, avatar })
            .map(|user| user.with_context(ctx))
    }

    #[inline]
    pub fn get_guilds(
        &self,
        select: Select<GuildId>,
    ) -> impl Future<Item = Vec<Guild>, Error = Error> {
        let ctx = self.ctx.clone();

        UserModel::exec_static(&self.ctx, user::GetCurrentGuilds { select }).map(move |guilds| {
            guilds
                .into_iter()
                .map(|guild| guild.with_context(ctx.clone()))
                .collect()
        })
    }

    #[inline]
    pub fn get_connections(&self) {}
}

impl User {
    #[inline]
    pub fn from_id(ctx: &ContextSnapshot, id: UserId) -> impl Future<Item = User, Error = Error> {
        let ctx = ctx.clone();

        ctx.cached_request(id.into(), move |ctx| {
            UserModel::exec_static(ctx, user::GetUser(Some(id)))
        })
        .map(|user| user.with_context(ctx))
    }

    #[inline]
    pub fn open_dm(&self) -> impl Future<Item = Channel, Error = Error> {
        let ctx = self.ctx.clone();

        UserModel::exec_static(&self.ctx, user::CreateDM::Direct(self.id))
            .map(|channel| channel.with_context(ctx))
    }
}
