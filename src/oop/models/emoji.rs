use crate::oop::context::WithContext;
use crate::oop::models::user::User;

declare_oop!(Emoji: crate::models::emoji::Emoji);

impl Emoji {
    pub fn user(&self) -> Option<User> {
        self.inner.user().map(|user| user.borrow_context(self))
    }
}
