use futures::Future;

use failure::Error;

use crate::models::invite::Invite as InviteModel;

use crate::api::endpoints::invite::{DeleteInvite, GetInvite};
use crate::api::{Context, Endpoint, InvertedStaticEndpoint};

use crate::oop::context::{ContextSnapshot, WithContext};
use crate::oop::models::channel::Channel;
use crate::oop::models::guild::Guild;

declare_oop!(Invite: crate::models::invite::Invite);

impl Invite {
    pub fn from_code(
        ctx: &ContextSnapshot,
        code: String,
        with_counts: bool,
    ) -> impl Future<Item = Invite, Error = Error> {
        let ctx2 = ctx.clone();

        InviteModel::exec_static(ctx, GetInvite { code, with_counts })
            .map(|invite| invite.with_context(ctx2))
    }

    pub fn delete(&self) -> impl Future<Item = Invite, Error = Error> {
        let ctx = self.ctx.clone();

        self.exec(&self.ctx, DeleteInvite)
            .map(|invite| invite.with_context(ctx))
    }

    pub fn guild(&self) -> Option<Guild> {
        self.inner.guild().map(|guild| guild.borrow_context(self))
    }

    pub fn channel(&self) -> Option<Channel> {
        self.inner
            .channel()
            .map(|channel| channel.borrow_context(self))
    }
}
