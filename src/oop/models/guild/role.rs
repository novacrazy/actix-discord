use crate::models::guild::role;
use crate::models::permission::Permission;

declare_oop!(Role: crate::models::guild::role::Role);

impl Role {}

#[derive(Debug, Clone)]
pub struct RoleBuilder(pub(in crate::oop) role::Role);

impl RoleBuilder {
    pub fn new(name: String) -> RoleBuilder {
        RoleBuilder(role::Role {
            name,
            ..Default::default()
        })
    }

    pub fn color(mut self, color: Option<u32>) -> Self {
        self.0.color = color.unwrap_or(0x000000);
        self
    }

    pub fn hoise(mut self, hoist: bool) -> Self {
        self.0.hoist = hoist;
        self
    }

    pub fn position(mut self, position: u64) -> Self {
        self.0.position = position;
        self
    }

    pub fn permissions(mut self, permissions: Permission) -> Self {
        self.0.permissions = permissions;
        self
    }

    pub fn managed(mut self, managed: bool) -> Self {
        self.0.managed = managed;
        self
    }

    pub fn mentionable(mut self, mentionable: bool) -> Self {
        self.0.mentionable = mentionable;
        self
    }

    pub(in crate::oop) fn finish(self) -> role::Role {
        self.0
    }
}
