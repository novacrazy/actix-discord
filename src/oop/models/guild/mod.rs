use std::iter::Iterator;

use futures::Future;

use failure::Error;

use crate::models::guild::{Guild as GuildModel, GuildId};
use crate::models::permission::Permission;
use crate::models::user::UserId;

use crate::api::endpoints::guild;
use crate::api::{Context, Endpoint, InvertedStaticEndpoint};

use crate::oop::context::{ContextExt, ContextSnapshot, WithContext};
use crate::oop::models::channel::Channel;
use crate::oop::models::user::User;
use crate::oop::visit::{Visited, Visitor};

pub mod builder;
pub mod integration;
pub mod role;

pub use self::integration::Integration;
pub use self::role::Role;

declare_oop!(Guild: crate::models::guild::Guild);
declare_oop!(Member: crate::models::guild::member::Member);

impl Guild {
    pub fn from_id(ctx: &ContextSnapshot, id: GuildId) -> impl Future<Item = Guild, Error = Error> {
        let ctx2 = ctx.clone();

        ctx.cached_request(id.into(), move |ctx| {
            GuildModel::exec_static(ctx, guild::GetGuild(id))
        })
        .map(|guild| guild.with_context(ctx2))
    }

    pub fn create(
        ctx: &ContextSnapshot,
        builder: self::builder::GuildBuilder,
    ) -> impl Future<Item = Guild, Error = Error> {
        let ctx2 = ctx.clone();

        GuildModel::exec_static(ctx, builder.finish()).map(|guild| guild.with_context(ctx2))
    }

    #[inline]
    pub fn delete(&self) -> impl Future<Item = (), Error = Error> {
        self.exec(&self.ctx, guild::DeleteGuild)
    }

    pub fn roles<'a>(&'a self) -> Option<impl Iterator<Item = Role> + 'a> {
        self.inner.roles().map(|roles| {
            roles
                .values()
                .map(move |role| role.clone().borrow_context(self))
        })
    }

    pub fn everyone(&self) -> Option<Role> {
        self.inner.roles().and_then(|roles| {
            roles
                .get(&unsafe { self.id().cast() })
                .map(|role| role.clone().borrow_context(self))
        })
    }

    #[inline]
    pub fn owner(&self) -> Option<impl Future<Item = User, Error = Error>> {
        self.owner_id().map(|id| User::from_id(&self.ctx, id))
    }

    /// Visitor all members of this guild
    ///
    /// This read-locks a context-global member table
    /// for the duration of the visit, so the visit function should
    /// be kept simple, ideally.
    ///
    /// For some operations, this is simpler and faster than
    /// using the collecting methods. (e.g. `members`)
    ///
    /// Using the `Visitor` interface, it is possible to break
    /// the iteration early and return a result.
    pub fn visit_members<F, V, T>(&self, mut f: F) -> Visited<T>
    where
        F: FnMut(Member) -> V,
        V: Into<Visitor<T>>,
    {
        let member_table = self.ctx.inner().members.table().read();

        for ((guild_id, _), member) in member_table.iter() {
            if *guild_id == self.id() {
                if let Visitor::Halt(e) = f(member.clone().borrow_context(self)).into() {
                    return Visited::Halted(e);
                }
            }
        }

        Visited::Completed
    }

    /// Collect members of this guild
    pub fn members(&self) -> Vec<Member> {
        let member_table = self.ctx.inner().members.table().read();

        member_table
            .iter()
            .filter_map(|((guild_id, _), member)| {
                if *guild_id == self.id() {
                    Some(member.clone().borrow_context(self))
                } else {
                    None
                }
            })
            .collect()
    }

    /// Visits all channels in this guild
    ///
    /// This read-locks a context-global channel table
    /// for the duration of the visit, so the visit function should
    /// be kept simple, ideally.
    ///
    /// For some operations, this is simpler and faster than
    /// using the collecting methods (e.g. `channels`)
    ///
    /// Using the `Visitor` interface, it is possible to break
    /// the iteration early and return a result.
    pub fn visit_channels<F, V, T>(&self, mut f: F) -> Visited<T>
    where
        F: FnMut(Channel) -> V,
        V: Into<Visitor<T>>,
    {
        let channel_table = self.ctx.inner().channels.table().read();

        for ((guild_id, _), channel) in channel_table.iter() {
            if *guild_id == self.id() {
                if let Visitor::Halt(e) = f(channel.clone().borrow_context(self)).into() {
                    return Visited::Halted(e);
                }
            }
        }

        Visited::Completed
    }

    /// Collect channels in this guild
    pub fn channels(&self) -> Vec<Channel> {
        let channel_table = self.ctx.inner().channels.table().read();

        channel_table
            .iter()
            .filter_map(|((guild_id, _), channel)| {
                if *guild_id == self.id() {
                    Some(channel.clone().borrow_context(self))
                } else {
                    None
                }
            })
            .collect()
    }

    /// Calculates the base permissions for a user, meaning it does
    /// not account for per-channel overwrites.
    ///
    /// If no user id is given, it uses the current user.
    pub fn base_permissions(&self, user: Option<UserId>) -> Option<Permission> {
        match *self.inner {
            GuildModel::Available {
                id,
                ref roles,
                owner_id,
                permissions: our_permissions,
                ..
            } => {
                if let Some(user) = user {
                    self.ctx.inner().members.get(id, user).map(|member| {
                        if user == owner_id {
                            return Permission::owner();
                        }

                        // https://discordapp.com/developers/docs/topics/permissions#permission-overwrites

                        let mut permissions = roles
                            .get(&unsafe { self.id().cast() })
                            .map(|everyone| everyone.permissions)
                            .unwrap_or_else(Permission::empty);

                        for role_id in member.roles() {
                            if let Some(role) = roles.get(role_id) {
                                permissions |= role.permissions;
                            }
                        }

                        if permissions.contains(Permission::ADMINISTRATOR) {
                            return Permission::admin();
                        }

                        permissions
                    })
                } else {
                    Some(if self.current_user().id() == owner_id {
                        Permission::owner()
                    } else if our_permissions.contains(Permission::ADMINISTRATOR) {
                        Permission::admin()
                    } else {
                        our_permissions
                    })
                }
            }
            _ => None,
        }
    }
}
