use crate::models::guild::{
    DefaultMessageNotificationLevel, ExplicitContentFilterLevel, VerificationLevel,
};
use crate::models::voice::VoiceRegionId;

use crate::api::endpoints::guild;

use crate::oop::models::channel::builder::ChannelBuilder;

#[derive(Debug, Default, Clone)]
pub struct GuildBuilder(pub(in crate::oop) guild::CreateGuild);

impl GuildBuilder {
    pub fn new(name: String) -> GuildBuilder {
        GuildBuilder(guild::CreateGuild {
            name,
            ..Default::default()
        })
    }

    #[inline]
    pub(in crate::oop) fn finish(self) -> guild::CreateGuild {
        self.0
    }

    #[inline]
    pub fn voice_region(mut self, region: VoiceRegionId) -> Self {
        self.0.region = Some(region);
        self
    }

    #[inline]
    pub fn verification_level(mut self, level: VerificationLevel) -> Self {
        self.0.verification_level = level;
        self
    }

    #[inline]
    pub fn default_message_notifications(
        mut self,
        default_message_notifications: DefaultMessageNotificationLevel,
    ) -> Self {
        self.0.default_message_notifications = default_message_notifications;
        self
    }

    #[inline]
    pub fn explicit_content_filter(
        mut self,
        explicit_content_filter: ExplicitContentFilterLevel,
    ) -> Self {
        self.0.explicit_content_filter = explicit_content_filter;
        self
    }

    #[inline]
    pub fn add_role(mut self, role: super::role::RoleBuilder) -> Self {
        self.0.roles.push(role.finish());
        self
    }

    #[inline]
    pub fn add_channel(self, _channel: ChannelBuilder) -> Self {
        //TODO
        self
    }
}
