use crate::oop::context::WithContext;
use crate::oop::models::user::User;

declare_oop!(Integration: crate::models::guild::integration::Integration);

impl Integration {
    pub fn user(&self) -> Option<User> {
        self.inner.user().map(|user| user.borrow_context(self))
    }
}
