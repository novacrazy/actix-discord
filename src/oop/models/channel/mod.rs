use std::sync::Arc;

use futures::Future;

use failure::Error;

use crate::models::channel::{Channel as ChannelModel, ChannelId};
use crate::models::message::MessageId;
use crate::models::permission::Permission;
use crate::models::user::UserId;

use crate::api::select::Select;
use crate::api::{Context, Endpoint, InvertedStaticEndpoint};

use crate::api::endpoints::channel;

use crate::oop::context::{ContextExt, ContextSnapshot, WithContext};
use crate::oop::models::guild::Guild;
use crate::oop::models::message::{Message, MessageBuilder};

pub mod builder;

declare_oop!(Channel: crate::models::channel::Channel);

impl Channel {
    pub fn from_id(
        ctx: &ContextSnapshot,
        id: ChannelId,
    ) -> impl Future<Item = Channel, Error = Error> {
        let ctx2 = ctx.clone();

        ctx.cached_request(id.into(), move |ctx| {
            ChannelModel::exec_static(ctx, channel::GetChannel(id))
        })
        .map(|channel| channel.with_context(ctx2))
    }

    /// Delete the current channel or close the current DM
    ///
    /// The returned channel value may not be valid.
    pub fn delete(&self) -> impl Future<Item = Channel, Error = Error> {
        let ctx = self.ctx.clone();

        self.exec(&self.ctx, channel::DeleteChannel)
            .map(move |channel| channel.with_context(ctx))
    }

    pub fn permissions(&self, user: Option<UserId>) -> Permission {
        //if base_permissions.contains(Permission::ADMINISTRATOR) {
        //    return Permission::all();
        //}

        //let mut permissions = base_permissions;

        //if let Some(everyone) = self
        //    .guild_id()
        //    .and_then(|guild_id| self.overwrites().get(&unsafe { guild_id.cast() }))
        //{
        //    permissions = everyone.apply(permissions);
        //}

        //// TODO

        //permissions

        unimplemented!()
    }

    /// Trigger a typing indicator in this channel
    ///
    /// The indicator is active for about 10 seconds.
    ///
    /// If you wish to have a typing indictor present for a longer
    /// duration, you may want to consider `type_message` or `type_while`
    #[inline]
    pub fn start_typing(&self) -> Box<dyn Future<Item = (), Error = Error>> {
        self.exec(&self.ctx, channel::TriggerTyping)
    }

    /// Repeatedly trigger typing indicators while a message is being generated
    ///
    /// Example:
    ///
    /// ```ignore
    /// channel.type_message(search_web_for_something())
    /// ```
    #[inline]
    pub fn type_message<T, F, M>(&self, f: F) -> impl Future<Item = Message, Error = Error>
    where
        F: Future<Item = M, Error = Error>,
        M: Into<MessageBuilder>,
    {
        let channel = self.clone();

        self.type_while(f)
            // convert to builder here to avoid extra generic monomorphisations
            // on `send_message`
            .and_then(move |builder| channel.send_message(builder.into()))
    }

    /// Repeatedly trigger typing indicators while an operation is being performed
    ///
    /// Example:
    ///
    /// ```ignore
    /// channel.type_while(future::lazy(|| {
    ///     find_nth_prime(10000000).and_then(|_| {
    ///         crack_256bit_aes()
    ///     }).and_then(|_| {
    ///         Delay::new(1000)
    ///     });
    /// }))
    /// ```
    pub fn type_while<T, F>(&self, f: F) -> impl Future<Item = T, Error = Error>
    where
        F: Future<Item = T, Error = Error>,
    {
        use std::time::{Duration, Instant};

        use futures::{future::Loop, Async, Poll};

        struct TypeWhile<T, F>
        where
            F: Future<Item = T, Error = Error>,
        {
            channel: Channel,
            next_typing: Instant,
            typing: Option<Box<dyn Future<Item = (), Error = Error>>>,
            operation: F,
        }

        impl<T, F> Future for TypeWhile<T, F>
        where
            F: Future<Item = T, Error = Error>,
        {
            type Item = T;
            type Error = Error;

            fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
                // poll the operation first so if it returns immediately
                // we don't even have to send a single typing indicator
                match self.operation.poll() {
                    Ok(Async::Ready(value)) => return Ok(Async::Ready(value)),
                    Err(e) => return Err(e),
                    _ => (),
                }

                // in-place map/poll
                self.typing = match self.typing.take() {
                    Some(mut typing) => match typing.poll() {
                        Ok(Async::Ready(_)) => {
                            // finished sending one typing trigger, so send another
                            // in 9.5 seconds
                            self.next_typing = Instant::now() + Duration::from_millis(9500);
                            None
                        }
                        // Error sending typing trigger
                        Err(e) => return Err(e),
                        // Not ready, so just set it back
                        _ => Some(typing),
                    },
                    // If there is no pending typing indicator and it's time
                    // to send a new one, send a new one
                    None if self.next_typing <= Instant::now() => Some(self.channel.start_typing()),
                    // if nothing is being done and it's not time to send another, do nothing
                    _ => None,
                };

                Ok(Async::NotReady)
            }
        }

        TypeWhile {
            channel: self.clone(),
            next_typing: Instant::now(),
            typing: None,
            operation: f,
        }
    }

    pub fn get_messages(
        &self,
        limit: Option<u64>,
        select: Select<MessageId>,
    ) -> impl Future<Item = Vec<Message>, Error = Error> {
        let ctx = self.ctx.clone();

        self.exec(&self.ctx, channel::messages::GetMessages { select, limit })
            .map(move |messages| {
                messages
                    .into_iter()
                    .map(|message| message.with_context(ctx.clone()))
                    .collect()
            })
    }

    pub fn get_message(&self, id: MessageId) -> impl Future<Item = Message, Error = Error> {
        let ctx = self.ctx.clone();
        let inner = self.inner.clone();

        ctx.cached_request(id.into(), move |ctx| {
            inner.exec(ctx, channel::messages::GetMessage(id))
        })
        .map(|message| message.with_context(ctx))
    }

    pub fn send_message(
        &self,
        builder: impl Into<MessageBuilder>,
    ) -> impl Future<Item = Message, Error = Error> {
        self.with_permission_async(Permission::all(), builder.into().finish(), |op| {
            let ctx = self.ctx.clone();

            self.exec(&self.ctx, op)
                .map(|message| message.with_context(ctx))
        })
    }

    pub fn bulk_delete<I: Iterator<Item = MessageId>>(
        &self,
        messages: I,
    ) -> Box<dyn Future<Item = (), Error = Error>> {
        self.exec(
            &self.ctx,
            channel::messages::BulkDeleteMessages(messages.collect()),
        )
    }
}
