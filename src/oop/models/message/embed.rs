//! Embed builder pattern
//!
//! For embeds, basically all fields are optional, but empty embeds are discouraged,
//! so at least one content field must be set. Therefore, a consuming builder interface
//! is presented here as a way of adding only the fields needed for the embed.
//!
//! If no content fields are provided, it will panic upon completion.
//!
//! For embedded media, you can add either an image **or** a video. The embed
//! thumbnail is separate.
//!
//! Example with many features shown:
//!
//! ```ignore
//! use std::iter::FromIterator;
//!
//! channel.send_message(
//!     EmbedBuilder::from_iter(vec![
//!             ("Name", "Value"),
//!             ("Hello", "World!")
//!         ])
//!         .field("Non-text", 10)
//!         .title("My Embed")
//!         .author(|a| a.name("Jack").url("https://google.com"))
//!         .color(0x080808)
//!         .foreach_field(|field| {
//!             if field.value.len() < 10 {
//!                 field.inline = true;
//!             }
//!         })
//!         .thumbnail(|m| m.url("some_image.png"))
//!         .footer("My Footer", |f| f.icon_url("image.png"))
//!         .provider(|p| p.name("My Bot"))
//! )
//! ```

use std::iter::{FromIterator, IntoIterator};
use std::ops::Deref;
use std::string::ToString;

use crate::models::media::Dimensions;
use crate::models::message::embed::{
    Embed, EmbedAuthor, EmbedField, EmbedFooter, EmbedMedia, EmbedMediaData, EmbedProvider,
};

use crate::oop::invalid_result;

/// Main Embed builder
///
/// See module-level documentation for more information and examples
#[derive(Debug, Default, Clone)]
pub struct EmbedBuilder(pub(in crate::oop) Embed);

#[derive(Debug, Default, Clone)]
pub struct EmbedAuthorBuilder(pub(in crate::oop) EmbedAuthor);

#[derive(Debug, Clone)]
pub struct EmbedFooterBuilder(pub(in crate::oop) EmbedFooter);

#[derive(Debug, Default, Clone)]
pub struct EmbedMediaDataBuilder(pub(in crate::oop) EmbedMediaData);

#[derive(Debug, Default, Clone)]
pub struct EmbedProviderBuilder(pub(in crate::oop) EmbedProvider);

impl From<Embed> for EmbedBuilder {
    #[inline]
    fn from(e: Embed) -> EmbedBuilder {
        EmbedBuilder(e)
    }
}

impl Deref for EmbedBuilder {
    type Target = Embed;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<EmbedAuthor> for EmbedAuthorBuilder {
    #[inline]
    fn from(e: EmbedAuthor) -> EmbedAuthorBuilder {
        EmbedAuthorBuilder(e)
    }
}

impl Deref for EmbedAuthorBuilder {
    type Target = EmbedAuthor;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<EmbedFooter> for EmbedFooterBuilder {
    #[inline]
    fn from(e: EmbedFooter) -> EmbedFooterBuilder {
        EmbedFooterBuilder(e)
    }
}

impl Deref for EmbedFooterBuilder {
    type Target = EmbedFooter;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<EmbedMediaData> for EmbedMediaDataBuilder {
    #[inline]
    fn from(e: EmbedMediaData) -> EmbedMediaDataBuilder {
        EmbedMediaDataBuilder(e)
    }
}

impl Deref for EmbedMediaDataBuilder {
    type Target = EmbedMediaData;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<EmbedProvider> for EmbedProviderBuilder {
    #[inline]
    fn from(e: EmbedProvider) -> EmbedProviderBuilder {
        EmbedProviderBuilder(e)
    }
}

impl Deref for EmbedProviderBuilder {
    type Target = EmbedProvider;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<K, V> FromIterator<(K, V)> for EmbedBuilder
where
    K: ToString,
    V: ToString,
{
    #[inline]
    fn from_iter<I>(iter: I) -> EmbedBuilder
    where
        I: IntoIterator<Item = (K, V)>,
    {
        EmbedBuilder::default().fields(false, iter)
    }
}

impl EmbedBuilder {
    /// title of embed
    #[inline]
    pub fn title(mut self, title: impl ToString) -> Self {
        self.0.title = Some(title.to_string());
        self
    }

    /// description of embed
    #[inline]
    pub fn description(mut self, description: impl ToString) -> Self {
        self.0.description = Some(description.to_string());
        self
    }

    /// url of embed
    #[inline]
    pub fn url(mut self, url: impl ToString) -> Self {
        self.0.url = Some(url.to_string());
        self
    }

    /// color code of the embed
    #[inline]
    pub fn color(mut self, color: u32) -> Self {
        self.0.color = Some(color);
        self
    }

    /// Use a closure to generate the author field
    #[inline]
    pub fn author<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedAuthorBuilder) -> EmbedAuthorBuilder,
    {
        self.0.author = Some(f(EmbedAuthorBuilder::default()).finish());
        self
    }

    /// Add fields by iterator, where each item also includes the `inline` properly.
    pub fn fields_variable<I, K, V>(mut self, iter: I) -> Self
    where
        I: IntoIterator<Item = (K, V, bool)>,
        K: ToString,
        V: ToString,
    {
        self.0
            .fields
            .extend(iter.into_iter().map(|(name, value, inline)| EmbedField {
                name: name.to_string(),
                value: value.to_string(),
                inline,
            }));

        self
    }

    /// Add fields by iterator, where every field is inline or not via the
    /// provided parameter.
    pub fn fields<I, K, V>(mut self, inline: bool, iter: I) -> Self
    where
        I: IntoIterator<Item = (K, V)>,
        K: ToString,
        V: ToString,
    {
        self.0
            .fields
            .extend(iter.into_iter().map(|(name, value)| EmbedField {
                name: name.to_string(),
                value: value.to_string(),
                inline,
            }));

        self
    }

    /// Add a single field to this embed
    pub fn field<K, V>(mut self, name: K, value: V, inline: bool) -> Self
    where
        K: ToString,
        V: ToString,
    {
        self.0.fields.push(EmbedField {
            name: name.to_string(),
            value: value.to_string(),
            inline,
        });

        self
    }

    /// Mutably iterate over every field in the embed.
    ///
    /// This is done in-place and does not apply to fields added
    /// after this is called.
    ///
    /// Example:
    ///
    /// ```ignore
    /// use std::iter::FromIterator;
    ///
    /// EmbedBuilder::from_iter(my_fields)
    ///     .foreach_field(|field| {
    ///         if field.value.len() < 10 {
    ///             field.inline = true;
    ///         }
    ///     })
    /// ```
    pub fn foreach_field<F>(mut self, mut f: F) -> Self
    where
        F: FnMut(&mut EmbedField),
    {
        for field in &mut self.0.fields {
            f(field);
        }

        self
    }

    /// Use a closure to generate an embed footer
    #[inline]
    pub fn footer<F>(mut self, text: impl ToString, f: F) -> Self
    where
        F: FnOnce(EmbedFooterBuilder) -> EmbedFooterBuilder,
    {
        self.0.footer = Some(f(EmbedFooterBuilder::new(text)).finish());
        self
    }

    /// Use a closure to provide an embedded thumbnail
    #[inline]
    pub fn thumbnail<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedMediaDataBuilder) -> EmbedMediaDataBuilder,
    {
        self.0.thumbnail = Some(f(EmbedMediaDataBuilder::default()).finish());
        self
    }

    /// Use a closure to provide an embedded image
    #[inline]
    pub fn image<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedMediaDataBuilder) -> EmbedMediaDataBuilder,
    {
        self.0.media = Some(EmbedMedia::Image(
            f(EmbedMediaDataBuilder::default()).finish(),
        ));
        self
    }

    /// Use a closure to provide an embedded video
    #[inline]
    pub fn video<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedMediaDataBuilder) -> EmbedMediaDataBuilder,
    {
        self.0.media = Some(EmbedMedia::Video(
            f(EmbedMediaDataBuilder::default()).finish(),
        ));
        self
    }

    /// Use a closure to generate an embed provider
    pub fn provider<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedProviderBuilder) -> EmbedProviderBuilder,
    {
        self.0.provider = Some(f(EmbedProviderBuilder::default()).finish());
        self
    }

    pub(in crate::oop) fn finish(self) -> Embed {
        let is_valid = self.title.is_some()
            || self.kind.is_some()
            || self.description.is_some()
            || self.url.is_some()
            || self.footer.is_some()
            || self.thumbnail.is_some()
            || self.media.is_some()
            || self.provider.is_some()
            || self.author.is_some()
            || !self.fields.is_empty();

        if !is_valid {
            invalid_result("Embed");
        }

        self.0
    }
}

impl EmbedMediaDataBuilder {
    /// source url of media (only supports http(s) and attachments)
    #[inline]
    pub fn url(mut self, url: impl ToString) -> Self {
        self.0.url = Some(url.to_string());
        self
    }

    /// a proxied url of the media
    #[inline]
    pub fn proxy_url(mut self, url: impl ToString) -> Self {
        self.0.proxy_url = Some(url.to_string());
        self
    }

    #[inline]
    pub fn dimensions(mut self, width: u64, height: u64) -> Self {
        self.0.dimensions = Some(Dimensions { width, height });
        self
    }

    pub(in crate::oop) fn finish(self) -> EmbedMediaData {
        let is_valid = self.url.is_some() || self.proxy_url.is_some() || self.dimensions.is_some();

        if !is_valid {
            invalid_result("EmbedMediaData");
        }

        self.0
    }
}

impl EmbedProviderBuilder {
    /// name of provider
    #[inline]
    pub fn name(mut self, name: impl ToString) -> Self {
        self.0.name = Some(name.to_string());
        self
    }

    /// url of provider
    #[inline]
    pub fn url(mut self, url: impl ToString) -> Self {
        self.0.url = Some(url.to_string());
        self
    }

    pub(in crate::oop) fn finish(self) -> EmbedProvider {
        let is_valid = self.name.is_some() || self.url.is_some();

        if !is_valid {
            invalid_result("EmbedProvider");
        }

        self.0
    }
}

impl EmbedAuthorBuilder {
    /// name of author
    #[inline]
    pub fn name(mut self, name: impl ToString) -> Self {
        self.0.name = Some(name.to_string());
        self
    }

    /// url of author
    #[inline]
    pub fn url(mut self, url: impl ToString) -> Self {
        self.0.url = Some(url.to_string());
        self
    }

    /// url of author icon (only supports http(s) and attachments)
    #[inline]
    pub fn icon_url(mut self, url: impl ToString) -> Self {
        self.0.icon_url = Some(url.to_string());
        self
    }

    /// a proxied url of author icon
    #[inline]
    pub fn proxy_icon_url(mut self, url: impl ToString) -> Self {
        self.0.proxy_icon_url = Some(url.to_string());
        self
    }

    pub(in crate::oop) fn finish(self) -> EmbedAuthor {
        let is_valid = self.name.is_some()
            || self.url.is_some()
            || self.icon_url.is_some()
            || self.proxy_icon_url.is_some();

        if !is_valid {
            invalid_result("EmbedAuthor");
        }

        self.0
    }
}

impl EmbedFooterBuilder {
    /// Create new FooterBuilder with the given text
    #[inline]
    pub fn new(text: impl ToString) -> EmbedFooterBuilder {
        EmbedFooterBuilder(EmbedFooter {
            text: text.to_string(),
            icon_url: None,
            proxy_icon_url: None,
        })
    }

    /// url of footer icon (only supports http(s) and attachments)
    #[inline]
    pub fn icon_url(mut self, url: impl ToString) -> Self {
        self.0.icon_url = Some(url.to_string());
        self
    }

    /// a proxied url of footer icon
    #[inline]
    pub fn proxy_icon_url(mut self, url: impl ToString) -> Self {
        self.0.proxy_icon_url = Some(url.to_string());
        self
    }

    #[inline]
    pub(in crate::oop) fn finish(self) -> EmbedFooter {
        self.0
    }
}
