use std::string::ToString;

use crate::api::endpoints::channel::messages::CreateMessage;

use crate::oop::invalid_result;

use super::EmbedBuilder;

#[derive(Debug, Default, Clone)]
pub struct MessageBuilder(pub(in crate::oop) CreateMessage);

impl<T: ToString> From<T> for MessageBuilder {
    #[inline]
    fn from(value: T) -> MessageBuilder {
        MessageBuilder::new().content(value.to_string())
    }
}

impl From<EmbedBuilder> for MessageBuilder {
    #[inline]
    fn from(embed: EmbedBuilder) -> MessageBuilder {
        MessageBuilder::new().embed(|_| embed)
    }
}

impl From<CreateMessage> for MessageBuilder {
    #[inline]
    fn from(value: CreateMessage) -> MessageBuilder {
        MessageBuilder(value)
    }
}

impl MessageBuilder {
    #[inline]
    pub fn new() -> MessageBuilder {
        MessageBuilder::default()
    }

    #[inline]
    pub fn content(mut self, content: impl ToString) -> Self {
        self.0.content = Some(content.to_string());
        self
    }

    #[inline]
    pub fn tts(mut self, tts: bool) -> Self {
        self.0.tts = tts;
        self
    }

    #[inline]
    pub fn embed<F>(mut self, f: F) -> Self
    where
        F: FnOnce(EmbedBuilder) -> EmbedBuilder,
    {
        self.0.embed = Some(Box::new(f(EmbedBuilder::default()).finish()));
        self
    }

    pub(in crate::oop) fn finish(self) -> CreateMessage {
        let is_valid = self.0.content.is_some() || self.0.embed.is_some();

        if !is_valid {
            invalid_result("Message");
        }

        self.0
    }
}
