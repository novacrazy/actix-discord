use std::sync::Arc;

use futures::Future;

use failure::Error;

use crate::models::emoji;
use crate::models::message::{self, embed::Embed};
use crate::models::user::UserId;

use crate::api::select::Select;
use crate::api::{Context, Endpoint, InvertedStaticEndpoint, StaticEndpoint};

use crate::api::endpoints::message::{
    CreateReaction, DeleteMessage, DeleteReaction, EditMessage, GetReactionUsers, PinMessage,
    UnpinMessage,
};

use crate::oop::context::{ContextExt, ContextSnapshot, WithContext};
use crate::oop::models::channel::Channel;
use crate::oop::models::guild::Guild;
use crate::oop::models::user::User;

pub mod builder;
pub mod embed;

pub use self::builder::MessageBuilder;
pub use self::embed::EmbedBuilder;

declare_oop!(Message: crate::models::message::Message);

impl Message {
    pub fn edit(
        &self,
        content: Option<String>,
        embed: Option<Box<message::embed::Embed>>,
    ) -> impl Future<Item = Message, Error = Error> {
        let ctx = self.ctx.clone();

        self.exec(&self.ctx, EditMessage { content, embed })
            .map(|message| message.with_context(ctx))
    }

    #[inline]
    pub fn delete(&self) -> Box<dyn Future<Item = (), Error = Error>> {
        self.exec(&self.ctx, DeleteMessage)
    }

    /// Pin this message to the channel
    #[inline]
    pub fn pin(&self) -> Box<dyn Future<Item = (), Error = Error>> {
        self.exec(&self.ctx, PinMessage)
    }

    #[inline]
    pub fn unpin(&self) -> Box<dyn Future<Item = (), Error = Error>> {
        self.exec(&self.ctx, UnpinMessage)
    }

    #[inline]
    pub fn add_reaction<E>(&self, reaction: E) -> Box<dyn Future<Item = (), Error = Error>>
    where
        E: AsRef<emoji::Emoji>,
    {
        self.exec(&self.ctx, CreateReaction(reaction.as_ref()))
    }

    #[inline]
    pub fn delete_reaction<E>(&self, reaction: E) -> impl Future<Item = (), Error = Error>
    where
        E: AsRef<emoji::Emoji>,
    {
        self.exec(
            &self.ctx,
            DeleteReaction {
                user: None,
                reaction: reaction.as_ref(),
            },
        )
    }

    #[inline]
    pub fn delete_user_reaction<E>(
        &self,
        reaction: E,
        user: UserId,
    ) -> impl Future<Item = (), Error = Error>
    where
        E: AsRef<emoji::Emoji>,
    {
        self.exec(
            &self.ctx,
            DeleteReaction {
                user: Some(user),
                reaction: reaction.as_ref(),
            },
        )
    }

    pub fn get_reaction_users<E>(
        &self,
        reaction: E,
        limit: Option<u64>,
        select: Select<UserId>,
    ) -> impl Future<Item = Vec<User>, Error = Error>
    where
        E: AsRef<emoji::Emoji>,
    {
        let ctx = self.ctx.clone();

        self.exec(
            &self.ctx,
            GetReactionUsers {
                reaction: reaction.as_ref(),
                select,
                limit,
            },
        )
        .map(move |users| {
            users
                .into_iter()
                .map(|user| user.with_context(ctx.clone()))
                .collect()
        })
    }
}
