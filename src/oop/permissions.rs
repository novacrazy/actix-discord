use futures::{Future, Poll};

use failure::Error;

use crate::error::NotPermittedError;

/// Simple Future to defer error handling and flattening for
/// `ContextExt::with_permission`
///
/// It will try to preserve the full backtrace
/// from when the error occurred.
pub struct PermittedOp<F> {
    result: Result<F, NotPermittedError>,
    err: Option<Error>,
}

impl<F> PermittedOp<F> {
    pub(in crate::oop) fn new(result: Result<F, NotPermittedError>) -> PermittedOp<F> {
        PermittedOp {
            err: match &result {
                Err(ref err) => Some(Error::from(*err)),
                _ => None,
            },
            result,
        }
    }
}

impl<F, T> Future for PermittedOp<F>
where
    F: Future<Item = T, Error = Error>,
{
    type Item = T;
    type Error = Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match &mut self.result {
            Ok(ref mut op) => op.poll(),

            // try to use original error if possible,
            // or just generate one here. Ideally,
            // `poll` won't be called after an error
            Err(err) => match self.err.take() {
                Some(original) => Err(original),
                None => Err(Error::from(*err)),
            },
        }
    }
}
