//! Object-Oriented API
//!
//! Most of this library is provided either as raw data structures
//! with very little functionality on their own, or the simplistic
//! "declarative" REST API which requires managing contexts and so forth.
//!
//! The OOP models here combined those into a more traditional API
//! with zero overhead in many cases.

#[cold]
pub(in crate::oop) fn invalid_result(name: &str) -> ! {
    panic!(
        "Invalid {}! At least one content or embed field must be set.",
        name
    );
}

macro_rules! declare_oop {
    ($(#[$($attrs:tt)*])* $name:ident: $p:path $(, {
        $(
            $(#[$($field_attrs:tt)*])*
            $field:ident: $field_ty:ty,
        )*
    })*) => {
        $(#[$($attrs)*])*
        #[derive(Debug, Clone)]
        pub struct $name {
            ctx: $crate::oop::context::ContextSnapshot,
            inner: std::sync::Arc<$p>,
            $($(
                $(#[$($field_attrs)*])*
                $field: $field_ty,
            )*)*
        }

        impl $crate::internal::Sealed for $name {}

        impl $name {
            #[allow(dead_code)]
            #[inline]
            pub(in crate::oop) fn new(
                ctx: $crate::oop::context::ContextSnapshot,
                inner: std::sync::Arc<$p>,
                $($($field: $field_ty),*)*
            ) -> $name {
                $name {
                    ctx, inner,
                    $($($field),*)*
                }
            }
        }

        impl $crate::oop::context::WithContext<$name> for $p {
            #[inline]
            fn with_context(self, ctx: $crate::oop::context::ContextSnapshot) -> $name {
                $name { ctx, inner: std::sync::Arc::new(self), $($($field: Default::default(),)*)* }
            }
        }

        impl $crate::oop::context::WithContext<$name> for std::sync::Arc<$p> {
            #[inline]
            fn with_context(self, ctx: $crate::oop::context::ContextSnapshot) -> $name {
                $name { ctx, inner: self, $($($field: Default::default(),)*)* }
            }
        }

        impl std::convert::AsRef<$p> for $name {
            #[inline(always)]
            fn as_ref(&self) -> &$p {
                &self.inner
            }
        }

        impl std::convert::AsRef<$crate::oop::context::ContextSnapshot> for $name {
            #[inline(always)]
            fn as_ref(&self) -> &$crate::oop::context::ContextSnapshot {
                &self.ctx
            }
        }

        impl std::convert::AsRef<$crate::api::Context> for $name {
            #[inline(always)]
            fn as_ref(&self) -> &$crate::api::Context {
                &self.ctx
            }
        }

        impl std::ops::Deref for $name {
            type Target = $p;

            #[inline(always)]
            fn deref(&self) -> &$p {
                self.as_ref()
            }
        }
    };
}

pub mod context;
pub mod models;
pub mod permissions;
pub mod visit;

/// All OOP Models and traits
pub mod prelude {
    pub use crate::oop::context::*;
    pub use crate::oop::models::{channel::*, guild::*, invite::*, message::*, user::*};
    pub use crate::oop::visit::{Visited, Visitor};
}
