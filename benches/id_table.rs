#[macro_use]
extern crate bencher;

use std::iter::{self, FromIterator};

use bencher::{black_box, Bencher};
use rand::prelude::*;

use actix_discord::models::id::IdTable;
use actix_discord::models::permission::{Overwrite, OverwriteType, Permission};
use actix_discord::models::snowflake::SnowFlake;

// Use a fixed seed for generating and testing,
// so it's consistent
const SEED: u64 = 0xDEAD_BEEF;

// Repeat for many iterations to provide ample random access
const ITERATIONS: u64 = 10000;

const FIXTURE_SIZE: usize = 20;

fn generate(count: usize) -> Vec<Overwrite> {
    let mut rng = StdRng::seed_from_u64(SEED);

    // the random kind and permissions are to avoid any
    // potential caching or whatever.
    iter::repeat_with(|| Overwrite {
        id: SnowFlake(rng.gen()).into(),
        kind: if rng.gen() {
            OverwriteType::Role
        } else {
            OverwriteType::Member
        },
        allow: Permission::from_bits_truncate(rng.gen()),
        deny: Permission::from_bits_truncate(rng.gen()),
    })
    .take(count)
    .collect()
}

fn id_table(bench: &mut Bencher) {
    let mut rng = SmallRng::seed_from_u64(SEED);

    let overwrites = generate(FIXTURE_SIZE);

    let table = IdTable::from_iter(overwrites.iter().cloned());

    bench.iter(|| {
        for _ in 0..ITERATIONS {
            let index = rng.gen_range(0, overwrites.len());

            let rand_id = unsafe { overwrites.get_unchecked(index).id };

            black_box(table.get(&rand_id));
        }
    })
}

fn plain_vec(bench: &mut Bencher) {
    let mut rng = SmallRng::seed_from_u64(SEED);

    let overwrites = generate(FIXTURE_SIZE);

    let mut shuffled = overwrites.clone();

    shuffled.shuffle(&mut rng);

    bench.iter(|| {
        for _ in 0..ITERATIONS {
            let index = rng.gen_range(0, overwrites.len());

            let rand_id = unsafe { overwrites.get_unchecked(index).id };

            black_box(shuffled.iter().find(|overwrite| overwrite.id == rand_id));
        }
    })
}

benchmark_group!(benches, id_table, plain_vec);
benchmark_main!(benches);
